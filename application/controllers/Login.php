<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
    }
	public function index()
	{
        $this->load->view('login');
	}
    
	public function login(){
		$username = $this->input->post('usuario');
        $password = $this->input->post('password');
        // Inicializamos la variable de respuesta en 0;
        $count = 0;
        // Obtenemos los datos del usuario ingresado mediante su usuario
        
        $respuesta = $this->Login_model->login($username);
        $contrasena='';
        foreach ($respuesta as $item) {
            $contrasena =$item->contrasena;
        }
        // Verificamos si las contraseñas son iguales
        $verificar = password_verify($password,$contrasena);

        // En caso afirmativo, inicializamos datos de sesión
        if ($verificar) 
        {
            $data = array(
                        'logeado' => true,
                        'usuarioid' => $respuesta[0]->UsuarioID,
                        'usuario' => $respuesta[0]->nombre,
                        'perfilid'=>$respuesta[0]->perfilId,
                        'idpersonal'=>$respuesta[0]->personalId,
                        'administrador'=>$respuesta[0]->administrador,
                        'cliente'=>'0',
                        'operador'=>'0',
                    );
            $this->session->set_userdata($data);
            $count=1;
        }

        if($count == 0){
            // Obtenemos los datos del usuario ingresado mediante su usuario
            $respuesta = $this->Login_model->loginO($username);
            $contrasena='';
            foreach ($respuesta as $item) {
                $contrasena =$item->contrasena;
            }
            // Verificamos si las contraseñas son iguales
            $verificar = password_verify($password,$contrasena);

            // En caso afirmativo, inicializamos datos de sesión
            if ($verificar) 
            {
                $data = array(
                            'logeado' => true,
                            'usuarioid' => $respuesta[0]->UsuarioID,
                            'usuario' => $respuesta[0]->nombre,
                            'perfilid'=>$respuesta[0]->perfilId,
                            'idpersonal'=>$respuesta[0]->operadorId,
                            'administrador'=>$respuesta[0]->administrador,
                            'cliente'=>'0',
                            'operador'=>$respuesta[0]->operadorId,
                        );
                $this->session->set_userdata($data);
                $count=1;
            }
        }

        if($count == 0){
            // Obtenemos los datos del usuario ingresado mediante su usuario 
            $respuesta = $this->Login_model->loginC($username); 
            $contrasena=''; 
            foreach ($respuesta as $item) { 
                $contrasena =$item->contrasena; 
            } 
            // Verificamos si las contraseñas son iguales
            $verificar = password_verify($password,$contrasena); 
 
            // En caso afirmativo, inicializamos datos de sesión 
            if ($verificar)  
            { 
                $data = array( 
                            'logeado' => true, 
                            'usuarioid' => $respuesta[0]->UsuarioID, 
                            'usuario' => $respuesta[0]->nombre, 
                            'perfilid'=>$respuesta[0]->perfilId, 
                            'idpersonal'=>$respuesta[0]->clienteId, 
                            'administrador'=>$respuesta[0]->administrador,
                            'cliente'=>$respuesta[0]->clienteId,
                            'operador'=>'0',
                        ); 
                $this->session->set_userdata($data); 
                $count=1; 
            } 
        } 

        // Devolvemos la respuesta
        //log_message('error','COUNT: '. $count);
        echo $count;

	}
	public function logout(){
		$this->session->sess_destroy();
        //redirect(base_url(), 'refresh');
        redirect('/index.php');
	}
}
