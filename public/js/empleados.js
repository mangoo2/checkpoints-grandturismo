var base_url = $('#base_url').val();
var perfil = $('#perfil').val(); 
var tabla;
var tabla_entrenamiento;
var aux_idempleado=0;
$(document).ready(function() {
    $('#foto_avatar').change(function(){
        document.getElementById('img_avatar').src = window.URL.createObjectURL(this.files[0]);
    });
	table();
    $('textarea.js-auto-size').textareaAutoSize();
});
function modal_empleado(){
    $('#personalId').val('0');
    $('#nombre').val('');
    
    $('#appaterno').val('');
    $('#apmaterno').val('');
    $('#operativo').val('');
    $('#empresa').val(1);

    $('#tel_emergencia').val('');
    $('#persona').val('');
    $('#parentesco').val('');
    $('#sangre').val('');

    $('#verificar_check').prop('checked',false);
    $('.baja_texto').css('display','none');
    $('#fechabaja').val('');
    $('#motivo').val('');
    $('.foto_empl').html('<img id="img_avatar" style="width: 145px; height: 145px; border-radius: 70px;" src="'+base_url+'images/annon.png"  accept=".jpg, .jpeg, .png">');   
	//$('#especial').val('');
    $('#especial').attr("checked",false);

    $('#registro_empleado').modal();
}

function guarda_empleado(){
    var form_register = $('#form_empleado');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre:{
                required: true
            },
            tel_emergencia: {
                maxlength: 10,
                minlength: 10,
                digits: true
            },
            cliente:{
                required: true
            },
        },
        messages: {
            tel_emergencia: {
                maxlength: "Ingrese número telefónico de 10 dígitos.",
                digits: "Por favor, ingresa solo números"
            }
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_empleado").valid();
    if($valid) {
        $('.btn_registro').attr('disabled',true);
        var datos = form_register.serialize();
        var especial=0;
        if($("#especial").is(":checked")==true){
            especial=1;
        }
        $.ajax({
            type:'POST',
            url: base_url+'Empleados/registra_empleado',
            data: datos+"&especial="+especial,
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){
                var id=data;
                add_file(id);
                $.toast({
                    heading: 'Éxito',
                    text: 'Guardado Correctamente',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                });

                setTimeout(function(){
                    $('#form_empleado')[0].reset();
                    //$valid.resetForm();
                    var formID = $('#form_empleado')
                    formID.validate().resetForm(); 

                    reload_registro();
                    $('#registro_empleado').modal('hide');
                    $('.btn_registro').attr('disabled',false);
                    
                }, 1500);

                /*
                setTimeout(function(){ 
                window.location = base_url+'Empleados';
                }, 1500);
                */
            }
        });
    }   
}
function reload_registro(){
    tabla.destroy();
    table();
}

function table(selectCliente = 0){
    console.log("table");
    var cont=0;
	tabla=$("#table_datos").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Empleados/getlistado",
            type: "post",
            "data":{
                empleado:$('#empleado_busqueda').val(),
                particular: '0',
                selectCliente: selectCliente,
            },
            error: function(){
                $("#table_datos").css("display","none");
            }
        },
        "columns": [
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='</div>\
                        <div class="custom-control custom-checkbox mr-sm-2 mb-3">\
                            <input type="checkbox" class="custom-control-input check_id" name="check_id" id="check_id_'+cont+'">\
                            <label class="custom-control-label" for="check_id_'+cont+'"></label>\
                            <input type="hidden" name="personalId" value="'+row.personalId+'">\
                        </div>'; 
                    cont ++;   
                return html;
                }
            },
            {"data":"personalId"},
            {"data":null,
                "render":function(row){
                    return row.nombre + ' ' + row.appaterno + ' ' + row.apmaterno;
                }
            },
            {"data":"operativo"},
            {"data":null,
                "render":function(row){
                    if (row.NomCliente == null){
                        return '------ ------ ------';
                    }
                    return row.NomCliente + ' ' + row.AppCliente + ' ' + row.ApmCliente;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html=''; 
                    if(row.check_baja=='on'){
                        html+='<span class="label label-danger m-r-10">Baja</span>';    
                    }else{
                        html+='';
                    }
                return html;
                }
            },
            {
                "data": null,
                "render": function (data, type, row, meta) {
                    //console.log("row "+ JSON.stringify(row));
                    var color_usuario = '';
                    if (row.usuario == 1) {
                        color_usuario = 'btn-warning';
                    } else {
                        color_usuario = 'btn-secondary';
                    }
                    var html = '';
                    if (perfil != 2) {
                        html += '<button type="button"\
                            data-perid="' + row.personalId + '"\
                            data-nombre="' + row.nombre + '"\
                            data-appaterno="' + row.appaterno + '"\
                            data-apmaterno="' + row.apmaterno + '"\
                            data-operativo="' + row.operativo + '"\
                            data-cliente="' + row.cliente + '"\
                            data-tel_emergencia="' + row.tel_emergencia + '"\
                            data-persona="' + row.persona + '"\
                            data-parentesco="' + row.parentesco + '"\
                            data-sangre="' + row.sangre + '"\
                            data-fechabaja="' + row.fechabaja + '"\
                            data-motivo="' + row.motivo + '"<\
                            data-check_baja="' + row.check_baja + '"\
                            data-foto="' + row.foto + '"\
                            data-especial="' + row.especial + '"\
                            class="btn btn_sistema btn-circle paciente_datos_' + row.personalId + '" onclick="modal_editar(' + row.personalId + ')"><i class="far fa-edit"></i></button>';

                        html += '<button type="button" class="btn ' + color_usuario + ' btn-circle" onclick="modal_usuario(' + row.personalId + ');"><i class="fas fa-user"></i> </button>';
                    }
                    html += '<button type="button" class="btn btn-primary btn-circle" onclick="imprimir_gafete(' + row.personalId + ');"><i class="fas fa-id-card-alt"></i> </button>';
                    if (perfil != 2) {
                        html += '<button type="button" class="btn btn-danger btn-circle" onclick="eliminar_empleado(' + row.personalId + ');"><i class="fas fa-trash-alt"></i> </button>';
                    }
                    return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
}

function modal_editar(id){
	$('#personalId').val($('.paciente_datos_'+id).data('perid'));
	$('#nombre').val($('.paciente_datos_'+id).data('nombre'));

    $('#appaterno').val($('.paciente_datos_'+id).data('appaterno'));
    $('#apmaterno').val($('.paciente_datos_'+id).data('apmaterno'));
    $('#operativo').val($('.paciente_datos_'+id).data('operativo'));
    $('#cliente').val($('.paciente_datos_'+id).data('cliente'));

    $('#tel_emergencia').val($('.paciente_datos_'+id).data('tel_emergencia'));
    $('#persona').val($('.paciente_datos_'+id).data('persona'));
    $('#parentesco').val($('.paciente_datos_'+id).data('parentesco'));
    $('#sangre').val($('.paciente_datos_'+id).data('sangre'));
    
    var check_baja = $('.paciente_datos_'+id).data('check_baja');
    if(check_baja=='on'){
       $('#verificar_check').prop('checked',true);
       $('.baja_texto').css('display','block');
    }else{
       $('#verificar_check').prop('checked',false);
       $('.baja_texto').css('display','none');
    }

    var foto = $('.paciente_datos_'+id).data('foto');
    if(foto!=''){
        $('.foto_empl').html('<img id="img_avatar" style="width: 145px; height: 145px; border-radius: 70px;" src="'+base_url+'uploads/personal/'+foto+'" accept=".jpg, .jpeg, .png">');
    }else{    
        $('.foto_empl').html('<img id="img_avatar" style="width: 145px; height: 145px; border-radius: 70px;" src="'+base_url+'images/annon.png"  accept=".jpg, .jpeg, .png">');   
    }
    $('#fechabaja').val($('.paciente_datos_'+id).data('fechabaja'));
    $('#motivo').val($('.paciente_datos_'+id).data('motivo'));

    if($('.paciente_datos_'+id).data('especial')==1){
        $('#especial').attr("checked",true);
    }else{
        $('#especial').attr("checked",false);
    }
    /*
    var color =$('.paciente_datos_'+id).data('color');
    if(color=='red'){
        $('#color1').prop('checked',true);
        $('#color2').prop('checked',false);
        $('#color3').prop('checked',false);
        $('#color4').prop('checked',false);
        $('#color5').prop('checked',false);
    }else if(color=='orange'){
        $('#color1').prop('checked',false);
        $('#color2').prop('checked',true);
        $('#color3').prop('checked',false);
        $('#color4').prop('checked',false);
        $('#color5').prop('checked',false);
    }else if(color=='yellow'){
        $('#color1').prop('checked',false);
        $('#color2').prop('checked',false);
        $('#color3').prop('checked',true);
        $('#color4').prop('checked',false);
        $('#color5').prop('checked',false);
    }else if(color=='#03a9f4'){
        $('#color1').prop('checked',false);
        $('#color2').prop('checked',false);
        $('#color3').prop('checked',false);
        $('#color4').prop('checked',true);
        $('#color5').prop('checked',false);
    } 
    else if(color=='#00bb2d'){
        $('#color1').prop('checked',false);
        $('#color2').prop('checked',false);
        $('#color3').prop('checked',false);
        $('#color4').prop('checked',false);
        $('#color5').prop('checked',true);
    } 
    */
	$('#registro_empleado').modal();
}
function check_baja_btn(){
    if($('#verificar_check').is(':checked')){
        $('.baja_texto').css('display','block');
    }else{
        $('.baja_texto').css('display','none');
    }
}
function eliminar_empleado(id){ 
    $('#id_empleado').val(id);
    $('#elimina_empleado_modal').modal();
}
function delete_empleado(){
    var idp=$('#id_empleado').val(); 
    $.ajax({
        type:'POST',
        url: base_url+"Empleados/delete_empleado",
        data:{id:idp},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Elimando Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            $('#elimina_empleado_modal').modal('hide');
            setTimeout(function(){ 
                tabla.ajax.reload(); 
            }, 1000); 
        }
    });  
}
function modal_usuario(id){
    $('#usuario_modal').modal();
    $('#idEmp').val(id);

    $.ajax({
        type:'POST',
        url: base_url+'Empleados/registro_usuario',
        data:{id:id},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.text_usuario').html(data);
            
            if ($('#usuario').val() == '') {
                $('#perfilId').val($('#tipoEmp').val());
            }
        }
    });
}
function guardar_usuario(){
    var form_register = $('#form_usuario');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            usuario:{
              required: true
            },
            contrasena:{
              required: true
            },
            contrasena2: {
                equalTo: contrasena,
                required: true
            },
            personalId: {
                required: true
            },
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_usuario").valid();
    if($valid) {
        $('.btn_registro').attr('disabled',true);
        var datos = form_register.serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Empleados/add_usuarios',
            data: datos,
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){
                $.toast({
                    heading: 'Éxito',
                    text: 'Guardado Correctamente',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                });
                $('#usuario_modal').modal('hide');
                setTimeout(function(){ 
                    $('.btn_registro').attr('disabled',false);
                }, 1000);
                reload_registro();
            }
        });
    }   
}
function verificar_usuario(id){
    $.ajax({
        type: 'POST',
        url: base_url + 'Empleados/validar',
        data: {
            Usuario:$('#usuario').val(),
            Id:$('#idEmp').val(),
        },
        async: false,
        statusCode: {
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success: function(data) {
            if (data == 1) {
                $('.txt_usuario').html('El usuario ya existe');
                $('#btn_modal_usu').prop('disabled', true);
                $.toast({
                    heading: '¡Atención!',
                    text: 'El usuario ya existe',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });

            }else{
                $('.txt_usuario').html('');
                $('#btn_modal_usu').prop('disabled', false);
            }
        }
    });
}
function imprimir_gafete(id){
    $.toast({
        heading: 'Éxito',
        text: 'Por favor, espere',
        position: 'top-right',
        loaderBg:'#ff6849',
        icon: 'success',
        hideAfter: 3500, 
        stack: 2
    });

    //$('.iframe_datos').html('');
    var htmlt='<iframe src="'+base_url+'Empleados/gafete/'+id+'"></iframe>';
    setTimeout(function(){     
        $('.iframe_datos').html(htmlt);
    }, 500);   
}

function add_file(id){
  //console.log("archivo: "+archivo);
  //console.log("name: "+name);
    var archivo=$('#foto_avatar').val()
    //var name=$('#foto_avatar').val()
    extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
    extensiones_permitidas = new Array(".jpeg",".png",".jpg");
    permitida = false;
    if($('#foto_avatar')[0].files.length > 0) {
        for (var i = 0; i < extensiones_permitidas.length; i++) {
           if (extensiones_permitidas[i] == extension) {
           permitida = true;
           break;
           }
        }  
        if(permitida==true){
          //console.log("tamaño permitido");
            var inputFileImage = document.getElementById('foto_avatar');
            var file = inputFileImage.files[0];
            var data = new FormData();
            data.append('foto',file);
            data.append('id',id);
            $.ajax({
                url:base_url+'Empleados/cargafiles',
                type:'POST',
                contentType:false,
                data:data,
                processData:false,
                cache:false,
                success: function(data) {
                    var array = $.parseJSON(data);
                        //console.log(array.ok);
                        /*
                        if (array.ok=true) {
                            swal("Éxito", "Guardado Correctamente", "success");
                        }else{
                            swal("Error!", "No Se encuentra el archivo", "error");
                        }
                        */
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    var data = JSON.parse(jqXHR.responseText);
                }
            });
        }
    }        
}
//===========================================================
function btn_check_todo(){
    if($('#check_todo').is(':checked')){
        $('.check_id').prop('checked', true);
    }else{
        $('.check_id').prop('checked', false);
    }
}

function btn_eliminar_registros(){
    $('#elimina_registro_modal').modal();
}

function delete_registros(){
    var DATA  = [];
    var TABLA   = $("#table_datos tbody > tr");
        TABLA.each(function(){         
            item = {};
            if($(this).find("input[name*='check_id']").is(':checked')){
                //item ["check_id"] = $(this).find("input[name*='check_id']").val();
                item ["personalId"] = $(this).find("input[name*='personalId']").val();
                DATA.push(item);
            }
        });
        INFO  = new FormData();
        aInfo   = JSON.stringify(DATA);
        INFO.append('data', aInfo);
        $.ajax({
            data: INFO, 
            type: 'POST',
            url : base_url + 'Empleados/registros_delete',
            processData: false, 
            contentType: false,
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success: function(data){
                tabla.ajax.reload();
                $.toast({
                    heading: 'Éxito',
                    text: 'Eliminado Correctamente',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                });
                $('#elimina_registro_modal').modal('hide');
            }
        });  
}

function btn_eliminar_registros_all(){
    $('#elimina_masivo_modal').modal();
}

function delete_registros_all(){
    $.ajax({
        type:'POST',
        url: base_url+"Empleados/delete_registros",
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Elimando Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            $('#elimina_masivo_modal').modal('hide');
            setTimeout(function(){ 
                tabla.ajax.reload(); 
            }, 1000); 
        }
    });  
}

function delete_registros_all_particular(){
    $.ajax({
        type:'POST',
        url: base_url+"Empleados/delete_registros_particular",
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Elimando Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            $('#elimina_masivo_modal').modal('hide');
            setTimeout(function(){ 
                tabla.ajax.reload(); 
            }, 1000); 
        }
    });  
}

function cargar_malla(){
    $("#carga_malla_modal").modal();

    $("#inputFile").fileinput({
        showCaption: false,
        showUpload: false, // quita el boton de upload
        allowedFileExtensions: ["csv"],
        browseLabel: 'Seleccionar Archivo',
        uploadUrl: base_url + 'Empleados/guardar_malla_empleados',
        maxFilePreviewSize: 5000,
        previewFileIconSettings: {
            'csv': '<i class="fa fa-file-excel-o text-success"></i>',
        },
        uploadExtraData: function (previewId, index) {
            var info = { };
            return info;
        }
    }).on('filebatchuploadcomplete', function (event, files, extra) {

        $.toast({
            heading: 'Éxito',
            text: 'Cargado Correctamente',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'success',
            hideAfter: 3500, 
            stack: 6
        });

        $("#carga_malla_modal").modal("hide");
        $('#inputFile').fileinput('clear');

        tabla.ajax.reload();
        
        unblockUI();
    });
}

function aceptar_malla(){
    if($("#inputFile").val()!=""){
        $('#inputFile').fileinput('upload');
        //$("#carga_malla_modal").modal("hide");
        blockUI();
    }else{
        $.toast({
            heading: 'Importante',
            text: 'Elija un archivo a cargar',
            position: 'top-right',
            icon: 'info',
            hideAfter: 3500, 
            stack: 6
        });
    }
}

function blockUI() {
    $.blockUI({
        message: "<h2>Importando datos <img src='public/img/loading.gif' width='100' heigth='100' ></h2>",
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff',
            baseZ: 5000
        }
    });
}

function unblockUI() {
    $.unblockUI();
}


function checkSelect(){
    tabla.destroy();
    table($('#clientes').val());
    console.log('c:' +$('#clientes').val());
}

function checkSelectRuta() {
    if ($('#clientes').val() > 0) {
        $('#rutas').removeAttr("disabled");

        $.ajax({
            type: 'POST',
            url: base_url + 'Reportes/get_select_rutas',
            data: {
                idC: $('#clientes').val()
            },
            statusCode: {
                404: function (data) {
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg: '#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function () {
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg: '#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success: function (data) {
                //var id = data;
                console.log(data);

                // Limpiar lista de resultados previa
                $('#rutas').empty();
                $('#rutas').append('<option value="0" selected="">Todos</option>');

                response = JSON.parse(data);

                // Recorrer cada elemento en los datos recibidos
                $.each(response, function (index, item) {
                    // Crear un nuevo elemento de lista y agregarlo a la lista de resultados
                    $("#rutas").append('<option value="' + item.id + '">' + item.ruta + '</option>');
                });
            }
        });
    } else {
        $('#rutas').val(0);
        $('#rutas').attr("disabled", "disabled");
        
    }
}
