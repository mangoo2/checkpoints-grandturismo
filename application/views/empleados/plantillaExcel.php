<?php
	header("Content-Type: text/html;charset=UTF-8");
	header("Pragma: public");
	header("Expires:0");
	header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	header("Content-Type: application/download");
	header("Content-Type: application/vnd.ms-excel;");
	header("Content-Disposition: attachment; filename=plantilla_excel".date("Ymd").".xls");
?>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<table border="1">
		<thead>
			<tr>
				<th align="center">ID (0 = empleado nvo)</th>
				<th align="center">NOMBRE (obligatorio)</th>
				<th align="center">APELLIDO PATERNO</th>
				<th align="center">APELLIDO MATERNO</th>
				<th align="center">OPERATIVA</th>
				<th align="center">TELEFONO EMERGENCIA</th>
				<th align="center">PERSONA CONTACTO</th>
				<th align="center">PARENTESCO</th>
				<th align="center">TIPO SANGRE</th>
				<?php if($esAdmin == 1){ ?>
					<th align="center">ID DE CLIENTE</th>
				<?php } ?>
				
			</tr>
		</thead>
		<tbody>
			<tr>
				<th align="center"></th>
				<th align="center"></th>
				<th align="center"></th>
				<th align="center"></th>
				<th align="center"></th>
				<th align="center"></th>
				<th align="center"></th>
				<th align="center"></th>
				<th align="center"></th>
				<?php if($esAdmin == 1){ ?>
					<th align="center"></th>
				<?php } ?>
			</tr>
		</tbody>
	</table>