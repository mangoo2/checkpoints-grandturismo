var base_url = $('#base_url').val();
var perfil = $('#perfil').val();
var tabla;
var tabla_entrenamiento;
var aux_idcliente = 0;

$(document).ready(function () {
    $('#foto_avatar').change(function(){
        document.getElementById('img_avatar').src = window.URL.createObjectURL(this.files[0]);
    });

    table();
});

function modal_cliente() {
    $('#clienteId').val('0');
    
    $('#nombre').val('');
    $('#appaterno').val('');
    $('#apmaterno').val('');

    $('#razon_social').val('');
    $('#rfc').val('');
    $('#telefono').val('');
    $('#correo').val('');
    $('#direccion').val('');
    $('#area').val('');
    $('#color').val('');

    $('.foto_client').html('<img id="img_avatar" style="width: 145px; height: 145px; border-radius: 70px;" src="' + base_url + 'images/annon.png" accept=".jpg, .jpeg, .png">');
    $('#registro_cliente').modal();
}

function guarda_cliente() {
    var form_register = $('#form_cliente');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1 = form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre: {
                required: true
            },
            telefono: {
                maxlength: 10,
                minlength: 10,
                digits: true
            },
        },

        messages: {
            telefono: {
                maxlength: "Ingrese número telefónico de 10 dígitos.",
                digits: "Por favor, ingresa solo números"
            }
        },

        errorPlacement: function (error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")) {
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit              
            success_register.fadeOut(500);
            error_register.fadeIn(500);
            scrollTo(form_register, -100);

        },

        highlight: function (element) { // hightlight error inputs

            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_cliente").valid();

    if ($valid) {
        $('.btn_registro').attr('disabled', true);
        var datos = form_register.serialize();
        $.ajax({
            type: 'POST',
            url: base_url + 'Clientes/registra_cliente',
            data: datos,
            statusCode: {
                404: function (data) {
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg: '#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function () {
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg: '#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success: function (data) {
                var id = data;
                add_file(id);
                $.toast({
                    heading: 'Éxito',
                    text: 'Guardado Correctamente',
                    position: 'top-right',
                    loaderBg: '#ff6849',
                    icon: 'success',
                    hideAfter: 3500,
                    stack: 6
                });

                setTimeout(function(){ 
                    reload_registro()

                    $('#registro_cliente').modal('hide');

                    $('.btn_registro').attr('disabled',false);
                    
                }, 1500);

                /*
                setTimeout(function () {
                    window.location = base_url + 'Clientes';
                }, 1500);
                */
            }
        });
    }
}

function reload_registro() {
    tabla.destroy();
    table();
}

function table() {
    var cont = 0;
    tabla = $("#table_datos").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url + "Clientes/getlistado",
            type: "post",
            "data": {
                cliente: $('#cliente_busqueda').val(),
                particular: '0',
            },
            error: function () {
                $("#table_datos").css("display", "none");
            }
        },
        "columns": [{
                "data": null,
                "render": function (data, type, row, meta) {
                    var html = '</div>\
                        <div class="custom-control custom-checkbox mr-sm-2 mb-3">\
                            <input type="checkbox" class="custom-control-input check_id" name="check_id" id="check_id_' + cont + '">\
                            <label class="custom-control-label" for="check_id_' + cont + '"></label>\
                            <input type="hidden" name="clienteId" value="' + row.clienteId + '">\
                        </div>';
                    cont++;
                    return html;
                }
            },
            {
                "data": "clienteId"
            },
            {
                "data": null,
                "render": function (row) {
                    return row.nombre + ' ' + row.appaterno + ' ' + row.apmaterno;
                }
            },
            {
                "data": "razon_social"
            },
            {
                "data": "telefono"
            },
            {
                "data": "correo"
            },
            {
                "data": null,
                "render": function (data, type, row, meta) {
                    //console.log("row "+ JSON.stringify(row));

                    var color_usuario = '';
                    if (row.usuario == 1) {
                        color_usuario = 'btn-warning';
                    } else {
                        color_usuario = 'btn-secondary';
                    }

                    var html = '';
                        
                    html += '<button type="button"\
                        data-cliid="' + row.clienteId + '"\
                        data-nombre="' + row.nombre + '"\
                        data-appaterno="' + row.appaterno + '"\
                        data-apmaterno="' + row.apmaterno + '"\
                        data-razon_social="' + row.razon_social + '"\
                        data-rfc="' + row.rfc + '"\
                        data-telefono="' + row.telefono + '"\
                        data-correo="' + row.correo + '"\
                        data-direccion="' + row.direccion + '"\
                        data-area="' + row.area + '"\
                        data-color="' + row.color + '"\
                        data-foto="' + row.foto + '"\
                        class="btn btn_sistema btn-circle cliente_datos_' + row.clienteId + '" onclick="modal_editar(' + row.clienteId + ')"><i class="far fa-edit"></i></button>';
                        
                    html += '<button type="button" class="btn ' + color_usuario + ' btn-circle" onclick="modal_usuario(' + row.clienteId + ');"><i class="fas fa-user"></i> </button>';

                    html += '<button type="button" class="btn btn-danger btn-circle" onclick="eliminar_cliente(' + row.clienteId + ');"><i class="fas fa-trash-alt"></i> </button>';

                    if(perfil != 1){
                        html = '';
                    }
                    
                    return html;

                }
            },
        ],
        "order": [
            [0, "desc"]
        ],
        "lengthMenu": [
            [10, 25, 50],
            [10, 25, 50]
        ],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
            }
        }

    });
}

function modal_editar(id) {
    $('#clienteId').val($('.cliente_datos_' + id).data('cliid'));
    
    $('#nombre').val($('.cliente_datos_' + id).data('nombre'));
    $('#appaterno').val($('.cliente_datos_' + id).data('appaterno'));
    $('#apmaterno').val($('.cliente_datos_' + id).data('apmaterno'));
    $('#razon_social').val($('.cliente_datos_' + id).data('razon_social'));
    $('#rfc').val($('.cliente_datos_' + id).data('rfc'));
    $('#telefono').val($('.cliente_datos_' + id).data('telefono'));
    $('#correo').val($('.cliente_datos_' + id).data('correo'));
    $('#direccion').val($('.cliente_datos_' + id).data('direccion'));
    $('#area').val($('.cliente_datos_' + id).data('area'));
    $('#color').val($('.cliente_datos_' + id).data('color'));

    var foto = $('.cliente_datos_' + id).data('foto');
    if (foto != '') {
        $('.foto_client').html('<img id="img_avatar" style="width: 145px; height: 145px; border-radius: 70px;" src="' + base_url + 'uploads/clientes/' + foto + '" accept=".jpg, .jpeg, .png">');
    } else {
        $('.foto_client').html('<img id="img_avatar" style="width: 145px; height: 145px; border-radius: 70px;" src="' + base_url + 'images/annon.png" accept=".jpg, .jpeg, .png">');
    }



    $('#registro_cliente').modal();
}

function check_baja_btn() {
    if ($('#verificar_check').is(':checked')) {
        $('.baja_texto').css('display', 'block');
    } else {
        $('.baja_texto').css('display', 'none');
    }
}

function eliminar_cliente(id) {
    $('#id_cliente').val(id);
    $('#elimina_cliente_modal').modal();
}

function delete_cliente() {
    var idp = $('#id_cliente').val();
    $.ajax({
        type: 'POST',
        url: base_url + "Clientes/delete_cliente",
        data: {
            id: idp
        },
        statusCode: {
            404: function (data) {
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg: '#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function () {
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg: '#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success: function (data) {
            $.toast({
                heading: 'Éxito',
                text: 'Elimando Correctamente',
                position: 'top-right',
                loaderBg: '#ff6849',
                icon: 'success',
                hideAfter: 3500,
                stack: 6
            });
            $('#elimina_cliente_modal').modal('hide');
            setTimeout(function () {
                tabla.ajax.reload();
            }, 1000);
        }
    });
}

function modal_usuario(id) {
    $('#usuario_modal').modal();
    $('#idCli').val(id);

    $.ajax({
        type: 'POST',
        url: base_url + 'Clientes/registro_usuario',
        data: {
            id: id
        },
        statusCode: {
            404: function (data) {
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg: '#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function () {
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg: '#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success: function (data) {
            $('.text_usuario').html(data);

            if ($('#usuario').val() == '') {
                $('#perfilId').val($('#tipoCli').val());
            }
        }
    });
}

function guardar_usuario() {
    var form_register = $('#form_usuario');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1 = form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            usuario: {
                required: true
            },
            contrasena: {
                required: true
            },
            contrasena2: {
                equalTo: contrasena,
                required: true
            },
            clienteId: {
                required: true
            },
        },

        errorPlacement: function (error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")) {
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit              
            success_register.fadeOut(500);
            error_register.fadeIn(500);
            scrollTo(form_register, -100);

        },

        highlight: function (element) { // hightlight error inputs

            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_usuario").valid();
    if ($valid) {
        $('.btn_registro').attr('disabled', true);
        var datos = form_register.serialize();
        $.ajax({
            type: 'POST',
            url: base_url + 'Clientes/add_usuarios',
            data: datos,
            statusCode: {
                404: function (data) {
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg: '#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function () {
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg: '#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success: function (data) {
                $.toast({
                    heading: 'Éxito',
                    text: 'Guardado Correctamente',
                    position: 'top-right',
                    loaderBg: '#ff6849',
                    icon: 'success',
                    hideAfter: 3500,
                    stack: 6
                });
                $('#usuario_modal').modal('hide');
                setTimeout(function () {
                    $('.btn_registro').attr('disabled', false);
                }, 1000);
                reload_registro();
            }
        });
    }
}

function verificar_usuario(id) {
    $.ajax({
        type: 'POST',
        url: base_url + 'Clientes/validar',
        data: {
            Usuario: $('#usuario').val(),
            Id:$('#idCli').val(),
        },
        async: false,
        statusCode: {
            404: function (data) {
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg: '#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function () {
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg: '#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success: function (data) {
            if (data == 1) {
                $('.txt_usuario').html('El usuario ya existe');
                $('#btn_modal_usu').prop('disabled', true);
                $.toast({
                    heading: '¡Atención!',
                    text: 'El usuario ya existe',
                    position: 'top-right',
                    loaderBg: '#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });

            } else {
                $('.txt_usuario').html('');
                $('#btn_modal_usu').prop('disabled', false);
            }
        }
    });
}

function imprimir_gafete(id) {
    $.toast({
        heading: 'Éxito',
        text: 'Por favor, espere',
        position: 'top-right',
        loaderBg: '#ff6849',
        icon: 'success',
        hideAfter: 3500,
        stack: 2
    });

    //$('.iframe_datos').html('');
    var htmlt = '<iframe src="' + base_url + 'Clientes/gafete/' + id + '"></iframe>';
    setTimeout(function () {
        $('.iframe_datos').html(htmlt);
    }, 500);
}

function add_file(id) {
    //console.log("archivo: "+archivo);
    //console.log("name: "+name);
    var archivo = $('#foto_avatar').val()
    //var name=$('#foto_avatar').val()
    extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
    extensiones_permitidas = new Array(".jpeg", ".png", ".jpg");
    permitida = false;
    if ($('#foto_avatar')[0].files.length > 0) {
        for (var i = 0; i < extensiones_permitidas.length; i++) {
            if (extensiones_permitidas[i] == extension) {
                permitida = true;
                break;
            }
        }
        if (permitida == true) {
            //console.log("tamaño permitido");
            var inputFileImage = document.getElementById('foto_avatar');
            var file = inputFileImage.files[0];
            var data = new FormData();
            data.append('foto', file);
            data.append('id', id);
            $.ajax({
                url: base_url + 'Clientes/cargafiles',
                type: 'POST',
                contentType: false,
                data: data,
                processData: false,
                cache: false,
                success: function (data) {
                    var array = $.parseJSON(data);
                    //console.log(array.ok);
                    /*
                    if (array.ok=true) {
                        swal("Éxito", "Guardado Correctamente", "success");
                    }else{
                        swal("Error!", "No Se encuentra el archivo", "error");
                    }
                    */
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    var data = JSON.parse(jqXHR.responseText);
                }
            });
        }
    }
}
//===========================================================
function btn_check_todo() {
    if ($('#check_todo').is(':checked')) {
        $('.check_id').prop('checked', true);
    } else {
        $('.check_id').prop('checked', false);
    }
}

function btn_eliminar_registros() {
    $('#elimina_registro_modal').modal();
}

function delete_registros() {
    var DATA = [];
    var TABLA = $("#table_datos tbody > tr");
    TABLA.each(function () {
        item = {};
        if ($(this).find("input[name*='check_id']").is(':checked')) {
            console.log("CHECK");
            //item ["check_id"] = $(this).find("input[name*='check_id']").val();
            item["clienteId"] = $(this).find("input[name*='clienteId']").val();
            DATA.push(item);
        }
    });
    INFO = new FormData();
    aInfo = JSON.stringify(DATA);
    INFO.append('data', aInfo);

    $.ajax({
        data: INFO,
        type: 'POST',
        url: base_url + 'Clientes/registros_delete',
        processData: false,
        contentType: false,
        async: false,
        statusCode: {
            404: function (data) {
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function () {
                toastr.error('Error', '500');
            }
        },
        success: function (data) {
            tabla.ajax.reload();
            $.toast({
                heading: 'Éxito',
                text: 'Eliminado Correctamente',
                position: 'top-right',
                loaderBg: '#ff6849',
                icon: 'success',
                hideAfter: 3500,
                stack: 6
            });
            $('#elimina_registro_modal').modal('hide');
        }
    });
}

function btn_eliminar_registros_all() {
    $('#elimina_masivo_modal').modal();
}

function delete_registros_all() {
    $.ajax({
        type: 'POST',
        url: base_url + "Clientes/delete_registros",
        statusCode: {
            404: function (data) {
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg: '#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function () {
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg: '#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success: function (data) {
            $.toast({
                heading: 'Éxito',
                text: 'Elimando Correctamente',
                position: 'top-right',
                loaderBg: '#ff6849',
                icon: 'success',
                hideAfter: 3500,
                stack: 6
            });
            $('#elimina_masivo_modal').modal('hide');
            setTimeout(function () {
                tabla.ajax.reload();
            }, 1000);
        }
    });
}