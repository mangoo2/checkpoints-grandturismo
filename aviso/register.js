$(function(){
	initRegister();
	$("#patientRegister").submit(sendForm);
	$("#medicRegister").submit(sendForm);
	$("#options div").click(function() { 
		$(this).addClass("active").siblings().removeClass("active");
		$(".fields").hide().find("input,select").attr("disabled", "disabled"); 
		var id = $(this).attr("id"); 
		$("#" + id +"Fields").show().find("input,select").removeAttr("disabled"); 
		$("#medicRegister").attr("action", "/register/" + id);
		
	});
	
	//$("#registerButton").on("click", validateRecaptcha);
	
	$("div#medic").click(function(){
		   $("div#secretary").addClass("medic"); 
		   $("div#medic").removeClass("secretary"); 
	});
	$("div#secretary").click(function(){
		   $("div#medic").addClass("secretary"); 
		   $("div#secretary").removeClass("medic"); 
	});
	
/**/
	
	 
	/*$('select#meetElonor').change(function() {
			  var value = $(" #meetEleonor option:selected ").val();
			  alert(value);
			});*/
	$("div#medic").trigger("click");
	
	$(".showPass").click(function() {
		 var parents=$(this).closest("form");
	    if ( parents.find("#password").attr("type") == "password") {
	    	parents.find("#password").attr("type", "text");
	    	parents.find("label[for='showPass']").removeClass("text-greenEleonor");
	    	parents.find("label[for='showPass']").addClass("text-green");
	    	$(".glyphicon").removeClass("glyphicon-eye-close");
		    $(".glyphicon").addClass("glyphicon-eye-open");
	    

	    } else {
	    	parents.find("#password").attr("type", "password");
	    	parents.find("label[for='showPass']").removeClass("text-green");
	    	$(".glyphicon").removeClass("glyphicon-eye-open");
		    $(".glyphicon").addClass("glyphicon-eye-close");
	    

	    }
	  });

	
	$("#email,#smail").change(function(){
		var field = $(this);
		if ( $(this).val() === '') {
			field.siblings("div.text_mail .textChange").html("<span> Correo:</span>" );
			/*$(".text_mail").text("Correo:");*/
			
	    }	
		if(validMail($(this)) == false ) { 
			$("#registerButton").attr("disabled", "disabled").addClass("dDisabled");
			$(this).focus();
		} else {
			verifyMail($(this), function(response){
				if (response.rol) {
					field.focus();
					field.siblings("div.text_mail .textChange").html("<span> Correo:</span><p class='noticeImportant text-sm animated shake'> Este correo ya existe en Elonor, por favor utilice otro correo</p>")
					field.closest("#registerForm").find("#registerButton").attr("disabled", "disabled").addClass("dDisabled").removeClass("VregisterButton");
					/*$("#register #rf h1").text(" :( Oops éste correo ya está registrado ").addClass("noticeImportant");
					$("#register #rf h3.noticeImportant").text("Seleccione otro correo y contraseña");*/
					/*$("#register #rf h3.slogan").text("");*/
				    
					
				} else {
					
					field.focus().siblings("div.text_mail .textChange").html("<span> Correo:</span>");
					field.focus().closest("#registerForm").find("#registerButton").removeAttr("disabled").addClass("VregisterButton").removeClass("dDisabled");
					
					/*$("#register #rf h3.noticeImportant").text("");*/
					/*$("#register #rf h3.slogan").text("Un respiro, una nueva experiencia en tu consulta");*/
				
				}
			});		
		}
	});
	
	/* Ocultar campos de correo prellenados por chrome*/
	setTimeout(function() {
		$("input.chromefiller").hide();
	}, 1);
});

function validateRecaptcha(){
	var id = $("#options div.active").attr("id"),
		form = $("#" + id + "Register");
	if (isValidForm("#" + id + "Register", true) && !form.data("registering"))  {
		form.data("registering", true);
		$.ajax({
			url: "/register/recaptcha/validate",
			data: $("#g-recaptcha-response").val(),
			type: "POST",
			global: false,
			success: function(response){
				if (response.success && response.score > 0.5){ 
					executeRegister();
				} else {
					form.data("registering", false);
					alert("Debe verificar que no es un robot");
					//grecaptcha.reset();
					grecaptcha.execute('6LdM-_8UAAAAAB8dWUM94AOtac8QsOERQfQndS2Q', {action: 'submit'}).then(function(token) {
						console.log(token);
					});
				}
			}
		});	
	} else {
		grecaptcha.reset();
		form.data("registering", false);
	}
}

function executeRegister() {
	var id = $("#options div.active").attr("id");
	$("form").each(function(i, form){
		form = $(form);
		if(form.find(".showPass").is(':checked')) {   	
			form.find("#password").attr("type", "password");
			form.find("label[for='showPass']").removeClass("text-green");
			form.find("label[for='showPass']").addClass("text-greenEleonor");
			form.find(".showPass").prop("checked",false);  
	    }
	});
 
	if (isValidForm("#" + id + "Register", true)) {
		if ($("#acept").is(":checked")) {
			$("#rf").hide();
			$("#ld").show();
			$("#" + id + "Register").submit();
		} else {
			alert("Debe aceptar los términos y condiciones y el aviso de privacidad", "error");
			grecaptcha.reset();
			return false;
		}
	} else {
		grecaptcha.reset();
	}
}

/*function validMail(mailField){
	var mail = mailField.val().toLowerCase(),
		re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	mailField.val(mail);
	return re.test(mail); 
}*/

function initRegister(){
	$("select#meetEleonor").change(function(){
		var typeSelected = $("#meetEleonor option:selected").val();
		if(typeSelected == "meetOther") {
			console.log(typeSelected);
				$("#op_meetOther").show();
			}else{
				$("#op_meetOther").hide();
				
				
			}
	});
}
		
function sendForm(token){
	if (!$("#age").val()) {
		$("#age").val("0");
	}
	if ($("#acept").is(":checked")) {
		$("#rf").hide();
		$("#ld").show();
	} else {
		alert("Debe aceptar los términos y condiciones y el aviso de privacidad", "error");
		return false;
	}
	try {
		var esp = $("select[name=specialism_name] option:selected").text().toLowerCase(); 
		if (esp.indexOf("pediát") >= 0 || esp.indexOf("pediat") >= 0) {
			gtag_report_conversion("AW-829699287/Z9n_CN2Y3XcQ1-nQiwM");
		} else {
			gtag_report_conversion("AW-829699287/t6t1COzO8HcQ1-nQiwM");
		}
		ga('send', 'event', { eventCategory: 'Registro', eventAction: 'clic', eventLabel: 'lead'});
	} catch (ex){}
}

function valid(){
	var filled = $("#email").val() && $("#password").val() && $("#acept").is(":checked");
	return filled;
}