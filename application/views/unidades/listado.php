<input type="hidden" id="perfil" value="<?php echo $perfil ?>">
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12"><br>
                <div class="card">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6 mb-2">
                                        <button type="button" class="btn btn-outline-info"  onclick="modal_registro()"><i class="fas fa-car-side"></i> Nueva Unidad</button> 
                                    </div>
                                    <div class="col-md-6">    
                                        <div class="input-group bootstrap-touchspin">
                                            <span class="input-group-text">Buscar unidades &nbsp;&nbsp; <i class="fas fa-search"></i></span></span><input id="unidades" type="text" class="form-control" placeholder="Escriba nombre de la unidad" oninput="reload_registro()">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12"></div>
                                        <table class="table" id="table_datos">
                                            <thead class="bg-blue">
                                                <tr>
                                                    <th>ID</th> 
                                                    <th>Unidad</th>
                                                    <th>Marca</th>
                                                    <th>Modelo</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>    
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>            

<div id="registro_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header modal_sistema">
                <div class="col-lg-1"></div>
                <div class="col-lg-10" align="center">
                <h4 class="modal-title">Registro de unidades</h4>
                </div>
                <div class="col-lg-1">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <form class="form" method="post" role="form" id="form_registro">
                                    <input type="hidden" name="id" id="id_unidades" value="0"> 
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label><span style="color: red;">*</span> Unidad</label>
                                                <input type="text" name="unidad" id="unidad" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label><span style="color: red;">*</span> Marca</label>
                                                <input type="text" name="marca" id="marca" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label><span style="color: red;">*</span> Modelo</label>
                                                <input type="text" name="modelo" id="modelo" class="form-control">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label><span style="color: red;">*</span> Placas</label>
                                                <input type="text" name="placas" id="placas" class="form-control">
                                                <input type="hidden" name="aux_placas" id="aux_placas" class="form-control" readonly>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </form>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info waves-effect btn_registro" onclick="guarda_registro()">Guardar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div id="elimina_registro_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea borrar ésta unidad? Esta operación no se puede deshacer.</h5>
            </div>
            <input type="hidden" id="id_points">
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="registro_delete()">Aceptar</button>
            </div>
        </div>   
    </div>
</div>
