<input type="hidden" id="perfil" value="<?php echo $perfil ?>">
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12"><br>
                <div class="card">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <!--<button type="button" class="btn btn-outline-info" onclick="modal_ruta()"><i class="fas fa-user-plus"></i> Seleccionar Ruta</button>-->
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group bootstrap-touchspin">
                                            <span class="input-group-text">Buscar Rutas &nbsp;&nbsp; <i class="fas fa-search"></i></span></span><input id="ruta_busqueda" type="text" class="form-control" placeholder="Escriba nombre de la ruta" oninput="reload_registro()">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <table class="table" id="table_datos">
                                            <thead class="bg-blue">
                                                <tr>
                                                    <th>Ruta</th>
                                                    <th>Unidad</th>
                                                    <th>Chofer</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<div id="datos_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl">
        <div class="modal-content">
            <div class="modal-header modal_sistema">
                <div class="col-lg-1"></div>
                <div class="col-lg-10" align="center">
                    <h4 class="modal-title" id="label_datos">Personal de ruta "*****"</h4>
                </div>
                <div class="col-lg-1">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
            </div>
            <div class="modal-body">

                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <form class="form" method="post" role="form" id="form_ruta">
                                <input type="hidden" name="id" id="rutaClienteId" value="0">
                                <input type="hidden" name="rutaId" id="rutaId" value="0">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label>Ruta</label>
                                                    <select class="form-control" id="selRuta" disabled>
                                                        <option disabled selected value="0">Selecciona una opción</option>
                                                        <?php foreach ($rutas as $r) {
                                                            echo '<option value="' . $r->id . '">' . $r->ruta . '</option>';
                                                        } ?>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label>Personal</label>
                                                    <select class="form-control" name="empleadoId" id="empleadoId">
                                                        <option disabled selected value="0">Selecciona una opción</option>
                                                        <?php foreach ($personal as $p) {
                                                            echo '<option value="' . $p->personalId . '">' . $p->nombre . ' ' . $p->appaterno . ' ' . $p->apmaterno . '</option>';
                                                        } ?>
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="col-md-">
                                                <?php if ($perfil == 1) { ?>
                                                    <label>Cliente</label>
                                                    <input type="text" value="<?php echo $cliente->nombre; ?>" disabled>
                                                <?php } ?>
                                            </div>

                                            <div class="col-md-2">
                                                <label>Acciones</label>
                                                <br>
                                                <button type="button" class="btn btn-info waves-effect btn_registro" title="Agregar personal" onclick="guarda_ruta()"><i class="fa fa-save" aria-hidden="true"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" id="row_clientes_rutas">
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <hr>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group bootstrap-touchspin">
                            <span class="input-group-text">Buscar Personal &nbsp;&nbsp; <i class="fas fa-search"></i></span></span><input id="rucli_busqueda" type="text" class="form-control" placeholder="Escriba nombre del empleado" oninput="reload_table_modal()">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <table class="table" id="table_rutas_clientes">
                            <thead class="bg-blue">
                                <tr>
                                    <th>Ruta</th>
                                    <th>Personal</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                
            </div>

            <input type="hidden" id="id_ruta">
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!--
<div id="registro_ruta" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header modal_sistema">
                <div class="col-lg-1"></div>
                <div class="col-lg-10" align="center">
                    <h4 class="modal-title">Seleccion de ruta</h4>
                </div>
                <div class="col-lg-1">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <form class="form" method="post" role="form" id="form_ruta">
                                    <input type="hidden" name="id" id="rutaClienteId" value="0">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Personal</label>
                                                        <select class="form-control" name="empleadoId" id="empleadoId">
                                                            <option disabled selected value="0">Selecciona una opción</option>
                                                            <?php foreach ($personal as $p) {
                                                                echo '<option value="' . $p->personalId . '">' . $p->nombre . ' ' . $p->appaterno . ' ' . $p->apmaterno . '</option>';
                                                            } ?>
                                                        </select>

                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Ruta</label>
                                                        <select class="form-control" name="rutaId" id="rutaIds">
                                                            <option disabled selected value="0">Selecciona una opción</option>
                                                            <?php foreach ($rutas as $r) {
                                                                echo '<option value="' . $r->id . '">' . $r->ruta . '</option>';
                                                            } ?>
                                                        </select>

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Tipo</label>
                                                        <select class="form-control" name="tipo" id="tipo">
                                                            <option value="0">Bajada</option>
                                                            <option value="1">Subida</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <?php /* if($perfil == 1){ */ ?>
                                                    <div class="form-group">
                                                        <label>Cliente</label>
                                                        <select class="form-control" name="clienteId" id="clienteId">
                                                            <option value="0">Ninguno</option>
                                                            <?php foreach ($clientes as $c) {
                                                                echo '<option value="' . $c->clienteId . '">' . $c->nombre . ' ' . $c->appaterno . '</option>';
                                                            } ?>
                                                        </select>
                                                    </div>
                                                    <?php /* } */ ?>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info waves-effect btn_registro" onclick="guarda_ruta()">Guardar</button>
            </div>
        </div>
    </div>
</div>
-->

<div id="elimina_ruta_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea borrar este registro? Esta operación no se puede deshacer.</h5>
            </div>
            <input type="hidden" id="id_ruta">
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="delete_cliru()">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div id="elimina_registro_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea borrar estos registros? Esta operación no se puede deshacer.</h5>
            </div>
            <input type="hidden" id="id_ruta">
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="delete_registros()">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div id="elimina_masivo_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea borrar todos los registros? Esta operación no se puede deshacer.</h5>
            </div>
            <input type="hidden" id="id_ruta">
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="delete_registros_all()">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="iframe_datos" style="display: none;">
</div>