<style type="text/css">
    .dt-buttons .dt-button {
        padding: 5px 15px;
        border-radius: .25rem;
        background: #026934 !important;
        color: #fff;
        margin-right: 3px;
    }

    #map {
        height: 50vh;
        width: 100vw;
    }

</style>
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12"><br>
                <div class="card">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3>Registro de check points</h3>
                                        <hr>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Fecha inicio</label>
                                            <input type="date" id="fecha_inicio" class="form-control" onchange="fechas_in_fi()">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Fecha fin</label>
                                            <?php
                                                $today = date('Y-m-d');
                                                echo '<input type="date" id="fecha_fin" class="form-control" max="' . $today . '" onchange="fechas_in_fi()">';
                                            ?>
                                            <!--
                                            <input type="date" id="fecha_fin" class="form-control" onchange="fechas_in_fi()">
                                            -->
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <br/>
                                        <label>Fecha actual</label>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox mr-sm-2">
                                                <br/>
                                                <input type="hidden" id="fecha_hoy" value="<?php echo $fecha_hoy ?>">
                                                <input type="checkbox" class="custom-control-input" id="fecha_actual" value="check" onclick="fecha_actual()">
                                                <label class="custom-control-label" for="fecha_actual"></label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Seleccionar Cliente</label>
                                            <input type="hidden" id="idCli" value="<?php echo $cliente ?>">
                                            <select id="clientes" class="form-control" onchange="fechas_in_fi(1)">
                                                <option value="0" selected="">Todos</option>
                                                <?php $result=$this->General_model->getselectwhere('clientes','estatus',1);
                                                foreach ($result as $item){
                                                    echo '<option value="'.$item->clienteId.'">'.$item->nombre.' '.$item->appaterno.' '.$item->apmaterno.'</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Seleccionar Ruta</label>
                                            <select id="rutas" class="form-control" onchange="fechas_in_fi()" disabled="disabled">
                                                <option value="0" selected="">Todas</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row my-2">
                                    <div class="col-md-6 mt-4">
                                        <button type="button" class="btn waves-effect waves-light btn-sm btn_sistema p-2" onclick="get_excel()">
                                            Excel
                                        </button>
                                        <button type="button" class="btn waves-effect waves-light btn-sm btn_sistema p-2" onclick="get_pdf()">
                                            PDF
                                        </button>
                                        <button type="button" class="btn waves-effect waves-light btn-sm btn_sistema p-2" onclick="get_impresion()">
                                            Imprimir
                                        </button>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Seleccionar check points</label>
                                            <select id="points_check" class="form-control" onchange="fechas_in_fi()">
                                                <option value="0" selected="">Todos</option>
                                                <?php $result=$this->General_model->getselectwhere('check_points','activo',1);
                                                foreach ($result as $item){
                                                    echo '<option value="'.$item->id.'">'.$item->nombre.'</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Buscar</label>
                                            <input type="text" id="buscar" class="form-control" oninput="fechas_in_fi()">
                                        </div>
                                    </div>

                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-12"></div>
                                        <table class="table" id="table_datos">
                                            <thead class="bg-blue">
                                                <tr>
                                                    <th rowspan="">ID</th> 
                                                    <th>Fecha y hora</th>
                                                    <th>Ubicación</th>
                                                    <th>Latitud</th>
                                                    <th>Longitud</th>
                                                    <th>Empleado</th>
                                                    <th>Ruta</th>
                                                    <th>Cliente</th>
                                                    <th>Check points</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                        
                                    </div>
                                    <div class="col-md-12 iframepoints" style="display: none;"></div>
                                </div>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>


<div id="modal_ubicacion" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header modal_sistema">
                <div class="col-lg-1"></div>
                <div class="col-lg-10" align="center">
                    <h4 class="modal-title">Ubicación de Checkpoint</h4>
                </div>
                <div class="col-lg-1">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div id="map" class="col-md-12">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>