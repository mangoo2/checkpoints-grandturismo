var base_url = $('#base_url').val();
var tabla;
var map;

$(document).ready(function() {
	//table();
    checkCliente();
});

function reload_registro(){
	tabla=$("#table_datos").DataTable();
    tabla.destroy();
    table();
}

function checkCliente(){
    var idCli = $("#idCli").val();
    console.log("CheckCliente");
    if ( idCli != 0 ){
        setTimeout(function() {
            $('#clientes').val(idCli).trigger('change');
            $('#clientes').prop('disabled', true);
        }, 500);
    }
}

function table(){
	var points_check = $('#points_check').val();

    var clientes = $('#clientes').val();
    var rutas = $('#rutas').val();
    //var operadores = $('#operadores').val();
    var operadores = 0;

	var fecha_inicio = $('#fecha_inicio').val();
	var fecha_fin = $('#fecha_fin').val();
    var buscar = $('#buscar').val();
	tabla=$("#table_datos").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Reportes/getlistado",
            type: "post",
            "data":{fecha_inicio:fecha_inicio,fecha_fin:fecha_fin,points_check:points_check,clientes:clientes,rutas:rutas,operadores:operadores,buscar:buscar},
            error: function(){
                $("#table_datos").css("display","none");
            }
        },
        "columns": [
            {"data":"id"},
            {"data":"reg"},
            {
                "data": null,
                "render": function (data, type, row, meta) {
                    console.log("row "+ JSON.stringify(row));

                    var html = '<div style="width:100px; display:flex; justify-content:center; align-items:center;">';
                    html += '<button type="button" title="Ver mapa" class="btn btn-secondary btn-circle" onclick="modal_ubicacion(' + row.longitud + ',' + row.latitud + ', &quot;' + row.point + '&quot;);"><i class="fas fa-map"></i> </button>';
                    html += '</div>';

                    return html;
                }
            },
            {"data":"latitud"},
            {"data":"longitud"},
            {"data":"empleado"},
            {"data":"ruta"},
            {"data":"cliente"},
            {"data":"point"},
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
            },
            "buttons": {
                "copy": 'Copiar',
                "csv": 'Exportar a CSV',
                "print": 'Imprimir',
            }
        },
        /*
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
        */
    });
}
function fecha_actual(){
	var fecha_hoy=$('#fecha_hoy').val();
	if($('#fecha_actual').is(':checked')){
        $('#fecha_inicio').val(fecha_hoy);
        $('#fecha_fin').val(fecha_hoy);
        reload_registro();
	}else{
		$('#fecha_inicio').val('');
		$('#fecha_fin').val('');
	}
}

function fechas_in_fi(type = 0){

    if(type == 1 ){
        checkSelectRuta(type);
    }

    if(!$('#fecha_inicio').val() || !$('#fecha_fin').val()){
        return;
    }

    reload_registro();

}

function get_excel(){
    var points_check = $('#points_check').val();
    var clientes = $('#clientes').val();
    var rutas = $('#rutas').val();
    var operadores = $('#operadores').val();
    var f1 = $('#fecha_inicio').val();
    var f2 = $('#fecha_fin').val();
    var buscar = $('#buscar').val();
    var buscar_aux='';
    if(buscar!=''){
        buscar_aux=buscar;
    }else{
        buscar_aux='null';
    }
    if(f1!='' && f2!=''){
        location.href= base_url+'Reportes/excel_poins/'+f1+'/'+f2+'/'+points_check+'/'+clientes+'/'+rutas+'/'+operadores+'/'+buscar_aux;
    }else{
        $.toast({
            heading: '¡Ateción!',
            text: 'Tienes que seleccionar un rango de fechas',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
        });
    }
}

function get_pdf(){
    var points_check = $('#points_check').val();
    var clientes = $('#clientes').val();
    var rutas = $('#rutas').val();
    var operadores = $('#operadores').val();
    var f1 = $('#fecha_inicio').val();
    var f2 = $('#fecha_fin').val();
    var buscar = $('#buscar').val();
    var buscar_aux='';
    if(buscar!=''){
        buscar_aux=buscar;
    }else{
        buscar_aux='null';
    }
    if(f1!='' && f2!=''){

        //location.href= base_url+'Reportes/points_pdf/'+f1+'/'+f2+'/'+points_check+'/'+buscar_aux;
        window.open(base_url+'Reportes/points_pdf/'+f1+'/'+f2+'/'+points_check+'/'+clientes+'/'+rutas+'/'+operadores+'/'+buscar_aux);
        //window.open("_blank");
        //location.target = "_blank";
    }else{
        $.toast({
            heading: '¡Ateción!',
            text: 'Tienes que seleccionar un rango de fechas',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
        });
    }
}

function get_impresion(){
    var points_check = $('#points_check').val();
    var clientes = $('#clientes').val();
    var rutas = $('#rutas').val();
    var operadores = $('#operadores').val();
    var f1 = $('#fecha_inicio').val();
    var f2 = $('#fecha_fin').val();
    var buscar = $('#buscar').val();
    var buscar_aux='';
    if(buscar!=''){
        buscar_aux=buscar;
    }else{
        buscar_aux='null';
    }
    if(f1!='' && f2!=''){
        var html='<iframe src="'+base_url+'Reportes/points_inmpresion/'+f1+'/'+f2+'/'+points_check+'/'+clientes+'/'+rutas+'/'+operadores+'/'+buscar_aux+'"></iframe>';
        $('.iframepoints').html('');
        $('.iframepoints').html(html);
    }else{
        $.toast({
            heading: '¡Ateción!',
            text: 'Tienes que seleccionar un rango de fechas',
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: 'error',
            hideAfter: 3500
        });
    }
}

function checkSelectRuta() {
    if ($('#clientes').val() > 0) {
        $('#rutas').removeAttr("disabled");

        $.ajax({
            type: 'POST',
            url: base_url + 'Reportes/get_select_rutas',
            data: {
                idC: $('#clientes').val()
            },
            statusCode: {
                404: function (data) {
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg: '#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function () {
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg: '#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success: function (data) {
                //var id = data;
                console.log(data);

                // Limpiar lista de resultados previa
                $('#rutas').empty();
                $('#rutas').append('<option value="0" selected="">Todos</option>');

                response = JSON.parse(data);

                // Recorrer cada elemento en los datos recibidos
                $.each(response, function (index, item) {
                    // Crear un nuevo elemento de lista y agregarlo a la lista de resultados
                    $("#rutas").append('<option value="' + item.id + '">' + item.ruta + '</option>');
                });
            }
        });
    } else {
        $('#rutas').val(0);
        $('#rutas').attr("disabled", "disabled");
        
    }
}

function checkSelectOperador() {
    if ($('#clientes').val() > 0) {
        $('#operadores').removeAttr("disabled");

        $.ajax({
            type: 'POST',
            url: base_url + 'Reportes/get_select_operadores',
            data: {
                idC: $('#clientes').val()
            },
            statusCode: {
                404: function (data) {
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg: '#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function () {
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg: '#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success: function (data) {
                //var id = data;
                console.log("-----------------------------");
                console.log(data);
                console.log("-----------------------------");

                // Limpiar lista de resultados previa
                $('#operadores').empty();
                $('#operadores').append('<option value="0" selected="">Todos</option>');

                response = JSON.parse(data);

                // Recorrer cada elemento en los datos recibidos
                $.each(response, function (index, item) {
                    // Crear un nuevo elemento de lista y agregarlo a la lista de resultados
                    $("#operadores").append('<option value="' + item.operadorId + '">' + item.operador + '</option>');
                });
            }
        });
    } else {
        $('#operadores').attr("disabled", "disabled");
    }
}

function modal_maps(longitud = 0, latitud = 0, point = '') {
    var url1 = "https://www.google.com/maps?q=" + latitud + "," + longitud;
    //var url1 = "https://www.google.com/maps?ll=" + latitud + "," + longitud;

    window.open(url1, "Prefactura", "width=880, height=612");
}


function modal_ubicacion(longitud = 0, latitud = 0, point = "Checkpoint") {
    $('#modal_ubicacion').modal();

    if (map) {
        map.remove();
    }
    
    map = L.map('map').setView([latitud, longitud], 16);

    const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 20,
        attribution: '<a href="http://mangoo.mx/" target="_blank">Mangoo.mx</a>'
    }).addTo(map);

    const marker = L.marker([latitud, longitud]).addTo(map)
            .bindPopup(point)
            .openPopup();

    /*
    const popup = L.popup()
        .setLatLng([latitud, longitud])
        .setContent('Dar click en cualquier parte para obtener las coordenadas.')
        .openOn(map);
        
    
    function onMapClick(e) {
        popup
            .setLatLng(e.latlng)
            .setContent(`Latitud ${e.latlng.lat} <br>Longitud ${e.latlng.lng}`)
            .openOn(map);
    }

    map.on('click', onMapClick);
    */
}