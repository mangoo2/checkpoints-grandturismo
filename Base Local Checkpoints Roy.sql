-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-07-2024 a las 20:14:16
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sicoinet_guanabana`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `check_points`
--

CREATE TABLE `check_points` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `codigo` varchar(100) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1',
  `reg` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `check_points`
--

INSERT INTO `check_points` (`id`, `nombre`, `codigo`, `activo`, `reg`) VALUES
(1, 'Subida', '54321', 1, '2024-05-22 17:56:38'),
(2, 'Bajada', '123455', 1, '2024-05-22 18:05:31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `check_points_detalles`
--

CREATE TABLE `check_points_detalles` (
  `id` int(11) NOT NULL,
  `personalId` int(11) NOT NULL,
  `idpoints_check` int(11) NOT NULL,
  `idempleado` int(11) NOT NULL COMMENT 'quien guardo el registro',
  `idcliente` int(11) NOT NULL,
  `idruta` int(11) NOT NULL,
  `longitud` float NOT NULL,
  `latitud` float NOT NULL,
  `reg` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `check_points_detalles`
--

INSERT INTO `check_points_detalles` (`id`, `personalId`, `idpoints_check`, `idempleado`, `idcliente`, `idruta`, `longitud`, `latitud`, `reg`) VALUES
(48, 85, 1, 1, 9, 3, 0, 0, '2024-05-23 19:57:10'),
(49, 86, 1, 4, 9, 3, 0, 0, '2024-05-23 21:04:48'),
(50, 85, 2, 4, 9, 3, 0, 0, '2024-05-23 21:04:59'),
(52, 87, 1, 1, 9, 5, 0, 0, '2024-05-24 17:08:32'),
(53, 85, 1, 1, 9, 3, 0, 0, '2024-05-24 20:06:44'),
(54, 85, 1, 1, 9, 3, 0, 0, '2024-05-24 20:07:04'),
(55, 85, 1, 1, 9, 3, 0, 0, '2024-05-24 20:11:48'),
(56, 85, 1, 1, 9, 3, 0, 0, '2024-05-24 20:15:10'),
(57, 85, 1, 1, 9, 3, 0, 0, '2024-05-24 20:16:01'),
(58, 85, 1, 1, 9, 3, 0, 0, '2024-05-24 20:17:16'),
(59, 85, 1, 1, 9, 3, 0, 0, '2024-05-24 20:34:55'),
(60, 85, 1, 1, 9, 3, 0, 0, '2024-05-24 20:36:13'),
(61, 85, 1, 1, 9, 3, 0, 0, '2024-05-24 20:38:14'),
(62, 85, 1, 1, 9, 3, 0, 0, '2024-05-24 20:41:37'),
(63, 86, 1, 1, 9, 5, 0, 0, '2024-05-24 20:47:55'),
(64, 88, 1, 1, 10, 2, 0, 0, '2024-05-24 21:27:57'),
(65, 85, 1, 1, 9, 3, 0, 0, '2024-05-30 17:59:47'),
(66, 88, 2, 1, 10, 2, 0, 0, '2024-05-30 18:00:42'),
(67, 86, 1, 1, 9, 5, 0, 0, '2024-05-30 18:04:01'),
(68, 86, 1, 1, 9, 5, 0, 0, '2024-05-30 18:04:26'),
(69, 85, 1, 1, 9, 3, 0, 0, '2024-05-30 18:06:40'),
(70, 85, 1, 1, 9, 3, 0, 0, '2024-05-30 20:17:56'),
(71, 85, 1, 1, 9, 3, -98.1428, 19.036, '2024-05-30 20:39:56'),
(72, 85, 1, 1, 9, 3, -98.1428, 19.036, '2024-05-31 20:02:56'),
(73, 86, 1, 1, 9, 5, -98.1428, 19.036, '2024-05-31 20:41:26'),
(74, 88, 1, 1, 10, 2, -98.1428, 19.036, '2024-05-31 20:41:35'),
(75, 88, 1, 1, 10, 2, -98.1428, 19.036, '2024-05-31 21:13:28'),
(76, 88, 1, 1, 10, 2, -98.1428, 19.036, '2024-05-31 21:15:33'),
(77, 88, 1, 1, 10, 2, -98.1428, 19.036, '2024-05-31 21:19:30'),
(78, 86, 1, 1, 9, 5, -98.1425, 19.0359, '2024-06-03 19:15:14'),
(79, 85, 2, 1, 9, 3, -98.2222, 19.0536, '2024-06-03 19:52:26'),
(80, 85, 1, 1, 9, 5, -98.1428, 19.036, '2024-06-04 21:59:25'),
(81, 85, 1, 1, 9, 5, -98.1428, 19.036, '2024-06-04 22:07:06'),
(82, 85, 1, 1, 9, 5, -98.1428, 19.036, '2024-06-05 18:41:53'),
(83, 85, 1, 1, 9, 5, -98.1428, 19.036, '2024-06-05 19:02:56'),
(84, 88, 2, 1, 10, 2, -98.1428, 19.036, '2024-06-05 19:05:42'),
(85, 88, 1, 1, 10, 2, -98.1428, 19.036, '2024-06-06 18:51:57'),
(86, 88, 1, 1, 10, 2, -98.1426, 19.0364, '2024-06-07 19:01:26'),
(87, 88, 1, 1, 10, 2, -19.0365, 98.1426, '2024-06-07 19:01:33'),
(88, 85, 1, 1, 9, 5, -98.1428, 19.036, '2024-07-04 01:21:04'),
(89, 85, 1, 1, 9, 5, -98.1428, 19.036, '2024-07-04 02:06:25'),
(90, 85, 1, 1, 9, 5, -98.1428, 19.036, '2024-07-04 02:06:40'),
(91, 85, 1, 1, 9, 5, -98.1428, 19.036, '2024-07-04 02:08:46'),
(92, 85, 1, 1, 9, 5, -98.1428, 19.036, '2024-07-04 02:10:49'),
(93, 86, 1, 1, 9, 3, -98.1428, 19.036, '2024-07-08 17:37:03'),
(94, 3, 1, 1, 9, 3, -98.1428, 19.036, '2024-07-08 19:37:20'),
(95, 3, 2, 1, 9, 3, -98, 19, '2024-07-08 19:40:46'),
(96, 3, 2, 1, 9, 3, -98.1, 19, '2024-07-08 19:41:42'),
(97, 3, 1, 1, 9, 3, -98.1428, 19.036, '2024-07-08 19:56:41'),
(98, 3, 1, 1, 9, 3, -98.1428, 19.036, '2024-07-08 20:02:01'),
(99, 3, 1, 1, 9, 3, -98.1428, 19.036, '2024-07-08 20:03:47'),
(100, 3, 1, 1, 9, 3, -98.1428, 19.036, '2024-07-08 20:04:51'),
(101, 3, 1, 1, 9, 3, -98.1, 19, '2024-07-09 17:08:04'),
(102, 3, 1, 1, 9, 3, 0, 0, '2024-07-09 19:19:46'),
(103, 3, 1, 1, 9, 3, 0, 0, '2024-07-09 19:21:36'),
(104, 3, 1, 1, 9, 3, 0, 0, '2024-07-09 19:23:42'),
(105, 3, 1, 1, 9, 3, 0, 0, '2024-07-09 19:25:00'),
(106, 3, 1, 1, 9, 3, 0, 0, '2024-07-09 19:25:57'),
(107, 3, 1, 1, 9, 3, 0, 0, '2024-07-09 19:26:45'),
(113, 3, 1, 1, 9, 3, 0, 0, '2024-07-09 19:41:38'),
(114, 3, 1, 1, 9, 3, 0, 0, '2024-07-09 19:42:25'),
(115, 3, 1, 1, 9, 3, 0, 0, '2024-07-09 19:43:15'),
(116, 3, 1, 1, 9, 3, 0, 0, '2024-07-09 19:51:30'),
(117, 3, 1, 1, 9, 3, -98.1428, 19.036, '2024-07-09 19:54:41'),
(118, 3, 1, 1, 9, 3, -98.1428, 19.036, '2024-07-09 20:00:32'),
(119, 3, 1, 1, 9, 3, -98.1428, 19.036, '2024-07-09 20:00:44'),
(120, 3, 1, 1, 9, 3, -98.1428, 19.036, '2024-07-09 20:04:47'),
(121, 3, 1, 1, 9, 3, -98.1428, 19.036, '2024-07-09 20:06:21'),
(122, 3, 1, 1, 9, 3, -98.1428, 19.036, '2024-07-09 20:08:11'),
(123, 3, 2, 1, 9, 3, -98.1428, 19.036, '2024-07-09 20:08:41'),
(124, 3, 2, 1, 9, 3, -98.1428, 19.036, '2024-07-09 20:09:03'),
(125, 3, 1, 1, 9, 3, -98.1428, 19.036, '2024-07-09 20:10:15'),
(126, 3, 1, 1, 9, 3, -98.1428, 19.036, '2024-07-09 20:10:25'),
(127, 3, 1, 1, 9, 3, -98.1428, 19.036, '2024-07-09 20:10:34');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `clienteId` int(11) NOT NULL,
  `nombre` varchar(300) COLLATE utf16_spanish_ci NOT NULL,
  `appaterno` varchar(300) COLLATE utf16_spanish_ci NOT NULL,
  `apmaterno` varchar(300) COLLATE utf16_spanish_ci NOT NULL,
  `razon_social` varchar(250) COLLATE utf16_spanish_ci NOT NULL,
  `rfc` varchar(15) COLLATE utf16_spanish_ci NOT NULL,
  `telefono` varchar(15) COLLATE utf16_spanish_ci NOT NULL,
  `correo` varchar(50) COLLATE utf16_spanish_ci NOT NULL,
  `direccion` varchar(300) COLLATE utf16_spanish_ci NOT NULL,
  `usuario` int(1) NOT NULL,
  `foto` varchar(255) COLLATE utf16_spanish_ci NOT NULL,
  `reg` datetime NOT NULL,
  `estatus` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_spanish_ci;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`clienteId`, `nombre`, `appaterno`, `apmaterno`, `razon_social`, `rfc`, `telefono`, `correo`, `direccion`, `usuario`, `foto`, `reg`, `estatus`) VALUES
(5, 'Cliente', 'Usu', 'Usu', 'PISA', 'AAPB2409057W3', '4081021436', 'R@prueba.com', '000 5 m', 1, '', '2024-05-03 18:24:20', 1),
(9, 'Bimbo SA de CV', '', '', 'BIMBOSA', '', '4081021436', 'prueba@prueba.com', '', 1, 'doc_240530-173846grupo-bimbo-logo.png', '2024-05-14 18:24:26', 1),
(10, 'VW', '', '', 'vW', '', '2222222222', 'prueba@prueba.com', 'assa', 1, 'doc_240529-210534volkswagen-logo-9.png', '2024-05-14 19:27:10', 1),
(11, 'VIAJES TURISTICOS MINI SA DE CV', '', '', 'vtm sa', '', '', '', '', 0, '', '2024-05-14 19:27:23', 1),
(12, 'LUIS MIGUEL', 'SEÑOR ', '', 'el Sol', '', '', '', '', 0, '', '2024-05-14 19:27:35', 1),
(13, 'Prueba Inf', '', '', '', '', '', '', '', 0, 'doc_240606-192936ine.jpg', '2024-06-06 19:28:19', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `config_general`
--

CREATE TABLE `config_general` (
  `id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `descripcion` text NOT NULL,
  `activo` int(11) NOT NULL,
  `url` varchar(100) DEFAULT NULL,
  `MenusubId` int(11) NOT NULL,
  `perfilId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `config_general`
--

INSERT INTO `config_general` (`id`, `status`, `descripcion`, `activo`, `url`, `MenusubId`, `perfilId`) VALUES
(1, 0, 'sms', 1, NULL, 0, 0),
(2, 0, 'pagina incio', 1, 'Agenda', 0, 0),
(3, 0, '', 0, NULL, 1, 0),
(4, 0, '', 0, NULL, 1, 2),
(5, 0, '', 0, NULL, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gafete`
--

CREATE TABLE `gafete` (
  `id` int(11) NOT NULL,
  `titulo1` varchar(255) NOT NULL,
  `titulo2` varchar(255) NOT NULL,
  `titulo3` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `gafete`
--

INSERT INTO `gafete` (`id`, `titulo1`, `titulo2`, `titulo3`) VALUES
(1, 'doc_231017-141848mangoo.png', 'doc_231017-141855mangoo.png', 'Para cualquier información referente a\nesta producción, comunicarte a:\n(55-68-92-45)');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE `menu` (
  `MenuId` int(11) NOT NULL,
  `Nombre` varchar(45) DEFAULT NULL,
  `Icon` varchar(45) DEFAULT NULL,
  `orden` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`MenuId`, `Nombre`, `Icon`, `orden`) VALUES
(1, 'Control Interno', 'icon-speedometer', 2),
(2, 'Catálogos', 'fa fa-list-alt', 3),
(3, 'Administrador\n', 'fa fa-lock', 4),
(4, 'Consultas', 'fa fa-list-alt', 1),
(5, 'Configuración', 'Configuracion', 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu_sub`
--

CREATE TABLE `menu_sub` (
  `MenusubId` int(11) NOT NULL,
  `MenuId` int(11) NOT NULL,
  `Nombre` varchar(50) DEFAULT NULL,
  `Pagina` varchar(120) DEFAULT NULL,
  `Icon` varchar(45) DEFAULT NULL,
  `orden` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `menu_sub`
--

INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `orden`) VALUES
(1, 0, 'Empleados', 'Empleados', 'fas fa-users', 1),
(2, 0, 'Check Points', 'Check_Points', 'fas fa-flag-checkered', 2),
(3, 0, 'Operación', 'Operacion', 'fas fa-qrcode', 3),
(4, 0, 'Reportes', 'Reportes', 'fas fa-clipboard-list', 4),
(5, 5, 'Gafete', 'Gafete', 'fa fa-cogs', 1),
(6, 0, 'Clientes', 'Clientes', 'fa fa-address-card', 7),
(8, 0, 'Operadores', 'Operadores', 'fa fa-car', 5),
(9, 0, 'Empresas', 'Empresas', 'fa fa-building', 6),
(11, 0, 'Rutas', 'Rutas', 'fa fa-route', 8),
(12, 0, 'SRutas', 'Seleccion_Rutas', 'fa fa-route', 9),
(13, 0, 'Unidades', 'Unidades', 'fa fa-car-side', 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operadores`
--

CREATE TABLE `operadores` (
  `operadorId` int(11) NOT NULL,
  `nombre` varchar(300) COLLATE utf16_spanish_ci NOT NULL,
  `ap_paterno` varchar(300) COLLATE utf16_spanish_ci NOT NULL,
  `ap_materno` varchar(300) COLLATE utf16_spanish_ci NOT NULL,
  `telefono` varchar(15) COLLATE utf16_spanish_ci NOT NULL,
  `direccion` varchar(300) COLLATE utf16_spanish_ci NOT NULL,
  `fecha_ingreso` date NOT NULL,
  `vigencia_examen` date NOT NULL,
  `vigencia_licencia` date NOT NULL,
  `tipo_licencia` int(11) NOT NULL,
  `usuario` int(1) NOT NULL,
  `estatus` int(1) NOT NULL DEFAULT '1',
  `reg` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_spanish_ci;

--
-- Volcado de datos para la tabla `operadores`
--

INSERT INTO `operadores` (`operadorId`, `nombre`, `ap_paterno`, `ap_materno`, `telefono`, `direccion`, `fecha_ingreso`, `vigencia_examen`, `vigencia_licencia`, `tipo_licencia`, `usuario`, `estatus`, `reg`) VALUES
(1, 'Roy', 'Sal', 'Sol', '2222222222', 'Razon Social', '2024-05-02', '2024-05-10', '2024-05-25', 1, 1, 1, '2024-05-07 16:45:25'),
(2, 'Usu', 'Usu', 'Usu', '4081021436', '000 5 m', '2024-05-25', '2024-05-18', '2024-05-10', 1, 1, 1, '2024-05-07 18:22:51'),
(3, 'Juan', 'Perez', 'Sol', '2222222222', '', '2024-05-09', '2024-05-21', '2024-05-18', 1, 0, 1, '2024-05-14 19:24:07'),
(4, 'Ramon', 'N', 'Sol', '2222222222', 'Razon Social', '2024-05-10', '2024-05-23', '2024-05-15', 1, 1, 0, '2024-05-14 19:24:29'),
(5, 'Omar', 'Ramos', 'Usu', '4081021436', '000 5 m', '2024-05-11', '2024-05-01', '2024-05-10', 1, 0, 1, '2024-05-14 19:24:49'),
(6, 'Albertano', 'Santa', 'Cruz', '2222222222', 'dom', '2024-05-08', '2024-05-17', '2024-04-28', 1, 0, 1, '2024-05-14 19:25:16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operadores_documentos`
--

CREATE TABLE `operadores_documentos` (
  `documentoId` int(11) NOT NULL,
  `operadorId` int(11) NOT NULL,
  `file` text COLLATE utf16_spanish_ci NOT NULL,
  `reg_file` datetime NOT NULL,
  `tipo` int(11) NOT NULL COMMENT '1=comprobante, 2=identificacion,3=examedico,4=licencia',
  `vigencia` date NOT NULL,
  `clase` int(11) NOT NULL,
  `estatus` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfiles`
--

CREATE TABLE `perfiles` (
  `perfilId` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `estatus` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `perfiles`
--

INSERT INTO `perfiles` (`perfilId`, `nombre`, `estatus`) VALUES
(1, 'Administrador', 1),
(2, 'Operador', 1),
(3, 'Cliente', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfiles_detalles`
--

CREATE TABLE `perfiles_detalles` (
  `Perfil_detalleId` int(11) NOT NULL,
  `perfilId` int(11) NOT NULL,
  `MenusubId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `perfiles_detalles`
--

INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES
(1, 1, 1),
(4, 1, 3),
(5, 1, 4),
(6, 2, 3),
(13, 1, 6),
(14, 1, 8),
(16, 3, 1),
(19, 1, 11),
(20, 3, 12),
(21, 1, 12),
(22, 1, 13),
(23, 3, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal`
--

CREATE TABLE `personal` (
  `personalId` int(11) NOT NULL,
  `numero_empleado` bigint(20) NOT NULL,
  `nombre` varchar(300) NOT NULL,
  `appaterno` varchar(300) NOT NULL,
  `apmaterno` varchar(300) NOT NULL,
  `operativo` varchar(300) NOT NULL,
  `cliente` int(11) NOT NULL,
  `tel_emergencia` varchar(15) NOT NULL,
  `persona` varchar(250) NOT NULL,
  `parentesco` varchar(100) NOT NULL,
  `sangre` varchar(15) NOT NULL,
  `puesto` varchar(255) NOT NULL,
  `celular` varchar(15) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `check_baja` varchar(2) NOT NULL,
  `fechabaja` date DEFAULT NULL,
  `motivo` text NOT NULL,
  `estatus` int(1) NOT NULL DEFAULT '1' COMMENT '1 visible 0 eliminado',
  `usuario` int(1) NOT NULL DEFAULT '0' COMMENT 'Es para saber cuando  ya sele haya agregad un usuario',
  `foto` varchar(255) NOT NULL,
  `color` varchar(20) NOT NULL,
  `tipo_empleado` int(1) NOT NULL,
  `reg` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `personal`
--

INSERT INTO `personal` (`personalId`, `numero_empleado`, `nombre`, `appaterno`, `apmaterno`, `operativo`, `cliente`, `tel_emergencia`, `persona`, `parentesco`, `sangre`, `puesto`, `celular`, `correo`, `check_baja`, `fechabaja`, `motivo`, `estatus`, `usuario`, `foto`, `color`, `tipo_empleado`, `reg`) VALUES
(1, 0, 'Administrador', '', '', '', 0, '3793', '', '', '', '', '2228340563', 'a@gmail.com', '', '2021-01-12', '', 1, 1, '', '', 1, '2021-06-06 00:00:00'),
(2, 0, 'prueba 1', '', '', '', 0, '', '', '', '', '', '', '', 'on', '2021-01-07', 'eee', 0, 0, '', '', 0, '2021-01-07 11:25:27'),
(3, 3, 'Clauida', 'Gomez', 'Moreno', 'OPT', 9, '', '', '', '', '23424234', '4234234234', 'rwe@gmail.com', '', '2021-01-08', 'werewew\r\ndasd\r\nasdsd\r\ndasd\r\ndasd\r\ndasd\r\ndasd', 1, 0, '', '', 0, '2021-01-07 11:26:31'),
(4, 2, 'José Luis Peralta', '', '', '', 0, '', '', '', '', '556668686', '240132323', 'jose@patito', '', '2021-03-01', 'por incumplimiento', 0, 1, '', '', 0, '2021-01-07 14:02:55'),
(5, 3, 'Mateo Gonzales', '', '', '', 0, '', '', '', '', 'Actor', '4234234234', 'mant@gmail.com', '', '0000-00-00', '', 0, 1, '', '', 0, '2021-03-01 17:05:37'),
(6, 0, 'Gio Perez Lopez', '', '', '', 0, '', '', '', '', 'Productor', '4234234234', 'gio@gmail.com', '', '0000-00-00', '', 0, 1, '', '', 0, '2021-03-01 17:07:00'),
(7, 0, 'Sergio Trujillo Conde', '', '', '', 0, '', '', '', '', 'cocinero', '2491024472', 'ser@gmail.com', '', '0000-00-00', '', 0, 1, '', '', 0, '2021-03-01 17:10:38'),
(8, 0, 'Luis Garcia Torres', '', '', '', 0, '', '', '', '', 'camarógrafo', '4234234234', 'luis@gmail.com', '', '0000-00-00', '', 0, 1, '', '', 0, '2021-03-03 13:07:32'),
(9, 1, 'Felipe Molares Torres', '', '', '', 0, '', '', '', '', 'De video', '4234234234', 'feli@gmail.com', '', '0000-00-00', '', 0, 0, '', 'yellow', 0, '2021-03-03 15:58:32'),
(12, 0, 'Administrador', '', '', '', 0, '', '', '', '', '', '', '', '', '2021-06-19', '', 0, 1, '', '#00bb2d', 0, '2021-06-13 00:00:00'),
(13, 0, 'Administrador a', '', '', '', 0, '', '', '', '', '', '', '', '', '0000-00-00', '', 0, 1, 'doc_210602-163944img.PNG', '', 0, '2021-06-02 12:19:29'),
(14, 1, 'Prueba 1', '', '', '', 0, '', '', '', '', '', '', '', '', '0000-00-00', '', 0, 0, '', 'red', 0, '2021-06-17 12:49:21'),
(15, 1, 'prueba 1', '', '', '', 0, '', '', '', '', '', '', '', '', '0000-00-00', '', 0, 0, '', '#00bb2d', 0, '2021-06-17 12:54:30'),
(16, 2, 'prueba 2', '', '', '', 0, '', '', '', '', '', '', '', '', '0000-00-00', '', 0, 0, '', '', 0, '2021-06-17 12:55:18'),
(17, 3, 'prueba 3', '', '', '', 0, '', '', '', '', '', '', '', '', '0000-00-00', '', 0, 0, '', '', 0, '2021-06-17 12:55:42'),
(18, 4, 'prueba 4', '', '', '', 0, '', '', '', '', '', '', '', '', '0000-00-00', '', 0, 0, '', '', 0, '2021-06-17 13:03:52'),
(19, 5, 'prueba 5', '', '', '', 0, '', '', '', '', '', '', '', '', '0000-00-00', '', 0, 0, '', 'yellow', 0, '2021-06-17 13:04:03'),
(20, 5, 'prueba 6', '', '', '', 0, '', '', '', '', '', '', '', '', '0000-00-00', '', 0, 0, '', '#03a9f4', 0, '2021-06-17 13:04:30'),
(21, 5, 'retret', '', '', '', 0, '', '', '', '', '', '', '', '', '0000-00-00', '', 0, 0, '', '#00bb2d', 0, '2021-06-17 13:23:03'),
(22, 1, 'prueba1', '', '', '', 0, '', '', '', '', '', '', '', '', '0000-00-00', '', 0, 0, '', '', 0, '2021-06-17 13:23:47'),
(23, 2, 'prueba 2', '', '', '', 0, '', '', '', '', '', '', '', '', '0000-00-00', '', 0, 0, '', '#00bb2d', 0, '2021-06-17 13:23:58'),
(24, 3, 'prueba3', '', '', '', 0, '', '', '', '', '', '', '', '', '0000-00-00', '', 0, 0, '', '#03a9f4', 0, '2021-06-17 13:24:09'),
(25, 4, 'prueba 4', '', '', '', 0, '', '', '', '', '', '', '', '', '0000-00-00', '', 0, 0, '', '#03a9f4', 0, '2021-06-17 13:24:25'),
(26, 4, 'prueba5 ', '', '', '', 0, '', '', '', '', '', '', '', '', '0000-00-00', '', 0, 0, '', '#03a9f4', 0, '2021-06-17 13:24:40'),
(27, 5, 'Pedro Ramirez Juares', '', '', '', 0, '', '', '', '', 'cocinero', '4234234234', 'rwe@gmail.com', '', '0000-00-00', '', 0, 0, '', 'orange', 0, '2021-06-19 16:13:04'),
(28, 6, 'Alejandra Morales Molina ', '', '', '', 0, '', '', '', '', 'camarógrafo', '4234234234', 'rwe@gmail.com', '', '0000-00-00', '', 0, 0, '', 'orange', 0, '2021-06-19 16:16:33'),
(66, 2, 'Roy', 'Sal', 'Sol', 'ZZZ', 3, '2222222222', 'Willy', 'Padre', 'B', '', '', '', '', '0000-00-00', '', 0, 0, '', '', 0, '2024-05-07 17:13:44'),
(67, 3, 'Jesus', 'Paquini', '', 'operativo', 5, '2222222222', 'Contacto', 'Hermano', 'AB', '', '', '', '', '0000-00-00', '', 0, 0, '', '', 0, '2024-05-07 17:14:15'),
(68, 4, 'Usu', 'Usu', 'Usu', 'operativo', 0, '4081021436', 'Contacto', 'Hermano', 'B', '', '', '', '', '0000-00-00', '', 0, 1, '', '', 0, '2024-05-07 17:58:44'),
(69, 5, 'Juan Escutia', 'Bandera', 'Apellido M', '', 5, '2222222222', 'Contacto', '', 'B', '', '', '', '', '0000-00-00', '', 0, 0, '', '', 0, '2024-05-07 18:29:09'),
(70, 6, 'Jose Jose', 'Don Pedro', 'Materno', 'TICS', 9, '1234567890', 'Jesus Paquini', '', 'Puro Alcohol', '', '', '', '', '0000-00-00', '', 0, 0, '', '', 0, '2024-05-14 19:34:24'),
(71, 7, 'Alfredo', 'Bimbo', '', 'BMO ', 9, '', '', '', '', '', '', '', '', '0000-00-00', '', 0, 0, '', '', 0, '2024-05-14 19:34:41'),
(72, 8, 'Willy', 'Bimbo', '', 'xxx', 9, '', '', '', '', '', '', '', '', '0000-00-00', '', 0, 0, '', '', 0, '2024-05-14 20:18:45'),
(73, 9, 'Mariana', 'Bimbo', '', 'mb', 9, '', '', '', '', '', '', '', '', '0000-00-00', '', 0, 0, '', '', 0, '2024-05-14 20:19:06'),
(74, 10, 'Mary Carmen', 'Bimbo', '', 'MCB', 9, '', '', '', '', '', '', '', '', '0000-00-00', '', 0, 0, '', '', 0, '2024-05-14 20:19:24'),
(75, 0, 'Jose Jose', 'Don Pedro', 'Materno', 'TICS', 9, '1234567890', 'Jesus Paquini', 'Papa BBF', 'Puro Alcohol', '', '', '', '', NULL, '', 0, 0, '', '', 0, '2024-05-20 21:32:56'),
(76, 0, 'Jose Jose', 'Don Pedro', 'Materno', 'TICS', 9, '1234567890', 'Jesus Paquini', 'Papa BBF', 'Puro Alcohol', '', '', '', '', NULL, '', 0, 0, '', '', 0, '2024-05-20 21:38:04'),
(77, 0, 'Jose Jose', 'Don Pedro', 'Materno', 'TICS', 9, '1234567890', 'Jesus Paquini', 'Papa BBF', 'Puro Alcohol', '', '', '', '', NULL, '', 0, 0, '', '', 0, '2024-05-20 21:43:49'),
(78, 0, 'Jose Jose', 'Don Pedro', 'Materno', 'TICS', 9, '1234567890', 'Jesus Paquini', 'Papa BBF', 'Puro Alcohol', '', '', '', '', NULL, '', 0, 0, '', '', 0, '2024-05-20 21:48:16'),
(79, 0, 'Jose Jose', 'Don Pedro', 'Materno', 'TICS', 9, '1234567890', 'Jesus Paquini', 'Papa BBF', 'Puro Alcohol', '', '', '', '', NULL, '', 0, 0, '', '', 0, '2024-05-20 21:56:34'),
(80, 0, 'Jose Jose', 'Don Pedro', 'Materno', 'TICS', 9, '1234567890', 'Jesus Paquini', 'Papa BBF', 'Puro Alcohol', '', '', '', '', NULL, '', 0, 0, '', '', 0, '2024-05-20 21:56:58'),
(81, 0, 'Jose Jose', 'Don Pedro', 'Materno', 'TICS', 9, '1234567890', 'Jesus Paquini', 'Papa BBF', 'Puro Alcohol', '', '', '', '', NULL, '', 0, 0, '', '', 0, '2024-05-20 21:58:06'),
(82, 0, 'Jose Jose', 'Don Pedro', 'Materno', 'TICS', 9, '1234567890', 'Jesus Paquini', 'Papa BBF', 'Puro Alcohol', '', '', '', '', NULL, '', 0, 0, '', '', 0, '2024-05-20 22:17:33'),
(83, 0, 'Carlso', 'Magno', 'Maternos', 'TICS', 0, '1234567890', 'Jesus Paquini', '', 'Puro Alcohol', '', '', '', '', NULL, '', 0, 0, '', '', 0, '2024-05-20 22:18:33'),
(84, 0, 'Carlso', 'Magno', 'Maternos', 'TICS', 9, '1234567890', 'Jesus Paquini', '', 'Puro Alcohol', '', '', '', '', '0000-00-00', '', 0, 0, '', '', 0, '2024-05-20 22:38:03'),
(85, 1, 'Susana', 'Distancia', '', 'PREP', 9, '2222222222', 'Willy', 'Hermano', 'AB', '', '', '', '', '0000-00-00', '', 1, 0, 'doc_240529-212415avatars.jpg', '', 0, '2024-05-22 21:50:41'),
(86, 2, 'Juan Jose', 'Relleno', 'Lopez', 'IFE', 9, '', '', '', '', '', '', '', '', '0000-00-00', '', 1, 0, '', '', 0, '2024-05-23 20:42:54'),
(87, 3, 'Paco', 'Malla', '', 'MAYA', 9, '4081021436', '', 'Hermano', 'o-', '', '', '', '', '0000-00-00', '', 0, 1, '', '', 0, '2024-05-23 20:59:55'),
(88, 4, 'Abel', 'Quintero', '', 'VW4P', 10, '4081021436', 'Willy', 'Padre', 'B+', '', '', '', '', '0000-00-00', '', 1, 0, 'doc_240530-174228m3.png', '', 0, '2024-05-24 21:26:37'),
(89, 5, 'Empty', '', '', '404', 13, '', '', '', '', '', '', '', '', '0000-00-00', '', 1, 1, 'doc_240606-193034volkswagen-logo-9.png', '', 0, '2024-06-06 19:28:32');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rutas`
--

CREATE TABLE `rutas` (
  `id` int(11) NOT NULL,
  `ruta` varchar(200) COLLATE utf16_spanish_ci NOT NULL,
  `choferId` int(11) NOT NULL,
  `unidadId` int(11) NOT NULL,
  `clienteId` int(11) NOT NULL,
  `reg` datetime NOT NULL,
  `estatus` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_spanish_ci;

--
-- Volcado de datos para la tabla `rutas`
--

INSERT INTO `rutas` (`id`, `ruta`, `choferId`, `unidadId`, `clienteId`, `reg`, `estatus`) VALUES
(1, 'ZOCALO DE PUEBLA', 6, 5, 11, '2024-05-09 18:59:05', 1),
(2, 'VW PUERTA 4', 1, 4, 10, '2024-05-09 19:52:58', 1),
(3, 'LA PAZ - BIMBO', 4, 3, 9, '2024-05-14 18:26:33', 1),
(4, 'LA PAZ', 3, 2, 12, '2024-05-14 19:28:40', 1),
(5, 'BIMBO - Sur', 1, 6, 9, '2024-05-23 21:08:40', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rutas_clientes`
--

CREATE TABLE `rutas_clientes` (
  `id` int(11) NOT NULL,
  `clienteId` int(11) NOT NULL,
  `empleadoId` int(11) NOT NULL,
  `rutaId` int(11) NOT NULL,
  `tipo` int(1) NOT NULL COMMENT '0=Bajada,1=Subida',
  `reg` datetime NOT NULL,
  `estatus` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_spanish_ci;

--
-- Volcado de datos para la tabla `rutas_clientes`
--

INSERT INTO `rutas_clientes` (`id`, `clienteId`, `empleadoId`, `rutaId`, `tipo`, `reg`, `estatus`) VALUES
(39, 9, 85, 3, 0, '2024-05-24 18:50:48', 0),
(40, 9, 86, 5, 0, '2024-05-24 20:47:01', 0),
(41, 10, 88, 2, 0, '2024-05-24 21:26:47', 1),
(42, 9, 87, 5, 0, '2024-05-30 17:53:10', 1),
(43, 9, 85, 5, 0, '2024-06-04 21:19:19', 1),
(44, 9, 86, 3, 0, '2024-06-04 21:49:29', 1),
(45, 9, 3, 3, 0, '2024-07-08 19:35:07', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `unidades`
--

CREATE TABLE `unidades` (
  `id` int(11) NOT NULL,
  `unidad` varchar(100) COLLATE utf16_spanish_ci NOT NULL,
  `ruta` varchar(100) COLLATE utf16_spanish_ci NOT NULL,
  `cliente` int(11) NOT NULL,
  `placas` varchar(10) COLLATE utf16_spanish_ci NOT NULL,
  `activo` int(11) NOT NULL DEFAULT '1',
  `reg` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_spanish_ci;

--
-- Volcado de datos para la tabla `unidades`
--

INSERT INTO `unidades` (`id`, `unidad`, `ruta`, `cliente`, `placas`, `activo`, `reg`) VALUES
(1, 'SPRINT ECO1', 'eco1', 10, '', 0, '0000-00-00 00:00:00'),
(2, 'SPRINT ECO1', 'eco1', 9, '', 1, '0000-00-00 00:00:00'),
(3, 'SPRINT ECO2', 'eco2', 11, '', 1, '0000-00-00 00:00:00'),
(4, 'SPRINT ECO3', 'eco3', 9, '', 1, '0000-00-00 00:00:00'),
(5, 'SPRINT ECO4', 'eco4', 12, '', 1, '0000-00-00 00:00:00'),
(6, 'SPRINT ECO5', 'eco50', 5, 'ABC2400', 1, '2024-05-17 18:24:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `UsuarioID` int(11) NOT NULL,
  `perfilId` int(11) NOT NULL,
  `personalId` int(11) NOT NULL,
  `operadorId` int(11) NOT NULL,
  `clienteId` int(11) NOT NULL,
  `Usuario` varchar(45) DEFAULT NULL,
  `contrasena` varchar(80) DEFAULT NULL,
  `estatus` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`UsuarioID`, `perfilId`, `personalId`, `operadorId`, `clienteId`, `Usuario`, `contrasena`, `estatus`) VALUES
(1, 1, 1, 0, 0, 'admin', '$2y$10$.yBpfsTTEmhYhECki.qy3eL45XaLo3MinuIPgiPsjmXBYItZXHUga', 1),
(11, 1, 0, 0, 5, 'Cliente', '$2y$10$4V9rxkxglwMTgH5gL62e5uvRovMFTbBiv1a6C9updeYaNjyNM3/X6', 1),
(14, 3, 0, 0, 9, 'Bimbo', '$2y$10$fF1FB38pMBTYKEK.wSzp9On9vt.jBngQrIY4OnMrHFQ07GxxFiVoS', 1),
(16, 3, 0, 0, 10, 'VW', '$2y$10$JHkrzotHr2IBfYiJBUhkW.u8qUMtZ9qqckS6ZJxNiFMUwfettpCO2', 1),
(19, 2, 0, 2, 0, 'usu', '$2y$10$xeiTX/XwUpsKCwkKdzSZA.7K7gWAtccfSZzJHhRPCCq4AkhWHuB02', 1),
(20, 2, 0, 1, 0, 'roy', '$2y$10$TZ9mVqbhtNpNiAQFEa1izuSi6.soLug9.RsItQwz0ZopdgZwuIBfC', 1),
(21, 1, 89, 0, 0, 'empty', '$2y$10$lupERCdR3m.eLYbnDQ4YuujNYWSkwEpcr8bYugWzlXSSVqc4BkKlC', 1),
(22, 1, 87, 0, 0, 'malla', '$2y$10$6to9Toznw1kl.ZsquaabC./gipZntkTi/.ndfa7F0jXx4AT8lNyG2', 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `check_points`
--
ALTER TABLE `check_points`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `check_points_detalles`
--
ALTER TABLE `check_points_detalles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idpoints_check` (`idpoints_check`),
  ADD KEY `check_points_detalles_ibfk_2` (`personalId`),
  ADD KEY `check_points_detalles_ibfk_3` (`idempleado`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`clienteId`);

--
-- Indices de la tabla `config_general`
--
ALTER TABLE `config_general`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `gafete`
--
ALTER TABLE `gafete`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`MenuId`);

--
-- Indices de la tabla `menu_sub`
--
ALTER TABLE `menu_sub`
  ADD PRIMARY KEY (`MenusubId`),
  ADD KEY `fk_menu_sub_menu_idx` (`MenuId`);

--
-- Indices de la tabla `operadores`
--
ALTER TABLE `operadores`
  ADD PRIMARY KEY (`operadorId`);

--
-- Indices de la tabla `operadores_documentos`
--
ALTER TABLE `operadores_documentos`
  ADD PRIMARY KEY (`documentoId`),
  ADD KEY `operadorId` (`operadorId`);

--
-- Indices de la tabla `perfiles`
--
ALTER TABLE `perfiles`
  ADD PRIMARY KEY (`perfilId`);

--
-- Indices de la tabla `perfiles_detalles`
--
ALTER TABLE `perfiles_detalles`
  ADD PRIMARY KEY (`Perfil_detalleId`),
  ADD KEY `fk_Perfiles_detalles_Perfiles1_idx` (`perfilId`),
  ADD KEY `fk_Perfiles_detalles_menu_sub1_idx` (`MenusubId`);

--
-- Indices de la tabla `personal`
--
ALTER TABLE `personal`
  ADD PRIMARY KEY (`personalId`);

--
-- Indices de la tabla `rutas`
--
ALTER TABLE `rutas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `rutas_clientes`
--
ALTER TABLE `rutas_clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `unidades`
--
ALTER TABLE `unidades`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`UsuarioID`),
  ADD KEY `fk_usuarios_Perfiles1_idx` (`perfilId`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `check_points`
--
ALTER TABLE `check_points`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `check_points_detalles`
--
ALTER TABLE `check_points_detalles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `clienteId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `config_general`
--
ALTER TABLE `config_general`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `gafete`
--
ALTER TABLE `gafete`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `menu`
--
ALTER TABLE `menu`
  MODIFY `MenuId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `menu_sub`
--
ALTER TABLE `menu_sub`
  MODIFY `MenusubId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `operadores`
--
ALTER TABLE `operadores`
  MODIFY `operadorId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `operadores_documentos`
--
ALTER TABLE `operadores_documentos`
  MODIFY `documentoId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `perfiles`
--
ALTER TABLE `perfiles`
  MODIFY `perfilId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `perfiles_detalles`
--
ALTER TABLE `perfiles_detalles`
  MODIFY `Perfil_detalleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `personal`
--
ALTER TABLE `personal`
  MODIFY `personalId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT de la tabla `rutas`
--
ALTER TABLE `rutas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `rutas_clientes`
--
ALTER TABLE `rutas_clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT de la tabla `unidades`
--
ALTER TABLE `unidades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `UsuarioID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `check_points_detalles`
--
ALTER TABLE `check_points_detalles`
  ADD CONSTRAINT `check_points_detalles_ibfk_1` FOREIGN KEY (`idpoints_check`) REFERENCES `check_points` (`id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `check_points_detalles_ibfk_2` FOREIGN KEY (`personalId`) REFERENCES `personal` (`personalId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `check_points_detalles_ibfk_3` FOREIGN KEY (`idempleado`) REFERENCES `personal` (`personalId`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `operadores_documentos`
--
ALTER TABLE `operadores_documentos`
  ADD CONSTRAINT `operadores_documentos_ibfk_1` FOREIGN KEY (`operadorId`) REFERENCES `operadores` (`operadorId`);

--
-- Filtros para la tabla `perfiles_detalles`
--
ALTER TABLE `perfiles_detalles`
  ADD CONSTRAINT `fk_Perfiles_detalles_Perfiles1` FOREIGN KEY (`perfilId`) REFERENCES `perfiles` (`perfilId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Perfiles_detalles_menu_sub1` FOREIGN KEY (`MenusubId`) REFERENCES `menu_sub` (`MenusubId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `fk_usuarios_Perfiles1` FOREIGN KEY (`perfilId`) REFERENCES `perfiles` (`perfilId`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
