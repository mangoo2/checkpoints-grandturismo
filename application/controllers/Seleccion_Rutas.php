<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Seleccion_Rutas extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->idpersonal = $this->session->userdata('idpersonal');
        $this->administrador = $this->session->userdata('administrador');
        $this->perfilid = $this->session->userdata('perfilid');
        date_default_timezone_set('America/Mexico_City');
        $this->fecha_hora_actual = date('Y-m-d G:i:s');
        $this->fechainicio = date('Y-m-d');
        $this->inicioactual = date('G:i:s');
        if ($this->session->userdata('logeado')) {
            $this->idpersonal = $this->session->userdata('idpersonal');
            $this->perfilid = $this->session->userdata('perfilid');
            $this->cliente = $this->session->userdata('cliente');
            $permiso = $this->Login_model->getviewpermiso($this->perfilid, 1); // perfil y id del submenu
            if ($permiso == 0) {
                redirect('Login');
            }
        } else {
            redirect('/Login');
        }
    }

    public function index()
    {
        $data['perfil'] = $this->perfilid;

        if ($this->perfilid == 3) {
            $data['personal'] = $this->General_model->getselectwhereall('personal',array('cliente'=>$this->cliente,'estatus'=>1));
            $data['rutas'] = $this->General_model->getselectwhereall('rutas',array('clienteId'=>$this->cliente,'estatus'=>1));
            $data['cliente'] = $this->General_model->get_record('clienteId',$this->cliente,'clientes');
            //log_message('error','Cliente: '.json_encode($data['cliente']));
        } else {
            $data['personal'] = $this->General_model->get_records_condition('estatus = 1', 'personal');
            $data['rutas'] = $this->General_model->get_records_condition('estatus = 1', 'rutas');
            $data['cliente'] = 'Ninguno';
        }

        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('seleccionRutas/index', $data);
        $this->load->view('templates/footer');
        $this->load->view('seleccionRutas/index_js');
    }

    public function registra_ruta()
    {
        $data = $this->input->post();
        //log_message('error', 'DATA: ' . json_encode($data));
        $ID = $data['id'];
        unset($data['id']);
        $reg_id = 0;

        if ($ID == 0) {
            
            if($this->perfilid == 3){
                $data['clienteId'] = $this->cliente;
            }else{
                $data['clienteId'] = 0;
            }

            $data['reg'] = $this->fecha_hora_actual;
            $reg_id = $this->General_model->add_record('rutas_clientes', $data);
        } else {
            $reg_id = $this->General_model->edit_record('id', $ID, $data, 'rutas_clientes');
            $reg_id = $ID;
        }
        echo $reg_id;
    }

    public function getlistado()
    {
        $params = $this->input->post();

        if($this->perfilid == 3){
            $params['particular'] = $this->cliente;
            //log_message('error','PARTICULAR');
        }

        $getdata = $this->ModelCatalogos->get_rutas_clientes($params);
        $totaldata = $this->ModelCatalogos->total_rutas_clientes($params);

        //log_message('error','DATA: '.json_encode($getdata->result()));

        $json_data = array(
            "draw"            => intval($params['draw']),
            "recordsTotal"    => intval($totaldata),
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           => $this->db->last_query()
        );
        echo json_encode($json_data);
    }

    public function delete_ruta()
    {
        $id = $this->input->post('id');
        $data = array('estatus' => 0);
        $this->General_model->edit_record('id', $id, $data, 'rutas');
    }

    public function delete_cliru()
    {
        $id = $this->input->post('id');
        $data = array('estatus' => 0);
        $this->General_model->edit_record('id', $id, $data, 'rutas_clientes');
    }

    function registros_delete()
    {
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);

        //log_message('error', 'DATA REGISTROS: ' . json_encode($DATA));

        for ($i = 0; $i < count($DATA); $i++) {
            $rutaId = $DATA[$i]->rutaId;
            $array = array('estatus' => 0);
            $this->General_model->edit_record('id', $rutaId, $array, 'rutas');
        }
    }

    public function delete_registros()
    {
        $array = array('estatus' => 0);
        $this->General_model->edit_record_all($array, 'rutas');
    }

    /*
    public function get_data_rutasClientes(){
        $id = $this->input->post('id');
        $data = $this->General_model->get_data_rutasClientes($id);
        echo json_encode($data);
    }
    */

    public function get_data_cleintes_rutas(){
        $id = $this->input->post('id');
        //$dataClientesR = $this->General_model->getselectwhereall('rutas_clientes',array('rutaId'=>$id,'clienteId'=>$this->cliente,'estatus'=>1));
        $dataClientesR = $this->General_model->get_data_rutasClientes($id);
        $tipos = array("Bajada","Subida");

        //log_message('error','RUTASCLIENTES: '.json_encode($dataClientesR));
        //[{"id":"9","tipo":"1","ruta":"LA PAZ - BIMBO","empleado":"Mary Carmen Bimbo "}]

        $data = array();

        foreach($dataClientesR as $key => $clienteR){
            $data[$key] = array($clienteR->id, $clienteR->ruta, $clienteR->empleado, $tipos[$clienteR->tipo]);
        }

        //log_message('error','DATA RUTASCLIENTES: '.json_encode($data));

        echo json_encode($data); 
    }



    public function getListadoRuCli()
    {
        $params = $this->input->post();

        //log_message('error','PARAM: '.json_encode($params));

        if($this->perfilid == 3){
            $params['particular'] = $this->cliente;
            //log_message('error','PARTICULAR');
        }

        //$dataClientesR = $this->General_model->get_data_rutasClientes($id);

        $getdata = $this->ModelCatalogos->get_RuCli($params);
        $totaldata = $this->ModelCatalogos->total_RuCli($params);


        $json_data = array(
            "draw"            => intval($params['draw']),
            "recordsTotal"    => intval($totaldata),
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           => $this->db->last_query()
        );
        echo json_encode($json_data);
    }

    public function searchDoubles(){
        $idRuta = $this->input->post('idRuta');
        $idEmpleado = $this->input->post('idEmpleado');

        $doubles = $this->General_model->get_doubles($idEmpleado,$idRuta,$this->cliente);

        if(!isset($doubles)){
            //log_message('error','NO Dobles');
            echo '0';
        }else{
            echo '1';
        }
    }

}
