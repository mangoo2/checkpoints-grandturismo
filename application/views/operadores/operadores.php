<style type="text/css">
	.kv-file-upload, .fileinput-remove-button {
		display: none;
	}
</style>

<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12"><br>
                <div class="card">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6 mb-2">
                                        <button type="button" class="btn btn-outline-info" onclick="modal_registro()"><i class="fas fa-truck"></i> Nuevo operador</button>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group bootstrap-touchspin">
                                            <span class="input-group-text">Buscar Operadores &nbsp;&nbsp; <i class="fas fa-search"></i></span></span><input id="operador" type="text" class="form-control" placeholder="Escriba nombre del operador" oninput="reload_registro()">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12"></div>
                                    <table class="table" id="table_datos">
                                        <thead class="bg-blue">
                                            <tr>
                                                <th>ID</th>
                                                <th>Nombre</th>
                                                <th>Teléfono</th>
                                                <th>Fecha ingreso</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<div id="registro_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header modal_sistema">
                <div class="col-lg-1"></div>
                <div class="col-lg-10" align="center">
                    <h4 class="modal-title">Registro de operadores</h4>
                </div>
                <div class="col-lg-1">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <form class="form" method="post" role="form" id="form_registro">
                                    <input type="hidden" name="id_operador" id="id_operador" value="0">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label><span style="color: red;">*</span> Nombre</label>
                                                <input type="text" name="nombre" id="nombre" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label><span style="color: red;">*</span> Apellido Paterno</label>
                                                <input type="text" name="ap_paterno" id="ap_paterno" class="form-control">
                                            </div>
                                        </div>

                                        <div class="col-md-6  ">
                                            <div class="form-group">
                                                <label> Apellido Materno</label>
                                                <input type="text" name="ap_materno" id="ap_materno" class="form-control">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label><span style="color: red;">*</span> Fecha ingreso</label>
                                                <input type="date" name="fecha_ingreso" id="fecha_ingreso" class="form-control">
                                            </div>
                                        </div>

                                        <div class="col-md-6  ">
                                            <div class="form-group">
                                                <label><span style="color: red;">*</span> Teléfono</label>
                                                <input type="text" name="telefono" id="telefono" class="form-control">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Dirección</label>
                                                <input type="text" name="direccion" id="direccion" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label class="form-label" for="especial">Operador especial</label>
                                                <span class="switch switch-icon">
                                                    <label>
                                                        <input type="checkbox" name="especial" id="especial">
                                                <span></span>
                                                </label>
                                            </span>
                                        </div>
                                    </div>

                                    <hr>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <h6>Datos de emergencia</h6>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Teléfono de emergencia</label>
                                                <input type="text" name="tel_emergencia" id="tel_emergencia" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Persona (contacto)</label>
                                                <input type="text" name="persona" id="persona" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Parentesco</label>
                                                <input type="text" name="parentesco" id="parentesco" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Tipo de sangre</label>
                                                <input type="text" name="sangre" id="sangre" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <hr>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <h6>Documentación</h6>
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="col-md-6">
                                            <label>Identificación Oficial</label>
                                            <input type="hidden" id="idDocIden">
                                            <input class="form-control" type="hidden" id="iden_aux">
                                            <input class="form-control" type="file" name="file" id="identificacion">
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Comprobante de Domicilio</label>
                                                <input type="hidden" id="idDocComp">
                                                <input class="form-control" type="hidden" id="comp_aux">
                                                <input class="form-control" type="file" name="file" id="comprobante">
                                            </div>
                                        </div>


                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Examen Médico</label>
                                                <input type="hidden" id="idDocExa">
                                                <input class="form-control" type="hidden" id="exa_aux">
                                                <input class="form-control" type="file" name="file" id="examen">

                                                <label><span style="color: red;">*</span> Fecha de vencimiento (examen médico)</label>
                                                <input type="date" name="vigencia_examen" id="vigencia_exa" class="form-control">
                                            </div>
                                        </div>

                                        <div class="col-md-6  ">
                                            <div class="form-group">
                                                <label>Licencia de Manejo</label>
                                                <input type="hidden" id="idDocLic">
                                                <input class="form-control" type="hidden" id="lic_aux">
                                                <input class="form-control" type="file" name="file" id="licencia">

                                                <label><span style="color: red;">*</span> Fecha de vencimiento (examen médico)</label>
                                                <input type="date" name="vigencia_licencia" id="vigencia_lic" class="form-control">
                                                <label><span style="color: red;">*</span> Tipo de licencia</label>
                                                <select class="form-control" name="tipo_licencia" id="tipo_lic">
                                                    <option value="1">Tipo A</option>
                                                    <option value="2">Tipo B</option>
                                                    <option value="3">Tipo C</option>
                                                    <option value="4">Tipo D</option>
                                                    <option value="5">Tipo E</option>
                                                    <option value="6">Tipo F</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info waves-effect btn_registro" onclick="guarda_registro()">Guardar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div id="elimina_registro_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea borrar éste operador? Esta operación no se puede deshacer.</h5>
            </div>
            <input type="hidden" id="id_points">
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="registro_delete()">Aceptar</button>
            </div>
        </div>
    </div>
</div>


<div id="usuario_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Usuario</h4>
                    <input type="hidden" id="idOpe">
                    <input type="hidden" id="tipoOpe" value="2">
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="text_usuario"></div>
            </div>
            <input type="hidden" id="id_empleado">
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" id="btn_modal_usu" class="btn btn_sistema waves-effect btn_registro" onclick="guardar_usuario()">Aceptar</button>
            </div>
        </div>
    </div>
</div>
