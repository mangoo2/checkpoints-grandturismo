-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 12-02-2024 a las 21:23:28
-- Versión del servidor: 5.7.26
-- Versión de PHP: 7.1.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sicoinet_guanabana`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `check_points`
--

DROP TABLE IF EXISTS `check_points`;
CREATE TABLE IF NOT EXISTS `check_points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `codigo` varchar(100) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1',
  `reg` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `check_points`
--

INSERT INTO `check_points` (`id`, `nombre`, `codigo`, `activo`, `reg`) VALUES
(1, 'comida', '1234567', 1, '2021-03-01 17:56:38'),
(2, 'prueba 2', '12345', 0, '2021-03-01 18:05:31'),
(3, 'prueba 3', '324567', 0, '2021-03-01 18:29:08'),
(4, 'prueba4', '423423', 0, '2021-03-01 19:08:21'),
(5, 'prueba 2', '987654', 1, '2021-03-02 11:57:25'),
(6, 'prueba 3', 'hz6bjs8km3', 0, '2021-03-02 11:59:08'),
(7, 'tubis', '232456', 1, '2021-03-02 11:59:32'),
(8, 'demo', '12345', 1, '2023-10-17 14:19:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `check_points_detalles`
--

DROP TABLE IF EXISTS `check_points_detalles`;
CREATE TABLE IF NOT EXISTS `check_points_detalles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `personalId` int(11) NOT NULL,
  `idpoints_check` int(11) NOT NULL,
  `idempleado` int(11) NOT NULL COMMENT 'quien guardo el registro',
  `reg` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idpoints_check` (`idpoints_check`),
  KEY `check_points_detalles_ibfk_2` (`personalId`),
  KEY `check_points_detalles_ibfk_3` (`idempleado`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `check_points_detalles`
--

INSERT INTO `check_points_detalles` (`id`, `personalId`, `idpoints_check`, `idempleado`, `reg`) VALUES
(1, 12, 5, 1, '2021-06-02 17:31:51'),
(2, 5, 5, 1, '2021-06-02 17:32:11'),
(3, 3, 5, 1, '2021-06-02 17:32:19'),
(4, 1, 5, 1, '2021-06-19 16:00:28'),
(5, 27, 1, 1, '2021-06-19 16:14:32'),
(6, 28, 1, 1, '2021-06-19 16:17:06'),
(7, 28, 5, 1, '2021-06-30 11:14:51'),
(8, 28, 5, 1, '2021-06-30 11:15:30'),
(9, 27, 5, 1, '2021-06-30 11:16:44'),
(10, 26, 5, 1, '2021-06-30 11:17:13'),
(11, 24, 5, 1, '2021-06-30 11:19:19'),
(12, 22, 7, 1, '2021-06-30 11:20:17'),
(13, 24, 1, 1, '2021-06-30 11:38:39'),
(14, 24, 1, 1, '2021-06-30 12:06:52'),
(15, 24, 1, 1, '2021-06-30 12:07:38'),
(16, 22, 1, 1, '2021-06-30 12:08:03'),
(17, 24, 1, 1, '2021-06-30 12:09:00'),
(18, 22, 1, 1, '2021-06-30 12:09:25'),
(19, 28, 1, 1, '2021-06-30 12:27:45'),
(20, 24, 1, 1, '2021-06-30 12:35:48'),
(21, 24, 1, 1, '2021-06-30 12:37:37'),
(22, 24, 1, 1, '2021-06-30 12:38:52'),
(23, 28, 1, 1, '2021-06-30 12:39:59'),
(24, 27, 1, 1, '2021-06-30 12:40:46'),
(25, 26, 5, 1, '2021-06-30 12:48:30'),
(26, 27, 1, 1, '2021-06-30 12:56:37'),
(27, 28, 1, 1, '2021-06-30 12:57:10'),
(28, 27, 1, 1, '2021-06-30 12:57:29'),
(29, 28, 1, 1, '2021-06-30 12:58:06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `config_general`
--

DROP TABLE IF EXISTS `config_general`;
CREATE TABLE IF NOT EXISTS `config_general` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `descripcion` text NOT NULL,
  `activo` int(11) NOT NULL,
  `url` varchar(100) DEFAULT NULL,
  `MenusubId` int(11) NOT NULL,
  `perfilId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `config_general`
--

INSERT INTO `config_general` (`id`, `status`, `descripcion`, `activo`, `url`, `MenusubId`, `perfilId`) VALUES
(1, 0, 'sms', 1, NULL, 0, 0),
(2, 0, 'pagina incio', 1, 'Agenda', 0, 0),
(3, 0, '', 0, NULL, 1, 0),
(4, 0, '', 0, NULL, 1, 2),
(5, 0, '', 0, NULL, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gafete`
--

DROP TABLE IF EXISTS `gafete`;
CREATE TABLE IF NOT EXISTS `gafete` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo1` varchar(255) NOT NULL,
  `titulo2` varchar(255) NOT NULL,
  `titulo3` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `gafete`
--

INSERT INTO `gafete` (`id`, `titulo1`, `titulo2`, `titulo3`) VALUES
(1, 'doc_231017-141848mangoo.png', 'doc_231017-141855mangoo.png', 'Para cualquier información referente a\nesta producción, comunicarte a:\n(55-68-92-45)');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

DROP TABLE IF EXISTS `menu`;
CREATE TABLE IF NOT EXISTS `menu` (
  `MenuId` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) DEFAULT NULL,
  `Icon` varchar(45) DEFAULT NULL,
  `orden` int(11) NOT NULL,
  PRIMARY KEY (`MenuId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`MenuId`, `Nombre`, `Icon`, `orden`) VALUES
(1, 'Control Interno', 'icon-speedometer', 2),
(2, 'Catálogos', 'fa fa-list-alt', 3),
(3, 'Administrador\n', 'fa fa-lock', 4),
(4, 'Consultas', 'fa fa-list-alt', 1),
(5, 'Configuración', 'Configuracion', 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu_sub`
--

DROP TABLE IF EXISTS `menu_sub`;
CREATE TABLE IF NOT EXISTS `menu_sub` (
  `MenusubId` int(11) NOT NULL AUTO_INCREMENT,
  `MenuId` int(11) NOT NULL,
  `Nombre` varchar(50) DEFAULT NULL,
  `Pagina` varchar(120) DEFAULT NULL,
  `Icon` varchar(45) DEFAULT NULL,
  `orden` int(11) NOT NULL,
  PRIMARY KEY (`MenusubId`),
  KEY `fk_menu_sub_menu_idx` (`MenuId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `menu_sub`
--

INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `orden`) VALUES
(1, 0, 'Empleados', 'Empleados', 'fas fa-users', 1),
(2, 0, 'Check Points', 'Check_Points', 'fas fa-flag-checkered', 2),
(3, 0, 'Operacion', 'Operacion', 'fas fa-qrcode', 3),
(4, 0, 'Reportes', 'Reportes', 'fas fa-clipboard-list', 4),
(5, 5, 'Gafete', 'Gafete', 'fa fa-cogs', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfiles`
--

DROP TABLE IF EXISTS `perfiles`;
CREATE TABLE IF NOT EXISTS `perfiles` (
  `perfilId` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `estatus` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`perfilId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `perfiles`
--

INSERT INTO `perfiles` (`perfilId`, `nombre`, `estatus`) VALUES
(1, 'Administrador', 1),
(2, 'Operador', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfiles_detalles`
--

DROP TABLE IF EXISTS `perfiles_detalles`;
CREATE TABLE IF NOT EXISTS `perfiles_detalles` (
  `Perfil_detalleId` int(11) NOT NULL AUTO_INCREMENT,
  `perfilId` int(11) NOT NULL,
  `MenusubId` int(11) NOT NULL,
  PRIMARY KEY (`Perfil_detalleId`),
  KEY `fk_Perfiles_detalles_Perfiles1_idx` (`perfilId`),
  KEY `fk_Perfiles_detalles_menu_sub1_idx` (`MenusubId`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `perfiles_detalles`
--

INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES
(1, 1, 1),
(2, 1, 2),
(4, 1, 3),
(5, 1, 4),
(6, 2, 3),
(7, 2, 1),
(8, 1, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal`
--

DROP TABLE IF EXISTS `personal`;
CREATE TABLE IF NOT EXISTS `personal` (
  `personalId` int(11) NOT NULL AUTO_INCREMENT,
  `numero_empleado` bigint(20) NOT NULL,
  `nombre` varchar(300) NOT NULL,
  `puesto` varchar(255) NOT NULL,
  `celular` varchar(15) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `check_baja` varchar(2) NOT NULL,
  `fechabaja` date DEFAULT NULL,
  `motivo` text NOT NULL,
  `estatus` int(1) NOT NULL DEFAULT '1' COMMENT '1 visible 0 eliminado',
  `usuario` int(1) NOT NULL DEFAULT '0' COMMENT 'Es para saber cuando  ya sele haya agregad un usuario',
  `foto` varchar(255) NOT NULL,
  `color` varchar(20) NOT NULL,
  `tipo_empleado` int(1) NOT NULL,
  `reg` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`personalId`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `personal`
--

INSERT INTO `personal` (`personalId`, `numero_empleado`, `nombre`, `puesto`, `celular`, `correo`, `check_baja`, `fechabaja`, `motivo`, `estatus`, `usuario`, `foto`, `color`, `tipo_empleado`, `reg`) VALUES
(1, 0, 'Administrador', '', '2228340563', 'a@gmail.com', '', '2021-01-12', '', 1, 1, '', '', 1, '2021-06-06 00:00:00'),
(2, 0, 'prueba 1', '', '', '', 'on', '2021-01-07', 'eee', 0, 0, '', '', 0, '2021-01-07 11:25:27'),
(3, 1, 'prueba 2', '23424234', '4234234234', 'rwe@gmail.com', 'on', '2021-01-08', 'werewew\r\ndasd\r\nasdsd\r\ndasd\r\ndasd\r\ndasd\r\ndasd', 0, 0, '', '', 0, '2021-01-07 11:26:31'),
(4, 2, 'José Luis Peralta', '556668686', '240132323', 'jose@patito', '', '2021-03-01', 'por incumplimiento', 0, 1, '', '', 0, '2021-01-07 14:02:55'),
(5, 3, 'Mateo Gonzales', 'Actor', '4234234234', 'mant@gmail.com', '', '0000-00-00', '', 0, 1, '', '', 0, '2021-03-01 17:05:37'),
(6, 0, 'Gio Perez Lopez', 'Productor', '4234234234', 'gio@gmail.com', '', '0000-00-00', '', 0, 1, '', '', 0, '2021-03-01 17:07:00'),
(7, 0, 'Sergio Trujillo Conde', 'cocinero', '2491024472', 'ser@gmail.com', '', '0000-00-00', '', 0, 1, '', '', 0, '2021-03-01 17:10:38'),
(8, 0, 'Luis Garcia Torres', 'camarógrafo', '4234234234', 'luis@gmail.com', '', '0000-00-00', '', 0, 1, '', '', 0, '2021-03-03 13:07:32'),
(9, 1, 'Felipe Molares Torres', 'De video', '4234234234', 'feli@gmail.com', '', '0000-00-00', '', 0, 0, '', 'yellow', 0, '2021-03-03 15:58:32'),
(12, 0, 'Administrador', '', '', '', '', '2021-06-19', '', 0, 1, '', '#00bb2d', 0, '2021-06-13 00:00:00'),
(13, 0, 'Administrador a', '', '', '', '', '0000-00-00', '', 0, 1, 'doc_210602-163944img.PNG', '', 0, '2021-06-02 12:19:29'),
(14, 1, 'Prueba 1', '', '', '', '', '0000-00-00', '', 0, 0, '', 'red', 0, '2021-06-17 12:49:21'),
(15, 1, 'prueba 1', '', '', '', '', '0000-00-00', '', 0, 0, '', '#00bb2d', 0, '2021-06-17 12:54:30'),
(16, 2, 'prueba 2', '', '', '', '', '0000-00-00', '', 0, 0, '', '', 0, '2021-06-17 12:55:18'),
(17, 3, 'prueba 3', '', '', '', '', '0000-00-00', '', 0, 0, '', '', 0, '2021-06-17 12:55:42'),
(18, 4, 'prueba 4', '', '', '', '', '0000-00-00', '', 0, 0, '', '', 0, '2021-06-17 13:03:52'),
(19, 5, 'prueba 5', '', '', '', '', '0000-00-00', '', 0, 0, '', 'yellow', 0, '2021-06-17 13:04:03'),
(20, 5, 'prueba 6', '', '', '', '', '0000-00-00', '', 0, 0, '', '#03a9f4', 0, '2021-06-17 13:04:30'),
(21, 5, 'retret', '', '', '', '', '0000-00-00', '', 0, 0, '', '#00bb2d', 0, '2021-06-17 13:23:03'),
(22, 1, 'prueba1', '', '', '', '', '0000-00-00', '', 1, 0, '', '', 0, '2021-06-17 13:23:47'),
(23, 2, 'prueba 2', '', '', '', '', '0000-00-00', '', 0, 0, '', '#00bb2d', 0, '2021-06-17 13:23:58'),
(24, 3, 'prueba3', '', '', '', '', '0000-00-00', '', 1, 0, '', '#03a9f4', 0, '2021-06-17 13:24:09'),
(25, 4, 'prueba 4', '', '', '', '', '0000-00-00', '', 0, 0, '', '#03a9f4', 0, '2021-06-17 13:24:25'),
(26, 4, 'prueba5 ', '', '', '', '', '0000-00-00', '', 1, 0, '', '#03a9f4', 0, '2021-06-17 13:24:40'),
(27, 5, 'Pedro Ramirez Juares', 'cocinero', '4234234234', 'rwe@gmail.com', '', '0000-00-00', '', 1, 0, '', 'orange', 0, '2021-06-19 16:13:04'),
(28, 6, 'Alejandra Morales Molina ', 'camarógrafo', '4234234234', 'rwe@gmail.com', '', '0000-00-00', '', 1, 0, '', 'orange', 0, '2021-06-19 16:16:33');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `UsuarioID` int(11) NOT NULL AUTO_INCREMENT,
  `perfilId` int(11) NOT NULL,
  `personalId` int(11) NOT NULL,
  `Usuario` varchar(45) DEFAULT NULL,
  `contrasena` varchar(80) DEFAULT NULL,
  `estatus` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`UsuarioID`),
  KEY `fk_usuarios_Perfiles1_idx` (`perfilId`),
  KEY `fk_usuarios_personal1_idx` (`personalId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`UsuarioID`, `perfilId`, `personalId`, `Usuario`, `contrasena`, `estatus`) VALUES
(1, 1, 1, 'admin', '$2y$10$.yBpfsTTEmhYhECki.qy3eL45XaLo3MinuIPgiPsjmXBYItZXHUga', 1),
(2, 2, 6, 'raul', '$2y$10$UjYSqxZojtQV6yMcOvmypuR0lN1dIWIbwws6x3OcU/vkjgVLBHGWm', 1),
(3, 1, 7, 'sergio', '$2y$10$IeO3A.Dz0on5nyFMD2/23ucEHjS4IdbcOPoiYVbPy0XLnosmOCd/O', 1),
(4, 2, 5, 'mateo', '$2y$10$3Ow2FKRM.pN0cA8/HEr0nevi8LdvZOk3ndb/TGbcq05iNT0A4TBCK', 1),
(5, 2, 4, 'jose', '$2y$10$3Pzy71EsT33mbCe8qPTCKeKPPahVFkT4QBdk790TbD/WCXwaQGRbW', 1),
(7, 1, 13, 'admin1', '$2y$10$.yBpfsTTEmhYhECki.qy3eL45XaLo3MinuIPgiPsjmXBYItZXHUga', 1);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `check_points_detalles`
--
ALTER TABLE `check_points_detalles`
  ADD CONSTRAINT `check_points_detalles_ibfk_1` FOREIGN KEY (`idpoints_check`) REFERENCES `check_points` (`id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `check_points_detalles_ibfk_2` FOREIGN KEY (`personalId`) REFERENCES `personal` (`personalId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `check_points_detalles_ibfk_3` FOREIGN KEY (`idempleado`) REFERENCES `personal` (`personalId`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `perfiles_detalles`
--
ALTER TABLE `perfiles_detalles`
  ADD CONSTRAINT `fk_Perfiles_detalles_Perfiles1` FOREIGN KEY (`perfilId`) REFERENCES `perfiles` (`perfilId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Perfiles_detalles_menu_sub1` FOREIGN KEY (`MenusubId`) REFERENCES `menu_sub` (`MenusubId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `fk_usuarios_Perfiles1` FOREIGN KEY (`perfilId`) REFERENCES `perfiles` (`perfilId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`personalId`) REFERENCES `personal` (`personalId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
