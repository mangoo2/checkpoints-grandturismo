ALTER TABLE `check_points` ADD `unidad` VARCHAR(50) NOT NULL AFTER `id`, ADD `ruta` VARCHAR(50) NOT NULL AFTER `unidad`, ADD `empresa` VARCHAR(255) NOT NULL AFTER `ruta`;
/*ALTER TABLE `check_points` DROP `nombre`, DROP `codigo`;*/

ALTER TABLE `personal` ADD `appaterno` VARCHAR(300) NOT NULL AFTER `nombre`, ADD `apmaterno` VARCHAR(300) NOT NULL AFTER `appaterno`, ADD `operativo` VARCHAR(300) NOT NULL AFTER `apmaterno`, ADD `empresa` VARCHAR(250) NOT NULL AFTER `operativo`, ADD `tel_emergencia` VARCHAR(15) NOT NULL AFTER `empresa`, ADD `persona` VARCHAR(250) NOT NULL AFTER `tel_emergencia`, ADD `parentesco` VARCHAR(100) NOT NULL AFTER `persona`, ADD `sangre` VARCHAR(15) NOT NULL AFTER `parentesco`;
/*ALTER TABLE `personal`DROP `puesto`,DROP `celular`,DROP `correo`,DROP `color`;*/

CREATE TABLE `sicoinet_guanabana`.`operadores` ( `operadorId` INT(11) NOT NULL AUTO_INCREMENT , `nombre` VARCHAR(300) NOT NULL , `ap_paterno` VARCHAR(300) NOT NULL , `ap_materno` VARCHAR(300) NOT NULL , `telefono` VARCHAR(15) NOT NULL , `direccion` VARCHAR(300) NOT NULL , `fecha_ingreso` DATE NOT NULL , `vigencia_examen` DATE NOT NULL , `vigencia_licencia` DATE NOT NULL , `tipo_licencia` INT(11) NOT NULL , `usuario` INT(1) NOT NULL , `estatus` INT(1) NOT NULL , `reg` DATETIME NOT NULL , PRIMARY KEY (`operadorId`)) ENGINE = InnoDB;
ALTER TABLE `operadores` CHANGE `estatus` `estatus` INT(1) NOT NULL DEFAULT '1';

ALTER TABLE `usuarios` ADD `operadorId` INT(11) NOT NULL AFTER `personalId`;
/*ALTER TABLE `usuarios` CHANGE `operadorId` `operadorId` INT(11) NULL;*/
/*ALTER TABLE `usuarios` ADD FOREIGN KEY (`operadorId`) REFERENCES `operadores`(`operadorId`) ON DELETE RESTRICT ON UPDATE RESTRICT;*/

INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `orden`) VALUES (NULL, '0', 'Operadores', 'Operadores', 'fa fa-truck', '5');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '6');

CREATE TABLE `sicoinet_guanabana`.`operadores_documentos` ( `documentoId` INT(11) NOT NULL , `operadorId` INT(11) NOT NULL , `file` TEXT NOT NULL , `reg_file` DATETIME NOT NULL , `tipo` INT(11) NOT NULL COMMENT '1=comprobante, 2=identificacion,3=examedico,4=licencia' , `vigencia` DATE NOT NULL , `clase` INT(11) NOT NULL , `estatus` INT(1) NOT NULL ) ENGINE = InnoDB;
ALTER TABLE `operadores_documentos` ADD PRIMARY KEY(`documentoId`);
ALTER TABLE `operadores_documentos` CHANGE `documentoId` `documentoId` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `operadores_documentos` CHANGE `estatus` `estatus` INT(1) NOT NULL DEFAULT '1';
ALTER TABLE `operadores_documentos` ADD FOREIGN KEY (`operadorId`) REFERENCES `operadores`(`operadorId`) ON DELETE RESTRICT ON UPDATE RESTRICT;


INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `orden`) VALUES (NULL, '0', 'Empresas', 'Empresas', 'fa fa-building', '6');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '7');

CREATE TABLE `sicoinet_guanabana`.`empresas` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `nombre` VARCHAR(255) NOT NULL , `activo` INT(1) NULL DEFAULT '1' , `reg` DATETIME NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

/***************************19/04/24*********************************/
INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `orden`) VALUES (NULL, '0', 'Clientes', 'Clientes', 'fa fa-address-card', '7');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '8');

CREATE TABLE `sicoinet_guanabana`.`clientes` ( `clienteId` INT(11) NOT NULL AUTO_INCREMENT , `nombre` VARCHAR(300) NOT NULL , `appaterno` VARCHAR(300) NOT NULL , `apmaterno` VARCHAR(300) NOT NULL , `razon_social` VARCHAR(250) NOT NULL , `rfc` VARCHAR(15) NOT NULL , `telefono` VARCHAR(15) NOT NULL , `correo` VARCHAR(50) NOT NULL , `direccion` VARCHAR(300) NOT NULL , `empresa` INT(11) NOT NULL , `reg` DATETIME NOT NULL , `estatus` INT(1) NOT NULL DEFAULT '1' , PRIMARY KEY (`clienteId`)) ENGINE = InnoDB;
ALTER TABLE `usuarios` ADD `clienteId` INT(11) NULL AFTER `operadorId`;

INSERT INTO `perfiles` (`perfilId`, `nombre`, `estatus`) VALUES (NULL, 'Cliente', '1')
ALTER TABLE `usuarios` CHANGE `operadorId` `operadorId` INT(11) NOT NULL;
ALTER TABLE `usuarios` CHANGE `clienteId` `clienteId` INT(11) NOT NULL;
ALTER TABLE `clientes` ADD `usuario` INT(1) NOT NULL AFTER `empresa`;
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '3', '8')

INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '3', '1')
ALTER TABLE `personal` ADD `cliente` INT(11) NOT NULL AFTER `empresa`;

/*----------------------02 05 24---------------------*/
ALTER TABLE sicoinet_guanabana.usuarios DROP FOREIGN KEY usuarios_ibfk_1;
ALTER TABLE ``usuarios`` DROP INDEX ``fk_usuarios_personal1_idx``;

/*----------------------06 05 24---------------------*/
ALTER TABLE `check_points` CHANGE `empresa` `cliente` INT(11) NOT NULL;
ALTER TABLE `personal` DROP `empresa`;
DROP TABLE ` empresas `;

/*----------------------07 05 24---------------------*/
ALTER TABLE `clientes` DROP `empresa`;
DELETE FROM `perfiles_detalles` WHERE `perfiles_detalles`.`Perfil_detalleId` = 15
DELETE FROM `perfiles_detalles` WHERE `perfiles_detalles`.`Perfil_detalleId` = 17
DELETE FROM `menu_sub` WHERE `menu_sub`.`MenusubId` = 9

/*----------------------09 05 24---------------------*/
CREATE TABLE `sicoinet_guanabana`.`rutas` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `ruta` VARCHAR(200) NOT NULL , `choferId` INT(11) NOT NULL , `unidadId` INT(11) NOT NULL , `clienteId` INT(11) NOT NULL , `reg` DATETIME NOT NULL , `estatus` TINYINT(1) NOT NULL DEFAULT '1' , PRIMARY KEY (`id`)) ENGINE = InnoDB;
CREATE TABLE `sicoinet_guanabana`.`rutas_clientes` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `clienteId` INT(11) NOT NULL , `empleadoId` INT(11) NOT NULL , `rutaId` INT(11) NOT NULL , `tipo` INT(1) NOT NULL COMMENT '0=Bajada,1=Subida' , `reg` DATETIME NOT NULL , `estatus` TINYINT(1) NOT NULL DEFAULT '1' , PRIMARY KEY (`id`)) ENGINE = InnoDB;

/*----------------------- 14 05 24 -----------------------*/
INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `orden`) VALUES (NULL, '0', 'Rutas', 'Rutas', 'fa fa-route', '8'), (NULL, '0', 'SRutas', 'Seleccion_Rutas', 'fa fa-route', '9');
DELETE FROM `perfiles_detalles` WHERE `perfiles_detalles`.`Perfil_detalleId` = 17;
DELETE FROM `perfiles_detalles` WHERE `perfiles_detalles`.`Perfil_detalleId` = 18;

INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '11'), (NULL, '3', '12');
/*INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '12');*/


/*------------------------17 05 24-------------------------------*/

UPDATE `menu_sub` SET `Icon` = 'fa fa-car' WHERE `menu_sub`.`MenusubId` = 8;
INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `orden`) VALUES (NULL, '0', 'Unidades', 'Unidades', 'fa fa-car-side', '10');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '13');


CREATE TABLE `sicoinet_guanabana`.`unidades` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `unidad` VARCHAR(100) NOT NULL , `ruta` VARCHAR(100) NOT NULL , `cliente` INT(11) NOT NULL , `activo` INT NOT NULL DEFAULT '1' , `reg` DATETIME NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
DELETE FROM `perfiles_detalles` WHERE `perfiles_detalles`.`Perfil_detalleId` = 15

ALTER TABLE `check_points` DROP `unidad`, DROP `ruta`, DROP `cliente`;

/*-------------------------22 05 24------------------------------*/
UPDATE `check_points` SET `nombre` = 'Bajada', `reg` = '2024-05-22 18:05:31' WHERE `check_points`.`id` = 2;
UPDATE `check_points` SET `nombre` = 'Subida', `codigo` = '54321', `reg` = '2024-05-22 17:56:38' WHERE `check_points`.`id` = 1;
/*INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '2', '3');*/
ALTER TABLE `check_points_detalles` ADD `idcliente` INT(11) NOT NULL AFTER `idempleado`;
ALTER TABLE `check_points_detalles` ADD `idruta` INT(11) NOT NULL AFTER `idcliente`;

/*-------------------29 05 24--------------------*/
ALTER TABLE `clientes` ADD `foto` VARCHAR(255) NOT NULL AFTER `usuario`;
ALTER TABLE `check_points_detalles` ADD `longitud` FLOAT NOT NULL AFTER `idruta`, ADD `latitud` FLOAT NOT NULL AFTER `longitud`;
UPDATE `menu_sub` SET `Nombre` = 'Operación' WHERE `menu_sub`.`MenusubId` = 3;
DELETE FROM `perfiles_detalles` WHERE `perfiles_detalles`.`Perfil_detalleId` = 7;

/*-------------------07 06 24--------------------*/
DELETE FROM `perfiles_detalles` WHERE `perfiles_detalles`.`Perfil_detalleId` = 2;
DELETE FROM `perfiles_detalles` WHERE `perfiles_detalles`.`Perfil_detalleId` = 8;

/*-------------------03 07 24--------------------*/
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '3', '4');

/*---------------------09 07 24------------------------------*/
ALTER TABLE `unidades` ADD `placas` VARCHAR(10) NOT NULL AFTER `cliente`;

/*---------------------10 07 24------------------------------*/
ALTER TABLE `clientes` ADD `area` VARCHAR(150) NOT NULL AFTER `direccion`, ADD `color` VARCHAR(11) NOT NULL AFTER `area`;
CREATE TABLE `sicoinet_guanabana`.`unidades_placas` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `unidadId` INT(11) NOT NULL , `placas` VARCHAR(10) NOT NULL , `reg` DATETIME NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `unidades` CHANGE `ruta` `marca` VARCHAR(100) CHARACTER SET utf16 COLLATE utf16_spanish_ci NOT NULL, CHANGE `cliente` `modelo` VARCHAR(100) NOT NULL;
ALTER TABLE `unidades_placas` ADD `personalId` INT(11) NOT NULL AFTER `unidadId`;

/*---------------------16 08 24------------------------------*/
ALTER TABLE `operadores` ADD `tel_emergencia` VARCHAR(15) NOT NULL AFTER `especial`, ADD `persona` VARCHAR(250) NOT NULL AFTER `tel_emergencia`, ADD `parentesco` VARCHAR(100) NOT NULL AFTER `persona`, ADD `sangre` VARCHAR(15) NOT NULL AFTER `parentesco`;