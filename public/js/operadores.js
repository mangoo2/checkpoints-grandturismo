var base_url=$('#base_url').val();
var tabla;
var operadorId;

$(document).ready(function() {
    table();

    loadInputFile1();
    loadInputFile2();
    loadInputFile3();
    loadInputFile4();
});

function loadInputFile1(){ 
    var img_file=""; var imgdet=""; var typePDF="";
    if($("#comp_aux").val()!=""){
        img_file=''+base_url+"public/uploads/choferes/documentos/"+$("#comp_aux").val()+'';
        ext = $("#comp_aux").val().split('.');
        if(ext[1]!="pdf"){ 
            imgdet = {type:"image", url: ""+base_url+"Operadores/delete_document/"+$("#idDocComp").val(), caption: $("#comp_aux").val()}
            typePDF = "false";  
        }else{
            imgdet = {type:"pdf", url: base_url+"Operadores/delete_document/"+$("#idDocComp").val(), caption: $("#comp_aux").val()};
            typePDF = "true";
        }
    }

	$('#comprobante').fileinput({
		showCaption: false, // Ocultar titulo
		showUpload: false, // Ocultar el botón "Upload File"

		allowedFileExtensions: ["png", "jpg", "jpeg", "bmp", "pdf"],
		browseLabel: 'Seleccionar comprobante de domicilio',
		uploadUrl: base_url + 'Operadores/cargar_documentos',
		maxFilePreviewSize: 5000,
		elErrorContainer: '#kv-avatar-errors-1',
		msgErrorClass: 'alert alert-block alert-danger',
		initialPreview: [
			''+img_file+'',
		],
		initialPreviewAsData: typePDF,
		initialPreviewFileType: 'image', // image is the default and can be overridden in config below
		initialPreviewConfig: [
			imgdet
		],
		previewFileIconSettings: {
			'png': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpeg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'bmp': '<i class="fa fa-file-photo-o text-warning"></i>',
            'pdf': '<i class="fa fa-pdf-o text-warning"></i>'
		},
		uploadExtraData: function (previewId, index) {
			var info = {
				filetipo: 1,
				id_operador: operadorId,
				vigencia_exa: 0,
				vigencia_lic: 0,
				tipo_lic: 0
			};
			return info;
		}
	}).on('fileuploaded', function(event, files, extra) {
		//location.reload();
	}).on('filedeleted', function(event, files, extra) {
		//location.reload();
		$('#comprobante').inputfile("reset");
	});
	/////////////////////////////////////////////////////////////////////////
}

function loadInputFile2(){
    var img_file2=""; var imgdet2=""; var typePDF2="";
    if($("#iden_aux").val()!=""){
        img_file2=''+base_url+"public/uploads/choferes/documentos/"+$("#iden_aux").val()+'';
        ext2 = $("#iden_aux").val().split('.');
        if(ext2[1]!="pdf"){ 
            imgdet2 = {type:"image", url: ""+base_url+"Operadores/delete_document/"+$("#idDocIden").val(), caption: $("#iden_aux").val()}
            typePDF2 = "false";  
        }else{
            imgdet2 = {type:"pdf", url: base_url+"Operadores/delete_document/"+$("#idDocIden").val(), caption: $("#iden_aux").val()};
            typePDF2 = "true";
        }
    }

	$('#identificacion').fileinput({
		showCaption: false, // Ocultar titulo
		showUpload: false, // Ocultar el botón "Upload File"

		allowedFileExtensions: ["png", "jpg", "jpeg", "bmp", "pdf"],
		browseLabel: 'Seleccionar identificacion',
		uploadUrl: base_url + 'Operadores/cargar_documentos',
		maxFilePreviewSize: 5000,
		elErrorContainer: '#kv-avatar-errors-1',
		msgErrorClass: 'alert alert-block alert-danger',
		initialPreview: [
			''+img_file2+'',
		],
		initialPreviewAsData: typePDF2,
		initialPreviewFileType: 'image', // image is the default and can be overridden in config below
		initialPreviewConfig: [
			imgdet2
		],
		previewFileIconSettings: {
			'png': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpeg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'bmp': '<i class="fa fa-file-photo-o text-warning"></i>',
            'pdf': '<i class="fa fa-pdf-o text-warning"></i>'
		},
		uploadExtraData: function (previewId, index) {
			var info = {
				filetipo: 2,
				id_operador: operadorId,
				vigencia_exa: 0,
				vigencia_lic: 0,
				tipo_lic: 0
			};
			return info;
		}
	}).on('fileuploaded', function(event, files, extra) {
		//location.reload();
	}).on('filedeleted', function(event, files, extra) {
		//location.reload();
		$('#identificacion').inputfile("reset");
	});
	/////////////////////////////////////////////////////////////////////////
}

function loadInputFile3(){
    var img_file3=""; var imgdet3=""; var typePDF3="";
    if($("#exa_aux").val()!=""){
        img_file3=''+base_url+"public/uploads/choferes/documentos/"+$("#exa_aux").val()+'';
        ext3 = $("#exa_aux").val().split('.');
        if(ext3[1]!="pdf"){ 
            imgdet3 = {type:"image", url: ""+base_url+"Operadores/delete_document/"+$("#idDocExa").val(), caption: $("#exa_aux").val()}
            typePDF3 = "false";  
        }else{
            imgdet3 = {type:"pdf", url: base_url+"Operadores/delete_document/"+$("#idDocExa").val(), caption: $("#exa_aux").val()};
            typePDF4 = "true";
        }
    }

	$('#examen').fileinput({
		showCaption: false, // Ocultar titulo
		showUpload: false, // Ocultar el botón "Upload File"

		allowedFileExtensions: ["png", "jpg", "jpeg", "bmp", "pdf"],
		browseLabel: 'Seleccionar examen medico',
		uploadUrl: base_url + 'Operadores/cargar_documentos',
		maxFilePreviewSize: 5000,
		elErrorContainer: '#kv-avatar-errors-1',
		msgErrorClass: 'alert alert-block alert-danger',
		initialPreview: [
			''+img_file3+'',
		],
		initialPreviewAsData: typePDF3,
		initialPreviewFileType: 'image', // image is the default and can be overridden in config below
		initialPreviewConfig: [
			imgdet3
		],
		previewFileIconSettings: {
			'png': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpeg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'bmp': '<i class="fa fa-file-photo-o text-warning"></i>',
            'pdf': '<i class="fa fa-pdf-o text-warning"></i>'
		},
		uploadExtraData: function (previewId, index) {
			var info = {
				filetipo: 3,
				id_operador: operadorId,
				vigencia_exa: $("#vigencia_exa").val(),
				vigencia_lic: 0,
				tipo_lic: 0
			};
			return info;
		}
	}).on('fileuploaded', function(event, files, extra) {
		//location.reload();
	}).on('filedeleted', function(event, files, extra) {
		//location.reload();
		$('#examen').inputfile("reset");
	});
	/////////////////////////////////////////////////////////////////////////
}

function loadInputFile4(){
    var img_file4=""; var imgdet4=""; var typePDF4="";
    if($("#lic_aux").val()!=""){
        img_file4=''+base_url+"public/uploads/choferes/documentos/"+$("#lic_aux").val()+'';
        ext4 = $("#lic_aux").val().split('.');
        if(ext4[1]!="pdf"){ 
            imgdet4 = {type:"image", url: ""+base_url+"Operadores/delete_document/"+$("#idDocLic").val(), caption: $("#lic_aux").val()}
            typePDF4 = "false";  
        }else{
            imgdet4 = {type:"pdf", url: base_url+"Operadores/delete_document/"+$("#idDocLic").val(), caption: $("#lic_aux").val()};
            typePDF4 = "true";
        }
    }

	$('#licencia').fileinput({
		showCaption: false, // Ocultar titulo
		showUpload: false, // Ocultar el botón "Upload File"

		allowedFileExtensions: ["png", "jpg", "jpeg", "bmp", "pdf"],
		browseLabel: 'Seleccionar licencia de manejo',
		uploadUrl: base_url + 'Operadores/cargar_documentos',
		maxFilePreviewSize: 5000,
		elErrorContainer: '#kv-avatar-errors-1',
		msgErrorClass: 'alert alert-block alert-danger',
		initialPreview: [
			''+img_file4+'',
		],
		initialPreviewAsData: typePDF4,
		initialPreviewFileType: 'image', // image is the default and can be overridden in config below
		initialPreviewConfig: [
			imgdet4
		],
		previewFileIconSettings: {
			'png': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'jpeg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'bmp': '<i class="fa fa-file-photo-o text-warning"></i>',
            'pdf': '<i class="fa fa-pdf-o text-warning"></i>'
		},
		uploadExtraData: function (previewId, index) {
			var info = {
				filetipo: 4,
				id_operador: operadorId,
				vigencia_lic: $("#vigencia_lic").val(),
				tipo_lic: $("#tipo_lic option:selected").val(),
				vigencia_exa: 0
			};
			return info;
		}
	}).on('fileuploaded', function(event, files, extra) {
		//location.reload();
	}).on('filedeleted', function(event, files, extra) {
		//location.reload();
		$('#licencia').inputfile("reset");
	});
	/////////////////////////////////////////////////////////////////////////
}

function modal_registro(){
    $('#id_operador').val('0');
    $('#nombre').val('');
    $('#ap_paterno').val('');
    $('#ap_materno').val('');
    $('#fecha_ingreso').val('');
    $('#telefono').val('');
    $('#direccion').val('');

    $('#tel_emergencia').val('');
    $('#persona').val('');
    $('#parentesco').val('');
    $('#sangre').val('');

    $('#vigencia_exa').val('');
    $('#vigencia_lic').val('');
    $('#tipo_lic').val(1);

    $('#idDocComp').val('');
    $('#comp_aux').val('');
    $('#comprobante').fileinput('clear');

    $('#idDocIden').val('');
    $('#iden_aux').val('');
    $('#identificacion').fileinput('clear');

    $('#idDocExa').val('');
    $('#exa_aux').val('');
    $('#examen').fileinput('clear');

    $('#idDocLic').val('');
    $('#lic_aux').val('');
    $('#licencia').fileinput('clear');

    $('#especial').attr("checked",false);
    
	$('#registro_modal').modal();
}

function guarda_registro(){
    var form_register = $('#form_registro');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre:{
                required: true
            },
            ap_paterno:{
                required: true
            },
            fecha_ingreso:{
                required: true
            },
            telefono:{
                required: true
            },
            vigencia_examen:{
                required: true
            },
            vigencia_licencia:{
                required: true
            },
            tipo_licencia:{
                required: true
            },
            telefono: {
                maxlength: 10,
                minlength: 10,
                digits: true
            }
        },

        messages: {
            telefono: {
                maxlength: "Ingrese número telefónico de 10 dígitos.",
                digits: "Por favor, ingresa solo números"
            }
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_registro").valid();
    if($valid) {
        $('.btn_registro').attr('disabled',true);
        var datos = form_register.serialize();
        var especial=0;
        if($("#especial").is(":checked")==true){
            especial=1;
        }
        $.ajax({
            type:'POST',
            url: base_url+'Operadores/registra_datos',
            data: datos+"&especial="+especial,
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){
                //console.log("data: " + data);
				operadorId = parseInt(data);
                if($("#identificacion").val()!=""){
                    $('#identificacion').fileinput('upload');
                }

                if($("#comprobante").val()!=""){
                    $('#comprobante').fileinput('upload');
                }

                if($("#examen").val()!=""){
                    $('#examen').fileinput('upload');
                    idDocExa = 0;
                }

                if($("#licencia").val()!=""){
                    $('#licencia').fileinput('upload');
                    idDocLic = 0;
                }
                //update_vigencias(idDocLic, idDocExa);
                $.toast({
                    heading: 'Éxito',
                    text: 'Guardado Correctamente',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                  });
                setTimeout(function(){ 
                    var formID = $('#form_registro')
                    formID.validate().resetForm(); 
                    reload_registro();
                    $('.btn_registro').attr('disabled',false);
                }, 1500);
                $('#nombre-error').remove();
                $('#id_operador').val('0');
                $('#nombre').val('');
                $('#ap_paterno').val('');
                $('#ap_materno').val('');
                $('#fecha_ingreso').val('');
                $('#telefono').val('');
                $('#direccion').val('');
                $('#registro_modal').modal('hide');
            }
        });
    }   
}

function reload_registro(){
    tabla.destroy();
    table();
}

function table(){
	tabla=$("#table_datos").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Operadores/getlistado",
            type: "post",
            "data":{points:$('#operador').val()},
            error: function(){
                $("#table_datos").css("display","none");
            }
        },
        "columns": [
            {"data":"operadorId"},
            {"data":null,
            "render": function (row){
                var html = row.nombre + ' ' + row.ap_paterno + ' ' + row.ap_materno;
                return html;
                }
            },
            {"data":"telefono"},
            {"data":"fecha_ingreso"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    console.log("ROW: " + JSON.stringify(row));
                    
                    var color_usuario = '';
                    if (row.usuario == 1) {
                        color_usuario = 'btn-warning';
                    } else {
                        color_usuario = 'btn-secondary';
                    }

                    var html='<button type="button"  class="btn btn_sistema btn-circle points_datos_'+row.operadorId+'" data-perid="'+row.operadorId+'" data-nombre="'+row.nombre+'" data-ap_paterno="'+row.ap_paterno+'" data-ap_materno="'+row.ap_materno+'" data-telefono="'+row.telefono+'" data-direccion="'+row.direccion+'" data-tel_emergencia="' + row.tel_emergencia + '" data-persona="' + row.persona + '" data-parentesco="' + row.parentesco + '" data-sangre="' + row.sangre + '" data-fecha_ingreso="'+row.fecha_ingreso+'" data-vigencia_examen="'+row.vigencia_examen+'" data-vigencia_licencia="'+row.vigencia_licencia+'" data-tipo_licencia="'+row.tipo_licencia+'" data-especial="'+row.especial+'" onclick="modal_editar('+row.operadorId+')"><i class="far fa-edit"></i></button>\
                            <button type="button" class="btn ' + color_usuario + ' btn-circle" onclick="modal_usuario('+row.operadorId+');"><i class="fas fa-user"></i> </button>\
                            <button type="button" class="btn btn-danger btn-circle" onclick="modal_eliminar_regsitro('+row.operadorId+');"><i class="fas fa-trash-alt"></i> </button>';
                    return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
            }
        }
    
    });
}

function modal_editar(id){
    $('#id_operador').val($('.points_datos_'+id).data('perid'));
    $('#nombre').val($('.points_datos_'+id).data('nombre'));
    $('#ap_paterno').val($('.points_datos_'+id).data('ap_paterno'));
    $('#ap_materno').val($('.points_datos_'+id).data('ap_materno'));
    $('#telefono').val($('.points_datos_'+id).data('telefono'));
    $('#direccion').val($('.points_datos_'+id).data('direccion'));
    $('#fecha_ingreso').val($('.points_datos_'+id).data('fecha_ingreso'));

    $('#tel_emergencia').val($('.points_datos_'+id).data('tel_emergencia'));
    $('#persona').val($('.points_datos_'+id).data('persona'));
    $('#parentesco').val($('.points_datos_'+id).data('parentesco'));
    $('#sangre').val($('.points_datos_'+id).data('sangre'));

    $('#vigencia_exa').val($('.points_datos_'+id).data('vigencia_examen'));
    $('#vigencia_lic').val($('.points_datos_'+id).data('vigencia_licencia'));
    $('#tipo_lic').val($('.points_datos_'+id).data('tipo_licencia'));

    if($('.points_datos_'+id).data('especial')==1){
        $('#especial').attr("checked",true);
    }else{
        $('#especial').attr("checked",false);
    }
    $.ajax({
        type:'POST',
        url: base_url+"Operadores/get_documents",
        data:{id: $('.points_datos_'+id).data('perid') },
        success:function(data){
            json = JSON.parse(data);
            $('#idDocComp').val(json.idDComp);
            $('#comp_aux').val(json.comprobante);

            $('#idDocIden').val(json.idDIden);
            $('#iden_aux').val(json.identificacion);

            $('#idDocExa').val(json.idDExa);
            $('#exa_aux').val(json.examen);

            $('#idDocLic').val(json.idDLic);
            $('#lic_aux').val(json.licencia);


            $('#comprobante').fileinput('destroy');
            loadInputFile1();

            $('#identificacion').fileinput('destroy');
            loadInputFile2();

            $('#examen').fileinput('destroy');
            loadInputFile3();

            $('#licencia').fileinput('destroy');
            loadInputFile4();

        }
    });


    $('#registro_modal').modal();
}

function modal_eliminar_regsitro(id){ 
    $('#id_points').val(id);
    $('#elimina_registro_modal').modal();
}

function registro_delete(){
    var idp=$('#id_points').val(); 
    $.ajax({
        type:'POST',
        url: base_url+"Operadores/delete_registro",
        data:{id:idp},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Elimando Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
            $('#elimina_registro_modal').modal('hide');
            setTimeout(function(){ 
                tabla.ajax.reload(); 
            }, 1000); 
        }
    });  
}


function modal_usuario(id){
    $('#usuario_modal').modal();
    $('#idOpe').val(id);

    $.ajax({
        type:'POST',
        url: base_url+'Operadores/registro_usuario',
        data:{id:id},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $('.text_usuario').html(data);

            if ($('#usuario').val() == '') {
                $('#perfilId').val($('#tipoOpe').val());
            }
        }
    });
}
function guardar_usuario(){
    var form_register = $('#form_usuario');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            usuario:{
                required: true
            },
            contrasena:{
                required: true
            },
            contrasena2: {
                equalTo: contrasena,
                required: true
            },
            personalId: {
                required: true
            },
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_usuario").valid();
    if($valid) {
        $('.btn_registro').attr('disabled',true);
        var datos = form_register.serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Operadores/add_usuarios',
            data: datos,
            statusCode:{
                404: function(data){
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function(){
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg:'#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success:function(data){
                $.toast({
                    heading: 'Éxito',
                    text: 'Guardado Correctamente',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'success',
                    hideAfter: 3500, 
                    stack: 6
                });
                $('#usuario_modal').modal('hide');
                setTimeout(function(){ 
                    $('.btn_registro').attr('disabled',false);
                }, 1000);
                reload_registro();
            }
        });
    }   
}

function verificar_usuario(id){
    $.ajax({
        type: 'POST',
        url: base_url + 'Operadores/validar',
        data: {
            Usuario:$('#usuario').val(),
            Id:$('#idOpe').val(),
        },
        async: false,
        statusCode: {
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success: function(data) {
            if (data == 1) {
                $('.txt_usuario').html('El usuario ya existe');
                $('#btn_modal_usu').prop('disabled', true);
                $.toast({
                    heading: '¡Atención!',
                    text: 'El usuario ya existe',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });

            }else{
                $('.txt_usuario').html('');
                $('#btn_modal_usu').prop('disabled', false);
            }
        }
    });
}