var base_url = $('#base_url').val();
var perfil = $('#perfil').val();
var tabla;
var tabla_entrenamiento;
var aux_idcliente = 0;
var lat; var long;
$(document).ready(function () {
    table();
});

function modal_ruta() {
    $('#rutaId').val('0');
    $('#ruta').val('');
    $('#clienteId').val(0);
    $('#unidadId').val(0);
    $('#choferId').val(0);
    
    $('#registro_ruta').modal();
}

function guarda_ruta() {
    var form_register = $('#form_ruta');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1 = form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            ruta: {
                required: true
            },
            clienteId: {
                required: true,
                min: 1
            },
            unidadId: {
                required: true,
                min: 1
            },
            choferId: {
                required: true,
                min: 1
            },
        },
        messages: {
            clienteId: {
                min: "El cliente debe ser valido."
            },
            unidadId: {
                min: "La unidad debe ser valida."
            },
            choferId: {
                min: "El chofer debe ser valido."
            }
        },

        errorPlacement: function (error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")) {
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit              
            success_register.fadeOut(500);
            error_register.fadeIn(500);
            scrollTo(form_register, -100);

        },

        highlight: function (element) { // hightlight error inputs

            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_ruta").valid();

    if ($valid) {
        $('.btn_registro').attr('disabled', true);
        var datos = form_register.serialize();
        $.ajax({
            type: 'POST',
            url: base_url + 'Rutas/registra_ruta',
            data: datos,
            statusCode: {
                404: function (data) {
                    $.toast({
                        heading: '¡Error!',
                        text: 'No Se encuentra el archivo',
                        position: 'top-right',
                        loaderBg: '#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                },
                500: function () {
                    $.toast({
                        heading: '¡Error!',
                        text: '500',
                        position: 'top-right',
                        loaderBg: '#ff6849',
                        icon: 'error',
                        hideAfter: 3500
                    });
                }
            },
            success: function (data) {
                var id = data;
                //add_file(id);
                $.toast({
                    heading: 'Éxito',
                    text: 'Guardado Correctamente',
                    position: 'top-right',
                    loaderBg: '#ff6849',
                    icon: 'success',
                    hideAfter: 3500,
                    stack: 6
                });

                setTimeout(function(){ 
                    reload_registro()

                    $('#registro_ruta').modal('hide');

                    $('.btn_registro').attr('disabled',false);
                    
                }, 1500);

                /*
                setTimeout(function () {
                    window.location = base_url + 'Rutas';
                }, 1500);
                */
            }
        });
    }
}

function reload_registro() {
    tabla.destroy();
    table();
}

function table() {
    var cont = 0;
    tabla = $("#table_datos").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url + "Rutas/getlistado",
            type: "post",
            "data": {
                ruta: $('#ruta_busqueda').val(),
                particular: '0',
            },
            error: function () {
                $("#table_datos").css("display", "none");
            }
        },
        "columns": [{
                "data": null,
                "render": function (data, type, row, meta) {
                    var html = '</div>\
                        <div class="custom-control custom-checkbox mr-sm-2 mb-3">\
                            <input type="checkbox" class="custom-control-input check_id" name="check_id" id="check_id_' + cont + '">\
                            <label class="custom-control-label" for="check_id_' + cont + '"></label>\
                            <input type="hidden" name="rutaId" value="' + row.id + '">\
                        </div>';
                    cont++;
                    return html;
                }
            },
            {"data": "ruta"},
            {
                "data": null,
                "render": function (row) {
                    return row.nomChofer + ' ' + row.appChofer + ' ' + row.apmChofer;
                }
            },
            {"data": "unidad"},
            {
                "data": null,
                "render": function (row) {
                    return row.nomCliente + ' ' + row.appCliente + ' ' + row.apmCliente;
                }
            },
            {
                "data": null,
                "render": function (data, type, row, meta) {
                    //console.log("row "+ JSON.stringify(row));

                    var html = '';
                        
                    html += '<button type="button"\
                        data-rutaid="' + row.id + '"\
                        data-ruta="' + row.ruta + '"\
                        data-cliente="' + row.clienteId + '"\
                        data-unidad="' + row.unidadId + '"\
                        data-chofer="' + row.choferId + '"\
                        class="btn btn_sistema btn-circle ruta_datos_' + row.id + '" onclick="modal_editar(' + row.id + ')"><i class="far fa-edit"></i></button>';

                    html += '<button type="button" class="btn btn-danger btn-circle" onclick="eliminar_ruta(' + row.id + ');"><i class="fas fa-trash-alt"></i> </button>';
                    html += '<button title="Registrar bajada masiva" type="button" class="btn btn-danger" onclick="check_down('+row.id+');"><i class="fas fa-angle-double-down"></i> </button>';
                    
                    return html;

                }
            },
        ],
        "order": [
            [0, "desc"]
        ],
        "lengthMenu": [
            [10, 25, 50],
            [10, 25, 50]
        ],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
            }
        }

    });
}

function modal_editar(id) {
    $('#rutaId').val($('.ruta_datos_' + id).data('rutaid'));
    
    $('#ruta').val($('.ruta_datos_' + id).data('ruta'));
    $('#clienteId').val($('.ruta_datos_' + id).data('cliente'));
    $('#unidadId').val($('.ruta_datos_' + id).data('unidad'));
    $('#choferId').val($('.ruta_datos_' + id).data('chofer'));
    
    $('#registro_ruta').modal();
}

function check_baja_btn() {
    if ($('#verificar_check').is(':checked')) {
        $('.baja_texto').css('display', 'block');
    } else {
        $('.baja_texto').css('display', 'none');
    }
}

function eliminar_ruta(id) {
    $('#id_ruta').val(id);
    $('#elimina_ruta_modal').modal();
}

function delete_ruta() {
    var idr = $('#id_ruta').val();
    $.ajax({
        type: 'POST',
        url: base_url + "Rutas/delete_ruta",
        data: {
            id: idr
        },
        statusCode: {
            404: function (data) {
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg: '#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function () {
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg: '#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success: function (data) {
            $.toast({
                heading: 'Éxito',
                text: 'Elimando Correctamente',
                position: 'top-right',
                loaderBg: '#ff6849',
                icon: 'success',
                hideAfter: 3500,
                stack: 6
            });
            $('#elimina_ruta_modal').modal('hide');
            setTimeout(function () {
                tabla.ajax.reload();
            }, 1000);
        }
    });
}

function modal_usuario(id) {
    $('#usuario_modal').modal();
    $.ajax({
        type: 'POST',
        url: base_url + 'Rutas/registro_usuario',
        data: {
            id: id
        },
        statusCode: {
            404: function (data) {
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg: '#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function () {
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg: '#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success: function (data) {
            $('.text_usuario').html(data);
        }
    });
}

//===========================================================
function btn_check_todo() {
    if ($('#check_todo').is(':checked')) {
        $('.check_id').prop('checked', true);
    } else {
        $('.check_id').prop('checked', false);
    }
}

function btn_eliminar_registros() {
    $('#elimina_registro_modal').modal();
}

function delete_registros() {
    var DATA = [];
    var TABLA = $("#table_datos tbody > tr");
    TABLA.each(function () {
        item = {};
        if ($(this).find("input[name*='check_id']").is(':checked')) {
            console.log("CHECK");
            //item ["check_id"] = $(this).find("input[name*='check_id']").val();
            item["rutaId"] = $(this).find("input[name*='rutaId']").val();
            DATA.push(item);
        }
    });
    INFO = new FormData();
    aInfo = JSON.stringify(DATA);
    INFO.append('data', aInfo);

    $.ajax({
        data: INFO,
        type: 'POST',
        url: base_url + 'Rutas/registros_delete',
        processData: false,
        contentType: false,
        async: false,
        statusCode: {
            404: function (data) {
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function () {
                toastr.error('Error', '500');
            }
        },
        success: function (data) {
            tabla.ajax.reload();
            $.toast({
                heading: 'Éxito',
                text: 'Eliminado Correctamente',
                position: 'top-right',
                loaderBg: '#ff6849',
                icon: 'success',
                hideAfter: 3500,
                stack: 6
            });
            $('#elimina_registro_modal').modal('hide');
        }
    });
}

function btn_eliminar_registros_all() {
    $('#elimina_masivo_modal').modal();
}

function delete_registros_all() {
    $.ajax({
        type: 'POST',
        url: base_url + "Rutas/delete_registros",
        statusCode: {
            404: function (data) {
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg: '#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function () {
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg: '#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success: function (data) {
            $.toast({
                heading: 'Éxito',
                text: 'Elimando Correctamente',
                position: 'top-right',
                loaderBg: '#ff6849',
                icon: 'success',
                hideAfter: 3500,
                stack: 6
            });
            $('#elimina_masivo_modal').modal('hide');
            setTimeout(function () {
                tabla.ajax.reload();
            }, 1000);
        }
    });
}

////////////////////////////////////
function check_down(id) {
    $('#modal_check').modal();
    $("#idruta").val(id);
    getLatLong();
}

function saveCheck(){
    $.ajax({
        type: 'POST',
        url: base_url+'Rutas/save_checks',
        async:false,
        data: { id: $("#idruta").val(), lat:$("#lat").val(), long:$("#long").val(), fecha:$("#fecha_reg").val() },
        success: function (response) {
            var array=$.parseJSON(response);
            $('#modal_check').modal("hide");
            if(array.cont_emp>0){
                swal("Éxito", "Registro(s) guardado correctamente", "success");
            }if(array.cont_check==0){
                swal("Álerta", "No existe empleado para registrar bajada", "warning");
            }if(array.cont_checkb>0 && array.cont_emp==0){
                swal("Álerta", "Ya existen registros en la ruta y fecha seleccionada", "warning");
            }
        }
    });
}

function getLatLong(){
    if (navigator.geolocation) {
        var success = function (position) {
            lat = position.coords.latitude;
            long = position.coords.longitude;
            $("#lat").val(lat);
            $("#long").val(long);
        }
        navigator.geolocation.getCurrentPosition(success, function (msg) {
            swal("Error", "Geolocalización no activa.", "error");
        });
    } else {
        swal("Error", "Geolocalización no activa.", "error");
    }
}