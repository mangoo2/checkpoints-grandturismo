<?php
class Login_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    function login($usuario){
        $strq = "SELECT usu.UsuarioID,per.personalId,per.nombre, usu.perfilId, usu.contrasena,per.nombre AS administrador
                FROM usuarios as usu 
                INNER JOIN personal as per on per.personalId=usu.personalId
                where per.estatus = 1 AND usu.Usuario ='".$usuario."'";
                log_message('error', 'sql: '.$strq);
                $query = $this->db->query($strq);
                return $query->result();
    }

    function loginO($usuario){
        $strq = "SELECT usu.UsuarioID,per.operadorId,per.nombre, usu.perfilId, usu.contrasena, per.operadorId, per.nombre AS administrador
                FROM usuarios as usu 
                INNER JOIN operadores as per on per.operadorId=usu.operadorId
                where per.estatus = 1 AND usu.Usuario ='".$usuario."'";
                log_message('error', 'sql: '.$strq);
                $query = $this->db->query($strq);
                return $query->result();
    }

    function loginC($usuario){
        $strq = "SELECT usu.UsuarioID,per.clienteId,per.nombre, usu.perfilId, usu.contrasena, per.clienteId, per.nombre AS administrador
                FROM usuarios as usu 
                INNER JOIN clientes as per on per.clienteId=usu.clienteId
                where per.estatus = 1 AND usu.Usuario ='".$usuario."'";
                log_message('error', 'sql: '.$strq);
                $query = $this->db->query($strq);
                return $query->result();
    }

    function getMenus($perfil)
    {
        $strq ="SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='$perfil'";
        $query = $this->db->query($strq);
        return $query->result();
    } 

    function submenus($perfil,$menu){
        $strq ="SELECT menus.MenuId,menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon from menu_sub as menus, perfiles_detalles as perfd WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='$perfil' and menus.MenuId='$menu' ORDER BY menus.orden ASC";
        $query = $this->db->query($strq);
        return $query->result();
    }
    function get_record($table,$col,$id){
        $sql = "SELECT * FROM $table WHERE $col=$id";
        $query = $this->db->query($sql);
        return $query->row();
    }
    function getviewpermiso($perfil,$modulo){
        $strq = "SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=$perfil AND MenusubId=$modulo";
        $query = $this->db->query($strq);
        $total=0;
        foreach ($query->result() as $row) {
            $total=$row->total;
        }
        return $total; 
    }
    ////////// Alertas 
    public function validar_productos_almacen($fecha_mayor){
        $sql = "SELECT pa.*, p.producto FROM productos_almacen AS pa
                LEFT JOIN productos AS p ON p.idproducto=pa.idproducto 
                WHERE pa.activo=1 AND pa.alerta=1 AND pa.fecha_caducidad <='".$fecha_mayor."'";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_vigencia_lic_chofer(){
        $sql = "SELECT *
        FROM operadores AS o
        WHERE o.estatus = 1
        AND o.vigencia_licencia != 0000-00-00
        AND o.vigencia_licencia < DATE_ADD(CURDATE(), INTERVAL 1 MONTH) + INTERVAL 1 DAY";

        $query = $this->db->query($sql);
        return $query;
    }

    function get_vigencia_exa_chofer(){
        $sql = "SELECT *
        FROM operadores AS o
        WHERE o.estatus = 1
        AND o.vigencia_examen != 0000-00-00
        AND o.vigencia_examen < DATE_ADD(CURDATE(), INTERVAL 1 MONTH) + INTERVAL 1 DAY";

        $query = $this->db->query($sql);
        return $query;
    }

}