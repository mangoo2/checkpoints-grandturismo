<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gafete extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->idpersonal=$this->session->userdata('idpersonal');
        $this->administrador=$this->session->userdata('administrador');
        $this->perfilid=$this->session->userdata('perfilid');
        date_default_timezone_set('America/Mexico_City');
        $this->fecha_hora_actual = date('Y-m-d G:i:s');
        $this->fechainicio = date('Y-m-d');
        $this->inicioactual = date('G:i:s');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,5);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }
    public function index(){
        $data['gafete']=$this->General_model->get_record('id',1,'gafete');
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('gafete/vista',$data);
        $this->load->view('templates/footer');
        $this->load->view('gafete/vistajs');
    }

    function cargafiles1(){
        $id=$this->input->post('id');
        $folder="gafete";
        $upload_folder ='uploads/'.$folder;
        $nombre_archivo = $_FILES['foto']['name'];
        $tipo_archivo = $_FILES['foto']['type'];
        $tamano_archivo = $_FILES['foto']['size'];
        $tmp_archivo = $_FILES['foto']['tmp_name'];
        //$archivador = $upload_folder . '/' . $nombre_archivo;
        $fecha=date('ymd-His');
        $newfile='doc_'.$fecha.$nombre_archivo;        
        $archivador = $upload_folder . '/'.$newfile;
        if (!move_uploaded_file($tmp_archivo, $archivador)) {
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{
          $array = array('titulo1'=>$newfile);
          $this->General_model->edit_record('id',1,$array,'gafete');
          $return = Array('ok'=>TRUE);
        }
        echo json_encode($return);
    }  

    function cargafiles2(){
        $id=$this->input->post('id');
        $folder="gafete";
        $upload_folder ='uploads/'.$folder;
        $nombre_archivo = $_FILES['foto']['name'];
        $tipo_archivo = $_FILES['foto']['type'];
        $tamano_archivo = $_FILES['foto']['size'];
        $tmp_archivo = $_FILES['foto']['tmp_name'];
        //$archivador = $upload_folder . '/' . $nombre_archivo;
        $fecha=date('ymd-His');
        $newfile='doc_'.$fecha.$nombre_archivo;        
        $archivador = $upload_folder . '/'.$newfile;
        if (!move_uploaded_file($tmp_archivo, $archivador)) {
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{
          $array = array('titulo2'=>$newfile);
          $this->General_model->edit_record('id',1,$array,'gafete');
          $return = Array('ok'=>TRUE);
        }
        echo json_encode($return);
    }  

    public function guardar_titulo3(){
        $titulo = $this->input->post('titulo');
        $array = array('titulo3'=>$titulo);  
        $this->General_model->edit_record('id',1,$array,'gafete');
    }

}