<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->idpersonal=$this->session->userdata('idpersonal');
        $this->administrador=$this->session->userdata('administrador');
        $this->perfilid=$this->session->userdata('perfilid');
        date_default_timezone_set('America/Mexico_City');
        $this->fecha_hora_actual = date('Y-m-d G:i:s');
        $this->fechainicio = date('Y-m-d');
        $this->inicioactual = date('G:i:s');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,6);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

    public function index(){
        $data['perfil'] = $this->perfilid;

        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('clientes/index',$data);
        $this->load->view('templates/footer');
        $this->load->view('clientes/index_js');
    }

    public function registra_cliente(){
        $data = $this->input->post();
        //log_message('error', 'DATA: '.json_encode($data));
        $ID = $data['clienteId'];
        unset($data['clienteId']);
        $reg_id = 0;
        

        if($ID == 0){
            $data['reg'] = $this->fecha_hora_actual;
            $reg_id = $this->General_model->add_record('clientes',$data);
        }else{
            $reg_id = $this->General_model->edit_record('clienteId',$ID,$data,'clientes');
            $reg_id = $ID;
        }
        echo $reg_id;
    }

    public function getlistado(){
        $params = $this->input->post();

        $getdata = $this->ModelCatalogos->get_clientes($params);
        $totaldata= $this->ModelCatalogos->total_clientes($params); 

        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    public function delete_cliente(){
        $id = $this->input->post('id');
        $data = array('estatus'=>0);
        $this->General_model->edit_record('clienteId',$id,$data,'clientes');
        $this->General_model->edit_record('clienteId',$id,$data,'usuarios');
    }

    public function registro_usuario(){
        $id=$this->input->post('id');
        $UsuarioID=0;
        $perfilId=0;
        $Usuario='';
        $pass='';
        $result_usuario=$this->General_model->getselectwhere('usuarios','clienteId',$id);
        foreach ($result_usuario as $item){
            $UsuarioID=$item->UsuarioID;
            $perfilId=$item->perfilId;
            $personalId=$item->personalId;
            $Usuario=$item->Usuario;
            $pass='xxxxxxxxxx.';
        }
        $html='<form class="form" method="post" role="form" id="form_usuario">
                    <input type="hidden" name="UsuarioID" id="UsuarioID" value="'.$UsuarioID.'"> 
                    <div class="txt_usu">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Nombre de usuario <div style="color: red" class="txt_usuario"></div></label>
                                    <input type="text" name="usuario" id="usuario" autocomplete="nope" oninput="verificar_usuario('.$UsuarioID.')" value="'.$Usuario.'" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>  
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Contraseña</label>
                                <input type="password" name="contrasena" id="contrasena" autocomplete="new-password" value="'.$pass.'" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Verificar contraseña</label>
                                <input type="password" name="contrasena2" id="contrasena2" value="'.$pass.'" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Cliente</label>';
                                $result=$this->General_model->getselectwhere('clientes','clienteId',$id);
                                $html.='<input type="hidden" name="clienteId" readonly  class="form-control" value="'.$id.'">'; 
                                foreach ($result as $item) {
                                $html.='<input type="text" readonly class="form-control" value="'.$item->nombre.'">'; 
                                } 
                                $html.='
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Perfil</label>';
                                $resultp=$this->General_model->getselectwhere('perfiles','estatus',1);
                                $html.='<select name="perfilId" id="perfilId" class="form-control">';
                                foreach ($resultp as $item) {
                                    if($item->perfilId==$perfilId){
                                        $html.='<option value="'.$item->perfilId.'" selected>'.$item->nombre.'</option>';
                                    }else{
                                        $html.='<option value="'.$item->perfilId.'">'.$item->nombre.'</option>';
                                    }  
                                }
                                $html.='</select>
                        </div>
                        </div>
                    </div>   
                </form>';
        echo $html;
    }
    public function add_usuarios(){
        $datos = $this->input->post();
        $pss_verifica = $datos['contrasena'];
        $pass = password_hash($datos['contrasena'], PASSWORD_BCRYPT);
        $datos['contrasena'] = $pass;
        if($pss_verifica == 'xxxxxxxxxx.'){
            unset($datos['contrasena']);
        }
        $id=$datos['UsuarioID'];
        $personalId=$datos['clienteId'];
        unset($datos['UsuarioID']);
        unset($datos['contrasena2']);

        
        if ($id>0) {
            $where = array('UsuarioID'=>$id);
            $this->General_model->edit_recordw($where,$datos,'usuarios');
            $result=2;
        }else{
            //$datos['operadorId'] = '0';
            //$datos['personalId'] = '0';

            //log_message('error','DataUSUCliente: ' . json_encode($datos));

            $this->General_model->add_record('usuarios',$datos);
            $result=1;
            $this->General_model->edit_recordw(array('clienteId'=>$personalId),array('usuario'=>1),'clientes');
        }

        
        echo $result;
    }

    function validar(){
        $Usuario = $this->input->post('Usuario');
        $Id = $this->input->post('Id');
        //$result=$this->General_model->getselectwhere('usuarios','Usuario',$Usuario);
        $result = $this->General_model->getselectwhereall('usuarios',array('Usuario'=>$Usuario,'estatus'=>1));
        //log_message('error','RESULT: '.json_encode($result));
        $resultado=0;
        foreach ($result as $row) {
            if($Id != $row->clienteId){
                $resultado = 1;
            }
        }
        echo $resultado;
    }

    function cargafiles(){
        //log_message('error','CARGA FILES');
        $id=$this->input->post('id');
        $folder="clientes";
        $upload_folder ='uploads/'.$folder;
        $nombre_archivo = $_FILES['foto']['name'];
        $tipo_archivo = $_FILES['foto']['type'];
        $tamano_archivo = $_FILES['foto']['size'];
        $tmp_archivo = $_FILES['foto']['tmp_name'];
        //$archivador = $upload_folder . '/' . $nombre_archivo;
        $fecha=date('ymd-His');
        $newfile='doc_'.$fecha.$nombre_archivo;        
        $archivador = $upload_folder . '/'.$newfile;
        if (!move_uploaded_file($tmp_archivo, $archivador)) {
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{
          $array = array('foto'=>$newfile);
          $this->General_model->edit_record('clienteId',$id,$array,'clientes');
          $return = Array('ok'=>TRUE);
        }
        echo json_encode($return);
    }  

    function registros_delete(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);

        //log_message('error','DATA REGISTROS: '.json_encode($DATA));

        for ($i=0;$i<count($DATA);$i++) { 
            $clienteId = $DATA[$i]->clienteId;    
            $array = array('estatus'=>0);      
            $this->General_model->edit_record('clienteId',$clienteId,$array,'clientes');
            $this->General_model->edit_record('clienteId',$clienteId,$array,'usuarios');
        }
    }

    public function delete_registros(){
        $array = array('estatus'=>0);  
        $this->General_model->edit_record_all($array,'clientes');
        $this->General_model->edit_record_all($array,'usuarios');

        $array3 = array('estatus'=>1); 
        $this->General_model->edit_record('UsuarioID',1,$array3,'usuarios');
    }
    
}