    
    <footer class="footer">
            <span>Copyright © <?php echo date('Y'); ?> <a href="http://mangoo.mx/" target="_blank"> Mangoo Software</a></span>
    </footer>
 
    </div>
    <script src="<?php echo base_url();?>assets/node_modules/jquery/jquery-3.2.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/node_modules/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/node_modules/datatables.net-bs4/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url();?>assets/node_modules/popper/popper.min.js"></script>
    <script src="<?php echo base_url();?>assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/node_modules/toast-master/js/jquery.toast.js"></script>
    <script src="<?php echo base_url(); ?>/plugins/jquery-validation/jquery.validate.min.js"></script>
    <script src="<?php echo base_url();?>assets/dist/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="<?php echo base_url();?>dist/js/sidebarmenu.js"></script>
    <script src="<?php echo base_url();?>dist/js/custom.min.js"></script>
    <script src="<?php echo base_url();?>assets/node_modules/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>

    <script src="<?php echo base_url(); ?>plugins/fileinput/fileinput.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>plugins/blockUI/jquery.blockUI.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>plugins/alert/sweetalert.js"></script>
    
    <script type="text/javascript">
        if ('serviceWorker' in navigator) {
          window.addEventListener('load', function() {
            navigator.serviceWorker.register('<?php echo base_url();?>service-worker.js')
            .then(function(registration) {
              // Si es exitoso
              console.log('SW registrado correctamente');
            }, function(err) {
              // Si falla
              console.log('SW fallo', err);
            });
          });
        }
    </script>
</body>

</html>