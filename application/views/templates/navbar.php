<?php
$color='#0188ca';
if (!$this->session->userdata('logeado')) {
    redirect('Login');
    $perfilid =0;
}else{
    $perfilid=$this->session->userdata('perfilid');
    $personalId=$this->session->userdata('idpersonal');
    $administrador=$this->session->userdata('administrador');
    $foto=$this->session->userdata('foto');
    $this->fechainicio = date('Y-m-d');
    //$info=$this->Login_model->get_record('personal','personalId',$personalId);
}
?>
<style type="text/css">

  #sidebarnav>li>a {
    /*width: 200px !important; */
    padding: 0px 12px 0px 15px !important;
}

.sidebar-nav>ul>li>a i {
    width: 35px !important;
    font-size: 30px !important;
    display: inline-block;
    vertical-align: middle;
    color: <?php echo $color ?> !important;
}
.form-group {
    margin-bottom: 5px !important;
}
.vd_red{
    color: red;
}
.vd_green{
    color: green;
}
/* EStilos para adartar el color del logo */
.skin-blue .topbar, .skin-blue-dark .topbar {
    background: <?php echo $color ?> !important;
}
.sidebar-nav ul li a {
    color: <?php echo $color ?> !important;
}
.btn-info{
    background: <?php echo $color ?> !important;
}
.bg-info {
    background-color: <?php echo $color ?> !important;
}
.btn-outline-info {
    color: <?php echo $color ?> !important;
    border-color: <?php echo $color ?>!important;
}
.customtab2 li a.nav-link:hover {
    color: #fff;
    background: <?php echo $color ?> !important;
}
.customtab2 li a.nav-link.active {
    background: <?php echo $color ?> !important;
    color: #fff;
}
.icono-rey{
    color: <?php echo $color ?> !important;
}
.btn-outline-info:hover {
    color: #fff;
    background-color: #AD99A2 !important;
    border-color: <?php echo $color ?> !important;
}
.modal_sistema{
    background: <?php echo $color ?> !important;
    color: #fff !important;
}
.btn_sistema{
    background: <?php echo $color ?> !important;
    color: #fff !important;
}


.notification-box{
    /*position: relative;*/
    -webkit-animation: tada 2s ease infinite;
    animation: tada 2s ease infinite; 
}

.notification-box .dot-animated {
    position: absolute;
    right: -1px;
    top: -2px;
    width: 8px;
    height: 8px;
    border-radius: 100%;
    content: '';
    background-color: #d22d3d;
    -webkit-animation: fadeIn 2s ease infinite;
    animation: fadeIn 2s ease infinite;
}


/* Contenedor del dropdown */
.dropdown {
    position: relative;
    display: inline-block;
}

/* Botón del dropdown */
.dropbtn {
    cursor: pointer;
}

/* Contenido del dropdown (con scroll) */
.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 20vw;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
    max-height: 35vh; 
    overflow-y: auto; 
    list-style: none;
    right: 0;
}

/* Estilos de los enlaces dentro del dropdown */
.dropdown-content a {
    color: black;
    padding: 0px 16px;
    text-decoration: none;
    display: block;
}

/* Efecto hover en los enlaces */
.dropdown-content a:hover {
    background-color: #faeaeb;
}

/* Mostrar el dropdown al hacer hover sobre el botón */
.dropdown:hover .dropdown-content {
    display: block;
}



/* //// */
</style>
<!-- ============================================================== -->
<!-- Preloader - style you can find in spinners.css -->
<!-- ============================================================== -->
<div class="preloader">
    <div class="loader">
        <div class="loader__figure"></div>
        <p class="loader__label">Cargando</p>
    </div>
</div>

<div id="main-wrapper">
    <header class="topbar" >
        <nav class="navbar top-navbar navbar-expand-md navbar-dark">
            <div class="navbar-header" style="background-color: white !important;">
                <a class="navbar-brand">
                    <b>
                        <img src="<?php echo base_url();?>public/img/logo_color.png" alt="homepage" class="dark-logo" />
                        <img style="width:52px" src="<?php echo base_url();?>public/img/logo_color.png" alt="homepage" class="light-logo" />
                    </b>
                    <span class="img_unne" style="display: none;">
                     <img src="<?php echo base_url();?>images/gc.png" alt="homepage" class="dark-logo" />   
                    </span>
                </a>
            </div>
            <div class="navbar-collapse">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item"> 
                        <a class="nav-link nav-toggler d-block d-md-none waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> 
                    </li>
                    <li class="nav-item"> 
                        <a class="nav-link sidebartoggler d-none d-lg-block d-md-block waves-effect waves-dark" href="javascript:void(0)"><i class="icon-menu"></i></a> 
                    </li>
                    <li class="nav-item dropdown u-pro">
                        <a class="nav-link profile-pic">
                            <?php if($foto!=''){ ?> 
                                <img src="<?php echo base_url();?>assets/images/users/1.jpg" alt="user" class=""> 
                            <?php }?>
                            <span style="font-size: 15px; font-weight: bold;"><?php echo $administrador ?> </span> 
                        </a>
                    </li>
                </ul>



                <?php if ($this->perfilid == 1) { ?>
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <?php 
                            
                            $notiVigenciaLicChofer = $this->Login_model->get_vigencia_lic_chofer();
                            $notiVigenciaExaChofer = $this->Login_model->get_vigencia_exa_chofer();

                            $numNotifications = $notiVigenciaLicChofer->num_rows() + $notiVigenciaExaChofer->num_rows();
                        ?>

                        <div class="dropbtn mr-2 <?php echo $numNotifications > 0 ? 'notification-box' : '' ?>">
                            <i class="fas fa-bell nav-link "></i>
                            <?php echo $numNotifications > 0 ? '<span class="dot-animated"></span>' : '' ?>
                        </div>


                        <ul class="dropdown-content px-2">
                            <li class="border-bottom">
                                <p class="mb-0 p-2" style="font-size:120%; font-weight: bold;">Notificaciones <span class="pull-right badge badge-danger badge-pill"><?php echo $numNotifications; ?></span></p>
                            </li>

                            <?php foreach ($notiVigenciaLicChofer->result() as $item) { ?>
                            <li>
                                <div class="border-bottom">
                                    <a href="<?php echo base_url() . 'Operadores'; ?>">
                                        <div>
                                            <p><span style="color:#0188ca; font-size:110%; font-weight: bold;"><i class="fas fa-id-badge"> </i> Licencia por vencer <br>
                                            <?php echo $item->vigencia_licencia ?> </span> <br>
                                            Operador: <?php echo $item->nombre . " " . $item->ap_paterno . " " . $item->ap_materno; ?></p>
                                        </div>
                                    </a>
                                </div>
                            </li>
                            <?php } ?>

                            <?php foreach ($notiVigenciaExaChofer->result() as $item) { ?>
                            <li>
                                <div class="border-bottom">
                                    <a href="<?php echo base_url() . 'Operadores'; ?>">
                                        <div>
                                            <p><span style="color:#0188ca; font-size:110%; font-weight: bold;"><i class="fas fa-file-alt"> </i> Examen medico por vencer <br>
                                            <?php echo $item->vigencia_examen ?> </span> <br>
                                            Operador: <?php echo $item->nombre . " " . $item->ap_paterno . " " . $item->ap_materno; ?></p>
                                        </div>
                                    </a>
                                </div>
                            </li>
                            <?php } ?>

                        </ul>


                    </li>
                </ul>
                <?php } ?>



                <ul class="navbar-nav my-lg-0">
                    <li class="nav-item"> <a class="nav-link" style="font-size: 20px; font-weight: bold;" href="<?php echo base_url();?>Login/logout"><i class="fas fa-sign-out-alt"></i> Salir</a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <aside class="left-sidebar">
        <div class="scroll-sidebar">
            <nav class="sidebar-nav">
                <ul id="sidebarnav">
                    <?php 
                    $menusub=$this->Login_model->submenus($perfilid,0);
                    foreach ($menusub as $items) { ?>
                        <li> 
                            <a class="waves-effect waves-dark" href="<?php echo base_url();?><?php echo $items->Pagina ?>" aria-expanded="false"><i class="<?php echo $items->Icon ?>"></i> <span class="hide-menu"><?php echo $items->Nombre ?></span></a>
                        </li>
                    <?php } 
                    $menusub=$this->Login_model->submenus($perfilid,5);
                    foreach ($menusub as $items) { ?>
                        <li> 
                            <a class="waves-effect waves-dark" href="<?php echo base_url();?><?php echo $items->Pagina ?>" aria-expanded="false"><i class="<?php echo $items->Icon ?>"></i> <span class="hide-menu"><?php echo $items->Nombre ?></span></a>
                        </li>
                    <?php } ?>
                </ul>
            </nav>
        </div>
    </aside>
