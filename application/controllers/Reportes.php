<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reportes extends CI_Controller {
    function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->perfilid=$this->session->userdata('perfilid');
            $this->cliente = $this->session->userdata('cliente');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,4);// id del perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

    public function index(){
        $data['fecha_hoy']=$this->fecha_reciente;
        $data['cliente'] = 0;

        if ($this->perfilid == 3) {
            $data['cliente'] = $this->cliente;
        }

        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('reportes/reporte',$data);
        $this->load->view('templates/footer');
        $this->load->view('reportes/reportejs.php');
    }
    
    public function getlistado(){
        $params = $this->input->post();
        $getdata = $this->ModelCatalogos->get_reporte($params);
        $totaldata= $this->ModelCatalogos->total_reporte($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    public function excel_poins($f1,$f2,$points_check=0,$clientes=0,$rutas=0,$operadores=0,$buscar){
        $data['f1']=$f1;
        $data['f2']=$f2;
        $buscara = str_replace("%20"," ",$buscar);
        $data['get_poins'] = $this->ModelCatalogos->get_poins_excel($f1,$f2,$points_check,$clientes,$rutas,$operadores,$buscara);
        //log_message('error','DATA EXCEL: '. json_encode($data['get_poins']));
        $this->load->view('reportes/excel',$data);
    }

    function points_pdf($f1,$f2,$points_check=0,$clientes=0,$rutas=0,$operadores=0,$buscar){
        $data['f1']=$f1;
        $data['f2']=$f2;
        $buscara = str_replace("%20"," ",$buscar);
        $data['get_poins'] = $this->ModelCatalogos->get_poins_excel($f1,$f2,$points_check,$clientes,$rutas,$operadores,$buscara);
        $this->load->view('reportes/points',$data);
    }

    function points_inmpresion($f1,$f2,$points_check=0,$clientes=0,$rutas=0,$operadores=0,$buscar){
        $data['f1']=$f1;
        $data['f2']=$f2;
        $buscara = str_replace("%20"," ",$buscar);
        $data['get_poins'] = $this->ModelCatalogos->get_poins_excel($f1,$f2,$points_check,$clientes,$rutas,$operadores,$buscara);
        $this->load->view('reportes/impresion',$data);
        $this->load->view('reportes/imprimirjs');
    }

    public function get_select_rutas(){
        $idC = $this->input->post('idC');

        $result = $this->General_model->getselectwhereall('rutas',array('clienteId'=>$idC,'estatus'=>1));

        echo json_encode($result);
    }

    public function get_select_operadores(){
        $idC = $this->input->post('idC');

        $result = $this->General_model->getOperadorRuta($idC);
        //log_message('error','RESULT: '.json_encode($result));
        //$result2 = array_unique($result);
        //log_message('error','RESULT2: '.json_encode($result2));

        echo json_encode($result);
        //echo json_encode($result2);
    }

}    