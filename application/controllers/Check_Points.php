<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Check_Points extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,2);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }

	public function index(){
        $data['perfil']=$this->perfilid;
        $data['clientes'] = $this->General_model->get_records_condition('estatus = 1','clientes');

        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('check_points/listado',$data);
        $this->load->view('templates/footer');
        $this->load->view('check_points/listadojs');
    }
    public function registra_datos(){
        $data=$this->input->post();
        $id=$data['id'];
        unset($data['id']);

        //log_message('error','data: '.json_encode($data));
        
        if($id==0){
            $data['reg']=$this->fechahoy;
            $idaux = $this->General_model->add_record('check_points',$data);
        }else{
            $this->General_model->edit_record('id',$id,$data,'check_points');
            $idaux = $id;
        }
    
        echo $idaux;
        
    }

    public function getlistado(){
        $params = $this->input->post();
        $getdata = $this->ModelCatalogos->get_check_points($params);
        $totaldata= $this->ModelCatalogos->total_check_points($params); 
        
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        
        echo json_encode($json_data);
    }

    public function delete_registro(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('id',$id,$data,'check_points');
    }


    
}