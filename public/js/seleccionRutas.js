var base_url = $('#base_url').val();
var perfil = $('#perfil').val();
var tabla;
var tablaModal;
var tabla_entrenamiento;
var aux_idcliente = 0;

$(document).ready(function () {
    table()
});

function modal_ruta() {
    $('#rutaClienteId').val('0');
    $('#rutaId').val(0);
    $('#empleadoId').val(0);
    $('#clienteId').val(0);
    $('#tipo').val(0);
    
    //$('#registro_ruta').modal();
}

function guarda_ruta() {
    var form_register = $('#form_ruta');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1 = form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            empleadoId: {
                required: true,
                min: 1
            },
        },
        messages: {
            empleadoId: {
                min: "El personal debe ser valido."
            },
        },

        errorPlacement: function (error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")) {
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },

        invalidHandler: function (event, validator) { //display error alert on form submit              
            success_register.fadeOut(500);
            error_register.fadeIn(500);
            scrollTo(form_register, -100);

        },

        highlight: function (element) { // hightlight error inputs

            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_ruta").valid();

    $.ajax({
        type: 'POST',
        url: base_url + 'Seleccion_Rutas/searchDoubles',
        data: {
            idEmpleado:$('#empleadoId').val(),
            idRuta:$('#rutaId').val(),
        },
        statusCode: {
            404: function (data) {
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg: '#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function () {
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg: '#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success: function (data) {
            if(data > 0){
                $.toast({
                    heading: 'Error',
                    text: 'El personal ya ha sido asignado anteriormente a esta u otra ruta.',
                    position: 'top-right',
                    loaderBg: '#ff6849',
                    icon: 'error',
                    hideAfter: 3500,
                    stack: 6
                });
            }else{
                if ($valid) {
                    $('.btn_registro').attr('disabled', true);
                    var datos = form_register.serialize();
                    console.log('SAVE O: ' +datos);
                    $.ajax({
                        type: 'POST',
                        url: base_url + 'Seleccion_Rutas/registra_ruta',
                        data: datos,
                        statusCode: {
                            404: function (data) {
                                $.toast({
                                    heading: '¡Error!',
                                    text: 'No Se encuentra el archivo',
                                    position: 'top-right',
                                    loaderBg: '#ff6849',
                                    icon: 'error',
                                    hideAfter: 3500
                                });
                            },
                            500: function () {
                                $.toast({
                                    heading: '¡Error!',
                                    text: '500',
                                    position: 'top-right',
                                    loaderBg: '#ff6849',
                                    icon: 'error',
                                    hideAfter: 3500
                                });
                            }
                        },
                        success: function (data) {
                            var id = data;
                            //add_file(id);
                            $.toast({
                                heading: 'Éxito',
                                text: 'Guardado Correctamente',
                                position: 'top-right',
                                loaderBg: '#ff6849',
                                icon: 'success',
                                hideAfter: 3500,
                                stack: 6
                            });
            
                            setTimeout(function(){ 
                                //reload_registro()
            
                                //$('#registro_ruta').modal('hide');
                                $('.btn_registro').attr('disabled',false);
            
                            }, 1500);
            
                            
                            //Recargar tabla
                            //modal_datos($('#rutaId').val());

                            reload_table_modal($('#rutaId').val());
            
                            /*
                            setTimeout(function () {
                                window.location = base_url + 'Seleccion_Rutas';
                            }, 1500);
                            */
                        }
                    });
                }
            }
        }
    });
    
}


function reload_registro() {
    tabla.destroy();
    table();
}

function table() {
    var cont = 0;
    tabla = $("#table_datos").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url + "Seleccion_Rutas/getlistado",
            type: "post",
            "data": {
                ruta: $('#ruta_busqueda').val(),
                particular: '0',
            },
            error: function () {
                $("#table_datos").css("display", "none");
            }
        },
        "columns": [
            {
                "data": "ruta"
            },
            {
                "data": "unidad"
            },
            {
                "data":null,
                "render":function(row){
                    return row.nombre + ' ' + row.ap_paterno + ' ' + row.ap_materno;
                }
            },
            {
                "data": null,
                "render": function (data, type, row, meta) {
                    //console.log("row "+ JSON.stringify(row));
                    var html = '<button type="button" title="Ver más" class="btn btn-secondary btn-circle" onclick="modal_datos(' + row.id + ');"><i class="fas fa-eye"></i> </button>';
                    return html;
                }
            },
            /*
            {
                "data": null,
                "render": function (data, type, row, meta) {
                    console.log("row "+ JSON.stringify(row));

                    var html = '';
                        
                    html += '<button type="button"\
                        data-rutaclienteid="' + row.idRutasClientes + '"\
                        data-rutaid="' + row.id + '"\
                        data-empleadoid="' + row.empleadoId + '"\
                        data-clienteid="' + row.clienteId + '"\
                        data-tipo="' + row.tipo + '"\
                        class="btn btn_sistema btn-circle ruta_datos_' + row.id + '" onclick="modal_editar(' + row.id + ')"><i class="far fa-edit"></i></button>';

                    html += '<button type="button" class="btn btn-danger btn-circle" onclick="eliminar_ruta(' + row.id + ');"><i class="fas fa-trash-alt"></i> </button>';
                    
                    return html;

                }
            },
            */
        ],
        "order": [
            [0, "desc"]
        ],
        "lengthMenu": [
            [10, 25, 50],
            [10, 25, 50]
        ],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
            }
        }

    });
}

function modal_editar(id) {
    $('#rutaClienteId').val($('.ruta_datos_' + id).data('rutaclienteid'));
    
    $('#rutaId').val($('.ruta_datos_' + id).data('rutaid'));
    $('#empleadoId').val($('.ruta_datos_' + id).data('empleadoid'));
    $('#clienteId').val($('.ruta_datos_' + id).data('clienteid'));
    $('#tipo').val($('.ruta_datos_' + id).data('tipo'));
    
    $('#registro_ruta').modal();
}

function check_baja_btn() {
    if ($('#verificar_check').is(':checked')) {
        $('.baja_texto').css('display', 'block');
    } else {
        $('.baja_texto').css('display', 'none');
    }
}

function eliminar_ruta(id) {
    $('#id_ruta').val(id);
    $('#elimina_ruta_modal').modal();
}

function delete_ruta() {
    var idr = $('#id_ruta').val();
    $.ajax({
        type: 'POST',
        url: base_url + "Seleccion_Rutas/delete_ruta",
        data: {
            id: idr
        },
        statusCode: {
            404: function (data) {
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg: '#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function () {
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg: '#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success: function (data) {
            $.toast({
                heading: 'Éxito',
                text: 'Elimando Correctamente',
                position: 'top-right',
                loaderBg: '#ff6849',
                icon: 'success',
                hideAfter: 3500,
                stack: 6
            });
            $('#elimina_ruta_modal').modal('hide');
            setTimeout(function () {
                tabla.ajax.reload();
            }, 1000);
        }
    });
}

function modal_usuario(id) {
    $('#usuario_modal').modal();
    $.ajax({
        type: 'POST',
        url: base_url + 'Seleccion_Rutas/registro_usuario',
        data: {
            id: id
        },
        statusCode: {
            404: function (data) {
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg: '#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function () {
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg: '#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success: function (data) {
            $('.text_usuario').html(data);
        }
    });
}

function modal_datos(id = 0) {
    $('#datos_modal').modal();

    $('#selRuta').val(id);
    $('#rutaId').val(id);
    
    $('#empleadoId').val(0);
    $('#tipo').val(0);

    table_modal(id);
/*
    $.ajax({
        type: 'POST',
        url: base_url+'Seleccion_Rutas/get_data_cleintes_rutas',
        data: {id: id},
        statusCode: {
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success: function(data){
            console.log("data: "+data);
            const array = JSON.parse(data);
            
            $('#row_clientes_rutas').empty();

            if(array.length > 0) {
                array.forEach(function(element){
                    $('#label_datos').text('Personal de ruta "'+element[1]+'"');

                    get_table_CliRu(element[0], element[1], element[2], element[3]);
                });
            }
        }
    });
    */

}

function get_table_CliRu(id,ruta,empleadoId,tipo) {
    var html = '<div class="col-md-12 border-top pt-2" id="tr_cliru_' + id + '">\
                    <div class="row">\
                        <div class="col-md-3">\
                            <div class="form-group">\
                                <label class="col-form-label">' + ruta + '</label>\
                                \
                            </div>\
                        </div>\
                        \
                        <div class="col-md-3">\
                            <div class="form-group">\
                                <label class="col-form-label">' + empleadoId + '</label>\
                            </div>\
                        </div>\
                        \
                        <div class="col-md-3">\
                            <div class="form-group">\
                                <label class="col-form-label">' + tipo + '</label>\
                            </div>\
                        </div>\
                        \
                        <div class="col-md-1"> </div>\
                        \
                        <div class="col-md-2">\
                            <button type="button" class="btn btn-danger waves-effect btn_registro" title="Eliminar personal" onclick="eliminarCliRu(' + id + ')"><i class="fas fa-trash-alt" aria-hidden="true"></i></button>\
                        </div>\
                    </div>\
                </div>';

    $('#row_clientes_rutas').append(html);
}


function eliminarCliRu(id = 0){
    if(id==0){
        return;
    }
    $('#id_ruta').val(id);
    $('#elimina_ruta_modal').modal();
}

function delete_cliru() {
    var idr = $('#id_ruta').val();
    $.ajax({
        type: 'POST',
        url: base_url + "Seleccion_Rutas/delete_cliru",
        data: {
            id: idr
        },
        statusCode: {
            404: function (data) {
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg: '#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function () {
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg: '#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success: function (data) {
            $.toast({
                heading: 'Éxito',
                text: 'Elimando Correctamente',
                position: 'top-right',
                loaderBg: '#ff6849',
                icon: 'success',
                hideAfter: 3500,
                stack: 6
            });
            $('#elimina_ruta_modal').modal('hide');
            setTimeout(function () {
                //Recargar tabla
                reload_table_modal();
            }, 1000);
        }
    });
}


//===========================================================
function btn_check_todo() {
    if ($('#check_todo').is(':checked')) {
        $('.check_id').prop('checked', true);
    } else {
        $('.check_id').prop('checked', false);
    }
}

function btn_eliminar_registros() {
    $('#elimina_registro_modal').modal();
}

function delete_registros() {
    var DATA = [];
    var TABLA = $("#table_datos tbody > tr");
    TABLA.each(function () {
        item = {};
        if ($(this).find("input[name*='check_id']").is(':checked')) {
            console.log("CHECK");
            //item ["check_id"] = $(this).find("input[name*='check_id']").val();
            item["rutaId"] = $(this).find("input[name*='rutaId']").val();
            DATA.push(item);
        }
    });
    INFO = new FormData();
    aInfo = JSON.stringify(DATA);
    INFO.append('data', aInfo);

    $.ajax({
        data: INFO,
        type: 'POST',
        url: base_url + 'Seleccion_Rutas/registros_delete',
        processData: false,
        contentType: false,
        async: false,
        statusCode: {
            404: function (data) {
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function () {
                toastr.error('Error', '500');
            }
        },
        success: function (data) {
            tabla.ajax.reload();
            $.toast({
                heading: 'Éxito',
                text: 'Eliminado Correctamente',
                position: 'top-right',
                loaderBg: '#ff6849',
                icon: 'success',
                hideAfter: 3500,
                stack: 6
            });
            $('#elimina_registro_modal').modal('hide');
        }
    });
}

function btn_eliminar_registros_all() {
    $('#elimina_masivo_modal').modal();
}

function delete_registros_all() {
    $.ajax({
        type: 'POST',
        url: base_url + "Seleccion_Rutas/delete_registros",
        statusCode: {
            404: function (data) {
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg: '#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function () {
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg: '#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success: function (data) {
            $.toast({
                heading: 'Éxito',
                text: 'Elimando Correctamente',
                position: 'top-right',
                loaderBg: '#ff6849',
                icon: 'success',
                hideAfter: 3500,
                stack: 6
            });
            $('#elimina_masivo_modal').modal('hide');
            setTimeout(function () {
                tabla.ajax.reload();
            }, 1000);
        }
    });
}

function reload_table_modal() {
    tablaModal.destroy();
    table_modal($('#rutaId').val());
}

function table_modal(idRuta) {
    var cont = 0;
    tablaModal = $("#table_rutas_clientes").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        destroy: true,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url + "Seleccion_Rutas/getListadoRuCli",
            type: "post",
            "data": {
                rucli: $('#rucli_busqueda').val(),
                id: idRuta,
                particular: '0',
            },
            error: function () {
                $("#table_rutas_clientes").css("display", "none");
            }
        },
        "columns": [
            {
                "data": "ruta"
            },
            {
                "data":null,
                "render":function(row){
                    return row.nombre + ' ' + row.appaterno + ' ' + row.apmaterno;
                }
            },
            {
                "data": null,
                "render": function (data, type, row, meta) {
                    //console.log("row "+ JSON.stringify(row));
                    $('#label_datos').text('Personal de ruta "'+row.ruta+'"');

                    var html = '<button type="button" class="btn btn-danger waves-effect btn_registro" title="Eliminar personal" onclick="eliminarCliRu('+ row.id +')"><i class="fas fa-trash-alt" aria-hidden="true"></i></button>';
                    
                    return html;

                }
            },
        ],
        "order": [
            [0, "desc"]
        ],
        "lengthMenu": [
            [5, 10, 15],
            [5, 10, 15]
        ],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
            }
        }

    });
}