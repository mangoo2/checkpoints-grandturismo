<?php

    require_once('TCPDF/examples/tcpdf_include.php');
    require_once('TCPDF/tcpdf.php');
    $this->load->helper('url');
    //var_dump($GLOBALS['folio']);die;
//=======================================================================================
class MYPDF extends TCPDF {
    //Page header
    public function Header() {
        $html = '';        
        $this->writeHTML($html, true, false, true, false, ''); 
    }
    // Page footer
    public function Footer() {
        $html = ' '; 
          //<td align="right" class="footerpage">Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td> 
        $this->writeHTML($html, true, false, true, false, ''); 
    }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Reporte');
$pdf->SetTitle('Reporte');
$pdf->SetSubject('Reporte');
$pdf->SetKeywords('Reporte');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('10', '10', '10'); 
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER); 
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER); 
$pdf->SetFooterMargin('60'); 
// set auto page breaks
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 13);
// add a page
$pdf->AddPage('P', 'A4');
  $html='   <style >
                table {
                    width: 100%;
                    border: 1px solid #000;
                }
                th, td {
                    width: 25%;
                    text-align: left;
                    border: 1px solid #000;
                    font-size: 10px;
                }
            </style>
        <table border="1">
            <thead>
                <tr>
                    <th style="width: 100%;">Reporte '.$f1.' - '.$f2.'</th> 
                </tr>
            </thead>
        </table>
        <table border="1">
            <thead>

                <tr>
                    <th style="width: 5%;">ID</th> 
                    <th style="width: 25%;">Fecha y hora</th>
                    <th style="width: 20%;">Empleado</th>
                    <th style="width: 15%;">Ruta</th>
                    <th style="width: 20%;">Cliente</th>
                    <th style="width: 15%;">Check points</th>
                </tr>
            </thead>
            <tbody>';
            foreach ($get_poins as $i){
            $html.='<tr>
                    <td style="width: 5%;">'.$i->id.'</td> 
                    <td style="width: 25%;">'.$i->reg.'</td>
                    <td style="width: 20%;">'.$i->nombre.'</td>
                    <td style="width: 15%;">'.$i->ruta.'</td>
                    <td style="width: 20%;">'.$i->cliente.'</td>
                    <td style="width: 15%;">'.$i->poins.'</td>
                </tr>';
            }
            $html.='</tbody>
        </table>';

$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('Reporte.pdf', 'I');
//$pdf->Output('files/'.$GLOBALS["carpeta"].'/facturas/'.$GLOBALS["rrfc"].'_'.$GLOBALS["Folio"].'.pdf', 'F');
?>
