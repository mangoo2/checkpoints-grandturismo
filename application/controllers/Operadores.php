<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Operadores extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->idpersonal = $this->session->userdata('idpersonal');
        $this->administrador = $this->session->userdata('administrador');
        $this->perfilid = $this->session->userdata('perfilid');
        date_default_timezone_set('America/Mexico_City');
        $this->fecha_hora_actual = date('Y-m-d G:i:s');
        $this->fechainicio = date('Y-m-d');
        $this->inicioactual = date('G:i:s');
        if ($this->session->userdata('logeado')) {
            $this->idpersonal = $this->session->userdata('idpersonal');
            $this->perfilid = $this->session->userdata('perfilid');
            $permiso = $this->Login_model->getviewpermiso($this->perfilid, 1); // perfil y id del submenu
            if ($permiso == 0) {
                redirect('Login');
            }
        } else {
            redirect('/Login');
        }
    }

    public function index()
    {
        $data['perfil'] = $this->perfilid;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('operadores/operadores', $data);
        $this->load->view('templates/footer');
        $this->load->view('operadores/operadoresjs');
    }

    public function registra_datos()
    {
        $data = $this->input->post();
        $id = $data['id_operador'];
        unset($data['id_operador']);

        //log_message('error','data: '.json_encode($data));

        if ($id == 0) {
            $data['reg'] = $this->fecha_hora_actual;
            $idaux = $this->General_model->add_record('operadores', $data);
        } else {
            $this->General_model->edit_record('operadorId', $id, $data, 'operadores');
            $idaux = $id;
        }
        echo $idaux;
    }

    public function getlistado(){
        $params = $this->input->post();
        $getdata = $this->ModelCatalogos->get_operadores($params);
        $totaldata = $this->ModelCatalogos->total_opeardores($params);
        $json_data = array(
            "draw"            => intval($params['draw']),
            "recordsTotal"    => intval($totaldata),
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           => $this->db->last_query()
        );

        echo json_encode($json_data);
    }

    public function get_documents()
    {
        $id = $this->input->post('id');
        $data = array();
        $data['comprobante'] = '';
        $data['idDComp'] = '';
        $data['identificacion'] = '';
        $data['idDIden'] = '';
        $data['examen'] = '';
        $data['idDExa'] = '';
        $data['licencia'] = '';
        $data['idDLic'] = '';

        
        $resultDoc1 = $this->General_model->getselectwhere_orden_desc_row('operadores_documentos',array('operadorId'=>$id, 'tipo'=>1),'documentoId');
        if($resultDoc1 != null){
            if($resultDoc1->estatus > 0){
            $data['comprobante'] = $resultDoc1->file;
            $data['idDComp'] = $resultDoc1->documentoId;
            }
        }

        $resultDoc2 = $this->General_model->getselectwhere_orden_desc_row('operadores_documentos',array('operadorId'=>$id, 'tipo'=>2),'documentoId');
        if($resultDoc2 != null){
            if($resultDoc2->estatus > 0){
            $data['identificacion'] = $resultDoc2->file;
            $data['idDIden'] = $resultDoc2->documentoId;
            }
        }

        $resultDoc3 = $this->General_model->getselectwhere_orden_desc_row('operadores_documentos',array('operadorId'=>$id, 'tipo'=>3),'documentoId');
        if($resultDoc3 != null){
            if($resultDoc3->estatus > 0){
            $data['examen'] = $resultDoc3->file;
            $data['idDExa'] = $resultDoc3->documentoId;
            }
        }

        $resultDoc4 = $this->General_model->getselectwhere_orden_desc_row('operadores_documentos',array('operadorId'=>$id, 'tipo'=>4),'documentoId');
        if($resultDoc4 != null){
            if($resultDoc4->estatus > 0){
            $data['licencia'] = $resultDoc4->file;
            $data['idDLic'] = $resultDoc4->documentoId;
            }
        }
        

        //log_message('error','DATA DOC 1: ' . json_encode($data));
        echo json_encode($data);
    }

    public function delete_registro()
    {
        $id = $this->input->post('id');
        $data = array('estatus' => 0);
        $this->General_model->edit_record('operadorId', $id, $data, 'operadores');
        $this->General_model->edit_record('operadorId', $id, $data, 'usuarios');
    }

    function validar(){
        $Usuario = $this->input->post('Usuario');
        $Id = $this->input->post('Id');
        //$result = $this->General_model->getselectwhere('usuarios','Usuario',$Usuario);
        $result = $this->General_model->getselectwhereall('usuarios',array('Usuario'=>$Usuario,'estatus'=>1));
        //log_message('error','RESULT: '.json_encode($result));
        $resultado = 0;

        foreach ($result as $row) {
            if($Id != $row->operadorId){
                $resultado = 1;
            }
        }

        echo $resultado;
    }


    function cargar_documentos()
    {
        $filetipo = $_POST['filetipo'];
        $chofer = $_POST['id_operador'];
        $vigencia_exa = $_POST['vigencia_exa'];
        $vigencia_lic = $_POST['vigencia_lic'];
        $tipo_lic = $_POST['tipo_lic'];

        $input_name = 'file';
        $DIR_SUC = FCPATH . 'public/uploads/choferes/documentos';
        $config['upload_path']          = $DIR_SUC;
        $config['allowed_types']        = 'gif|jpg|png|jpeg|bmp|pdf';
        $config['max_size']             = 5000;
        $file_names = 'c'.$chofer.'_'.'t'.$filetipo.'_'.date('YmdGis');
        $config['file_name'] = $file_names;
        $output = [];

        $this->load->library('upload', $config);
        if (!$this->upload->do_upload($input_name)) {
        $data = array('error' => $this->upload->display_errors());
        //log_message('error',"DOC: ". json_encode($data));
        } else {
        $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
        $file_name = $upload_data['file_name']; //uploded file name
        $extension = $upload_data['file_ext'];    // uploded file extension

        if($filetipo == 1 || $filetipo == 2){
            $id = $this->General_model->add_record('operadores_documentos', array('operadorId'=> $chofer, 'file' => $file_name, 'reg_file' => date("Y-m-d H:i:s"), 'tipo' => $filetipo));
        }else if($filetipo == 3){
            $id = $this->General_model->add_record('operadores_documentos', array('operadorId'=> $chofer, 'file' => $file_name, 'reg_file' => date("Y-m-d H:i:s"), 'tipo' => $filetipo, 'vigencia' => $vigencia_exa));
        }else if($filetipo == 4){
            $id = $this->General_model->add_record('operadores_documentos', array('operadorId'=> $chofer, 'file' => $file_name, 'reg_file' => date("Y-m-d H:i:s"), 'tipo' => $filetipo, 'vigencia' => $vigencia_lic, 'clase' => $tipo_lic));
        }

        $data = array('upload_data' => $this->upload->data());
        }
        echo json_encode($output);
    }


    function delete_document($id)
    {
        $result = $this->General_model->edit_recordw(array('documentoId'=> $id),array('estatus'=>0),'operadores_documentos');
        echo $id;
    }

    public function registro_usuario(){
        $id=$this->input->post('id');
        $UsuarioID=0;
        $perfilId=0;
        $Usuario='';
        $pass='';
        $result_usuario=$this->General_model->getselectwhere('usuarios','operadorId',$id);

        foreach ($result_usuario as $item){
            $UsuarioID=$item->UsuarioID;
            $perfilId=$item->perfilId;
            $personalId=$item->personalId;
            $Usuario=$item->Usuario;
            $pass='xxxxxxxxxx.';
        }

        $html = '<form class="form" method="post" role="form" id="form_usuario">
                    <input type="hidden" name="UsuarioID" id="UsuarioID" value="'.$UsuarioID.'"> 
                    <div class="txt_usu">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Nombre de usuario <div style="color: red" class="txt_usuario"></div></label>
                                    <input type="text" name="usuario" id="usuario" autocomplete="nope" oninput="verificar_usuario('.$UsuarioID.')" value="'.$Usuario.'" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>  
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Contraseña</label>
                                <input type="password" name="contrasena" id="contrasena" autocomplete="new-password" value="'.$pass.'" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Verificar contraseña</label>
                                <input type="password" name="contrasena2" id="contrasena2" value="'.$pass.'" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Operador</label>';
                                $result=$this->General_model->getselectwhere('operadores','operadorId',$id);
                                $html.='<input type="hidden" name="operadorId" readonly  class="form-control" value="'.$id.'">'; 
                                foreach ($result as $item) {
                                $html.='<input type="text" readonly class="form-control" value="'.$item->nombre.'">'; 
                                } 
                                $html.='
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Perfil</label>';
                                $resultp=$this->General_model->getselectwhere('perfiles','estatus',1);
                                $html.='<select name="perfilId" id="perfilId" class="form-control">';
                                foreach ($resultp as $item) {
                                    if($item->perfilId==$perfilId){
                                        $html.='<option value="'.$item->perfilId.'" selected>'.$item->nombre.'</option>';
                                    }else{
                                        $html.='<option value="'.$item->perfilId.'">'.$item->nombre.'</option>';
                                    }  
                                }
                                $html.='</select>
                        </div>
                        </div>
                    </div>   
                </form>';
        echo $html;
    }

    public function add_usuarios(){
        $datos = $this->input->post();
        $pss_verifica = $datos['contrasena'];
        $pass = password_hash($datos['contrasena'], PASSWORD_BCRYPT);
        $datos['contrasena'] = $pass;
        if($pss_verifica == 'xxxxxxxxxx.'){
            unset($datos['contrasena']);
        }
        $id=$datos['UsuarioID'];
        $personalId=$datos['operadorId'];
        unset($datos['UsuarioID']);
        unset($datos['contrasena2']);
        if ($id>0) {
            $where = array('UsuarioID'=>$id);
            $this->General_model->edit_recordw($where,$datos,'usuarios');
            $result=2;
        }else{
            $this->General_model->add_record('usuarios',$datos);
            $result=1;
            $this->General_model->edit_recordw(array('operadorId'=>$personalId),array('usuario'=>1),'operadores');
        }   
        echo $result;
    }

}
