<input type="hidden" id="perfil" value="<?php echo $perfil ?>">
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12"><br>
                <div class="card">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6 mb-2">
                                        <?php if ($perfil == 1) { ?>
                                            <button type="button" class="btn btn-outline-info" onclick="modal_cliente()"><i class="fas fa-user-plus"></i> Nuevo Cliente</button>
                                        <?php } ?>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group bootstrap-touchspin">
                                            <span class="input-group-text">Buscar Cliente &nbsp;&nbsp; <i class="fas fa-search"></i></span></span><input id="cliente_busqueda" type="text" class="form-control" placeholder="Escriba nombre del cliente" oninput="reload_registro()">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-2 mt-2">
                                        <div class="custom-control custom-checkbox mr-sm-2 mb-3">
                                            <input type="checkbox" class="custom-control-input" id="check_todo" onclick="btn_check_todo()">
                                            <label class="custom-control-label" for="check_todo">Seleccionar todo</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 mt-2">
                                        <?php if ($perfil == 1) { ?>
                                            <button type="button" class="btn waves-effect waves-light btn-sm btn-danger" onclick="btn_eliminar_registros()"><i class="fas fa-trash-alt"></i> Eliminar Selección</button>
                                        <?php } ?>
                                    </div>
                                    <div class="col-lg-2 mt-2">
                                        <?php if ($perfil == 1) { ?>
                                            <button type="button" class="btn waves-effect waves-light btn-sm btn-info" onclick="btn_eliminar_registros_all()"><i class="fas fa-trash-alt"></i> Eliminar Todos</button>
                                        <?php } ?>
                                    </div>
                                    <div class="col-lg-12">
                                        <table class="table" id="table_datos">
                                            <thead class="bg-blue">
                                                <tr>
                                                    <th></th>
                                                    <th>ID</th>
                                                    <th>Nombre del cliente</th>
                                                    <th>Razón social</th>
                                                    <th>Numero</th>
                                                    <th>Correo</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="registro_cliente" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header modal_sistema">
                <div class="col-lg-1"></div>
                <div class="col-lg-10" align="center">
                    <h4 class="modal-title">Registro de cliente</h4>
                </div>
                <div class="col-lg-1">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" id="foto_validar" value="0">
                                <form class="form" method="post" role="form" id="form_cliente">
                                    <input type="hidden" name="clienteId" id="clienteId" value="0">

                                    <div class="row">

                                        <div class="col-md-3">
                                            <div class="foto_client">
                                                <img id="img_avatar" style="width: 145px; height: 145px; border-radius: 70px;" src="<?php echo base_url(); ?>images/annon.png">    
                                            </div>
                                            <span class="foto_avatar" >
                                                <input type="file" id="foto_avatar" style="display: none" accept=".jpg, .jpeg, .png">
                                            </span>
                                            <label for="foto_avatar">
                                                <span class="btn waves-effect waves-light btn-secondary"><i class="fas fa-camera"></i> Cambiar foto</span>
                                            </label>
                                        </div>

                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label><span style="color: red;">*</span> Nombre</label>
                                                        <input type="text" name="nombre" id="nombre" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Apellido Paterno</label>
                                                        <input type="text" name="appaterno" id="appaterno" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Apellido Materno</label>
                                                        <input type="text" name="apmaterno" id="apmaterno" class="form-control">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Razón Social</label>
                                                        <input type="text" name="razon_social" id="razon_social" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>RFC</label>
                                                        <input type="text" name="rfc" id="rfc" class="form-control">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Teléfono</label>
                                                        <input type="text" name="telefono" id="telefono" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Correo</label>
                                                        <input type="mail" name="correo" id="correo" class="form-control">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Dirección</label>
                                                        <input type="text" name="direccion" id="direccion" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Área</label>
                                                        <input type="text" name="area" id="area" class="form-control">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Color</label>
                                                        <input type="color" name="color" id="color" class="form-control">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info waves-effect btn_registro" onclick="guarda_cliente()">Guardar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div id="elimina_cliente_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea borrar éste cliente? Esta operación no se puede deshacer.</h5>
            </div>
            <input type="hidden" id="id_cliente">
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="delete_cliente()">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div id="usuario_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Usuario</h4>
                    <input type="hidden" id="idCli">
                    <input type="hidden" id="tipoCli" value="3">
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="text_usuario"></div>
            </div>
            <input type="hidden" id="id_cliente">
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" id="btn_modal_usu" class="btn btn_sistema waves-effect btn_registro" onclick="guardar_usuario()">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div id="elimina_registro_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea borrar estos registros? Esta operación no se puede deshacer.</h5>
            </div>
            <input type="hidden" id="id_cliente">
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="delete_registros()">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div id="elimina_masivo_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea borrar todos los registros? Esta operación no se puede deshacer.</h5>
            </div>
            <input type="hidden" id="id_cliente">
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="delete_registros_all()">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="iframe_datos" style="display: none;">
</div>