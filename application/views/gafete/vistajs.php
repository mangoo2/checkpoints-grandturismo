<link href="<?php echo base_url();?>public/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<script src="<?php echo base_url();?>public/js/fileinput.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/js/locales/es.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>public/js/gafete.js?v=<?php echo date('YmdGis') ?>"></script>
<script>
	<?php 
        $img=$gafete->titulo1;
	    if ($img!='') { ?>
		var url1 = '<?php echo base_url().'uploads/gafete/'.$img ?>';	
	<?php } ?>
    setTimeout(function(){ 
        $('.kv-file-remove').css('display','none');
    }, 500);
	
	$("#titulo1").fileinput({
		<?php if ($img!='') { ?>
    		initialPreview: [url1],
    		initialPreviewAsData: true,
    	<?php } ?>

            showCaption: false,
            showUpload: false,// quita el boton de upload
            //rtl: true,
            allowedFileExtensions: ["jpg","jpeg","png"],
            language: 'es',
        });

    
    <?php 
        $img2=$gafete->titulo2;
	    if ($img2!='') { ?>
		var url1 = '<?php echo base_url().'uploads/gafete/'.$img2 ?>';	
	<?php } ?>
	
	$("#titulo2").fileinput({
		<?php if ($img2!='') { ?>
    		initialPreview: [url1],
    		initialPreviewAsData: true,
    	<?php } ?>

            showCaption: false,
            showUpload: false,// quita el boton de upload
            //rtl: true,
            allowedFileExtensions: ["jpg","jpeg","png"],
            language: 'es',
        });

</script>