<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Operacion extends CI_Controller {
	function __construct()    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        if ($this->session->userdata('logeado')){
            $this->perfilid=$this->session->userdata('perfilid');
            $this->operador = $this->session->userdata('operador');
            $this->rutaId = $this->session->userdata('rutaId');
            //log_message('error',"Operador = ".$this->session->userdata('operador'));
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,3);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }
	public function index(){
        if($this->perfilid > 1){
            $data['rutas'] = $this->General_model->getselectwhereall('rutas',array('choferId'=>$this->operador,'estatus' => 1));
        }else{
            $data['rutas'] = $this->General_model->getselectwhereall('rutas',array('estatus' => 1));
        }

        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('escaneo/escaneo',$data);
        $this->load->view('templates/footer');
        $this->load->view('escaneo/escaneojs.php');
    }
    public function camara($id){
        $data['ponit']=$id;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('escaneo/camara',$data);
        $this->load->view('templates/footer');
        $this->load->view('escaneo/camarajs.php');
    }

    function validarcodigo(){
        $params = $this->input->post();
        $codigo = $params['codigo'];
        $point = $params['point'];
        $clic = $params['clic'];
        $latitud = $params['latitud'];
        $longitud = $params['longitud'];

        $personalId = 0; $personalId_aux=0;
        $clienteId = 0;
        $rutaId = 0;
        $especialp=0; $especialc=0;

        $array = array('numero_empleado' => $codigo, 'estatus' => 1);
        $dataEmpleado = $this->General_model->getselectwhereall2('personal', $array);
        $dataEmpleado = $dataEmpleado->row();
        //log_message("error", "Array 1: " . json_encode($dataEmpleado));

        if (!is_null($dataEmpleado)) {
            $personalId = $dataEmpleado->personalId;
            $personalId_aux=$personalId;
            $clienteId = $dataEmpleado->cliente;
            $especialp = $dataEmpleado->especial;
            $rutaId = $this->session->userdata('rutaId');
        }
        
        $array2 = array('clienteId' => $clienteId, 'empleadoId' => $personalId, 'rutaId' => $rutaId, 'estatus' => 1);
        $dataRuta = $this->General_model->getselectwhereall2('rutas_clientes', $array2);
        $dataRuta = $dataRuta->row();
        //log_message("error", "Array 2: " . json_encode($dataRuta));

        if (is_null($dataRuta)) {
            $array3 = array('empleadoId' => $personalId, 'estatus' => 1);
            $dataSearch = $this->General_model->getselectwhereall2('rutas_clientes', $array3);
            $dataSearch = $dataSearch->row();
            //log_message("error", "Array 3: " . json_encode($dataSearch));
            if (is_null($dataSearch)) {
                //log_message("error", "Empleado Sin Ruta");
                $personalId = '-1';
            } else {
                //log_message("error", "Empleado En otra ruta Ruta");
                $personalId = '-2';
            }
        }

        if (is_null($dataEmpleado)) {
            $personalId = '0';
        }

        /* *****************/
        if($especialp==1){ //empleado especial
            $where = array('clienteId' => $clienteId, 'empleadoId' => $personalId_aux, 'estatus' => 1);
            $get_ruta = $this->General_model->getselectwhereall2('rutas_clientes', $where);
            if($get_ruta->num_rows()>0){
                $get_ruta = $get_ruta->row();
                $personalId = $get_ruta->empleadoId;
                $rutaId = $get_ruta->rutaId;
            }
        }

        /* *****************/
        //chofer especial
        $where = array('operadorId' => $this->operador);
        $get_chofe = $this->General_model->getselectwhereall2('operadores', $where);
        if($get_chofe->num_rows()>0){
            $get_chofe = $get_chofe->row();
            $especialc=$get_chofe->especial;
            if($especialc!=1){
                $personalId= "-3";
                $array2 = array('clienteId'=>$clienteId,'empleadoId'=>$personalId_aux,'rutaId'=>$this->rutaId,'estatus'=>1);
                $dataRuta = $this->General_model->getselectwhereall2('rutas_clientes', $array2);
                if($dataRuta->num_rows()>0){
                    $dataRuta = $dataRuta->row();
                    $personalId=$dataRuta->empleadoId;
                }
            }else{ //especial
                $especialc=$personalId_aux;
            }
        }
        
        //log_message("error", "PersonalID: " . json_encode($personalId));
        if ($personalId > 0) {
            $data['personalId'] = $personalId;
            $data['idcliente'] = $clienteId;
            $data['idruta'] = $rutaId;
            $data['idpoints_check'] = $point;
            $data['idempleado'] = $this->idpersonal;
            $data['latitud'] = $latitud;
            $data['longitud'] = $longitud;
            $data['reg'] = $this->fechahoy;

            if ($clic == 0) {
                $id = $this->General_model->add_record('check_points_detalles', $data);
            }
        }
        echo $personalId;
    }

/*
    function validarcodigo(){
        $params = $this->input->post();
        $codigo = $params['codigo'];
        $point = $params['point'];
        $clic = $params['clic'];
        $latitud = $params['latitud'];
        $longitud = $params['longitud'];

        $idRutasDelOperador = array();
        $perteneceAruta = false;

        //$resultado=$this->General_model->getselectwhere('personal','personalId',$codigo);

        $personalId = 0;
        $clienteId = 0;
        $rutaId = 0;


        $array = array('numero_empleado'=>$codigo,'estatus'=>1);
        $resultado = $this->General_model->getselectwhereall2('personal',$array);
        $resultado = $resultado->row();
        log_message("error","Array 1: " . json_encode($resultado));

        if (!is_null($resultado)) {
            $personalId = $resultado->personalId;
            $clienteId = $resultado->cliente;
        }


        $array2 = array('empleadoId'=>$personalId,'clienteId'=>$clienteId,'estatus'=>1);
        $resultado2 = $this->General_model->getselectwhereall2('rutas_clientes',$array2);
        $resultado2 = $resultado2->row();
        log_message("error","Array 2: " . json_encode($resultado2));

        //Rutas del operador INICIO
        
        if($this->perfilid > 1)
        {
            $idRutasDelOperador = $this->General_model->obtener_ids_por_tabla('rutas', array('choferId'=>$this->operador,'estatus'=>1), 'id');
            log_message("error","Data DESDE QR: " . json_encode($idRutasDelOperador));
        }
        
        //Rutas del operador FINAL

        if (!is_null($resultado2)) {
            $rutaId = $resultado2->rutaId;


            if($this->perfilid > 1)
            {
                $perteneceAruta = in_array($rutaId, $idRutasDelOperador);

                if(!$perteneceAruta){
                    log_message('error', "El Personal no esta asignado a esta ruta!!!");
                    $personalId = "-2";
                }
            }


        }else if(!is_null($resultado)){
            $personalId = "-1";
        }

        //log_message('error','Resultado1: ' . json_encode($resultado));
        //log_message('error','Resultado2: ' . json_encode($resultado2));


        if($personalId>0){
            $data['personalId'] = $personalId;
            $data['idcliente'] = $clienteId;
            $data['idruta'] = $rutaId;
            $data['idpoints_check'] = $point;
            $data['idempleado'] = $this->idpersonal;
            $data['latitud'] = $latitud;
            $data['longitud'] = $longitud;
            $data['reg'] = $this->fechahoy;
            
            if($clic==0){
                $id = $this->General_model->add_record('check_points_detalles',$data);
            }
        }
        
        echo $personalId;
    }
*/
    public function empleado($id,$point){
        $data['get_emp']=$this->General_model->get_record('personalId',$id,'personal');
        $data['check_points']=$this->General_model->get_record('id',$point,'check_points');
        $data['point']=$point;
        log_message('error','DATOS: '.json_encode($data['get_emp']));
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('escaneo/empleado',$data);
        $this->load->view('templates/footer');
        $this->load->view('escaneo/empleadojs.php');
        //$this->load->view('escaneo/escaneojs.php');
    }

    public function set_ruta_session() {
        $id = $this->input->post('IDruta');
        if($this->perfilid > 1){
            $rutas = $this->General_model->getselectwhereall('rutas',array('choferId'=>$this->operador,'estatus' => 1));
        }else{
            $rutas = $this->General_model->getselectwhereall('rutas',array('estatus' => 1));
        }
        $inRutasChofer = false;
        
        $data_session = array('rutaId' => '0');

        foreach($rutas as $ruta){
            if($id == $ruta->id){
                $inRutasChofer = true;
            }
        }
        
        if($inRutasChofer){
            $data_session = array('rutaId' => $id);
        }
        
        $this->session->set_userdata($data_session);
    }

    public function get_ruta_session() {
        echo $this->session->userdata('rutaId');
    }

    public function checkSubidasBajadas(){
        $params = $this->input->post();
        $codigo = $params['codigo'];
        //$point = $params['point'];
        $existe = 0;


        $array = array('numero_empleado' => $codigo, 'estatus' => 1);
        $dataEmpleado = $this->General_model->getselectwhereall2('personal', $array);
        $dataEmpleado = $dataEmpleado->row();

        if (!is_null($dataEmpleado)) {
            $personalId = $dataEmpleado->personalId;
            log_message('error','ID: '.$personalId);
            $diferencia = $this->General_model->checkSubidaBajada($personalId);
            log_message('error','Diferencia: '.$diferencia);

            if($diferencia >= 1){
                $existe = 1;
            }
            
        }

        
        log_message('error','CODIGO: '.$codigo);
        log_message('error','EXISTE?: '.$existe);

        
        
        
        echo $existe;
    }
    
}