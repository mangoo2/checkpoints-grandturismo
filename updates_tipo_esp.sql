/* **********************10-07-2024*********************************/
ALTER TABLE `personal` ADD `especial` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=si' AFTER `tipo_empleado`;
ALTER TABLE `operadores` ADD `especial` VARCHAR(1) NOT NULL DEFAULT '0' COMMENT '0=no, 1=si' AFTER `direccion`;