<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12"><br>
                <div class="card">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12" align="center"> 
                                        <h1>Escaneo QR</h1>
                                    </div>
                                    <div class="col-md-3" align="center"></div>
                                    <div class="col-md-6" align="center"> 
                                        <select id="points_check" class="form-control">
                                            <?php $result=$this->General_model->getselectwhere('check_points','activo',1);
                                            foreach ($result as $item){
                                                echo '<option value="'.$item->id.'">'.$item->nombre.'</option>';
                                            }
                                            ?>
                                        </select>
                                    </div> 
                                    <div class="col-md-12" align="center"><br>
                                        <button type="button"  class="btn waves-effect waves-light btn-rounded btn_sistema" onclick="escaneo()">Realizar escaneo</button>
                                    </div>   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>      