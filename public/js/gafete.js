var base_url = $('#base_url').val();
$(document).ready(function() {
    
});    
function guardar_titulo1(){
	var archivo=$('#titulo1').val();
    //var name=$('#foto_avatar').val()
	    var extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
	    var extensiones_permitidas = new Array(".jpeg",".png",".jpg");
	    var permitida = false;
	    if($('#titulo1')[0].files.length > 0) {
	        for (var i = 0; i < extensiones_permitidas.length; i++) {
	           if (extensiones_permitidas[i] == extension) {
	           permitida = true;
	           break;
	           }
	        }  
	        if(permitida==true){
	          //console.log("tamaño permitido");
	            var inputFileImage = document.getElementById('titulo1');
	            var file = inputFileImage.files[0];
	            var data = new FormData();
	            data.append('foto',file);
	            $.ajax({
	                url:base_url+'Gafete/cargafiles1',
	                type:'POST',
	                contentType:false,
	                data:data,
	                processData:false,
	                cache:false,
	                success: function(data) {
	                    var array = $.parseJSON(data);
	                    if (array.ok=true) {
	                        $.toast({
	                            heading: 'Éxito',
	                            text: 'Guardado Correctamente',
	                            position: 'top-right',
	                            loaderBg:'#ff6849',
	                            icon: 'success',
	                            hideAfter: 3500, 
	                            stack: 6
	                        });
	                    }else{
	                        $.toast({
	                            heading: '¡Error!',
	                            text: 'No Se encuentra el archivo',
	                            position: 'top-right',
	                            loaderBg:'#ff6849',
	                            icon: 'error',
	                            hideAfter: 3500
	                        });
	                    }
	                }
	            });
	        }
	    }   
}

function guardar_titulo2(){
	var archivo=$('#titulo2').val();
    //var name=$('#foto_avatar').val()
    var extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase();
    var extensiones_permitidas = new Array(".jpeg",".png",".jpg");
    var permitida = false;
    if($('#titulo2')[0].files.length > 0) {
        for (var i = 0; i < extensiones_permitidas.length; i++) {
           if (extensiones_permitidas[i] == extension) {
           permitida = true;
           break;
           }
        }  
        if(permitida==true){
          //console.log("tamaño permitido");
            var inputFileImage = document.getElementById('titulo2');
            var file = inputFileImage.files[0];
            var data = new FormData();
            data.append('foto',file);
            $.ajax({
                url:base_url+'Gafete/cargafiles2',
                type:'POST',
                contentType:false,
                data:data,
                processData:false,
                cache:false,
                success: function(data) {
                    var array = $.parseJSON(data);
                    if (array.ok=true) {
                        $.toast({
                            heading: 'Éxito',
                            text: 'Guardado Correctamente',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'success',
                            hideAfter: 3500, 
                            stack: 6
                        });
                    }else{
                        $.toast({
                            heading: '¡Error!',
                            text: 'No Se encuentra el archivo',
                            position: 'top-right',
                            loaderBg:'#ff6849',
                            icon: 'error',
                            hideAfter: 3500
                        });
                    }
                }
            });
        }
    }   
}

function guardar_titulo3(){
	$.ajax({
        type:'POST',
        url: base_url+"Gafete/guardar_titulo3",
        data:{titulo:$('#titulo3').val()},
        statusCode:{
            404: function(data){
                $.toast({
                    heading: '¡Error!',
                    text: 'No Se encuentra el archivo',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            },
            500: function(){
                $.toast({
                    heading: '¡Error!',
                    text: '500',
                    position: 'top-right',
                    loaderBg:'#ff6849',
                    icon: 'error',
                    hideAfter: 3500
                });
            }
        },
        success:function(data){
            $.toast({
                heading: 'Éxito',
                text: 'Guardado Correctamente',
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: 'success',
                hideAfter: 3500, 
                stack: 6
            });
        }
    });  
}