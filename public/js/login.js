$(function () {
	$("#loader").hide();
	$("#success").hide();
    $("#error").hide();
    //$('.login-box').animateCss("zoomIn");

	$('#sign_in').validate({
        rules: {
            usuario: "required",
            
        },
        messages: {
            usuario: "Ingrese su nombre de usuario",
            password: "Ingrese su contraseña"
            
        },
        submitHandler: function (form) {
             $.ajax({
                 type: "POST",
                 url: base_url+"index.php/Login/login",
                 data: $(form).serialize(), 
                 beforeSend: function(){
                    $("#loader").show();
                    $('#success').hide();
                    $('#error').hide();
                 },
                 success: function (result) {
                    console.log(result);
                    setTimeout(function () { $('#loader').fadeOut(); }, 50);
                    if(result==1){
                    	//setTimeout(function () { $('#success').fadeIn(); }, 300);
                    	//setTimeout(function () { location.reload(); }, 2000);
                        $('.login_texto').css('display','none'); 
                        $('.sesion_texto').css('display','block'); 
                        setTimeout(function(){window.location.href = base_url+"Sistema"},3500);

                    }
                    else{
                    	setTimeout(function () { $('#error').fadeIn(); }, 500);
                    }
                    
                    
                 }
             });
             return false; // required to block normal submit for ajax
         },
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        }
    });
});

