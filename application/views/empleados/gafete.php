<style type="text/css">
	.margen_todo {
		border-style: solid;
		border-width: 10px;
		border-radius: 15px;
		border-color: <?php echo $get_empresa->color; ?>;
	}
	
</style>
<table class="margen_todo" width="90%">
	<tbody>
		<tr>
			<td width="45%" class="margen_todo" align="center">
				<table width="100%">
					<tbody>
						<tr align="center">
							<td>
								<br>
								<?php if ($get_empresa->foto != '') { ?>
									<img id="img_empresa" style="width: 200px; height: 150px; border-radius: 10px;" src="<?php echo base_url() ?>uploads/clientes/<?php echo $get_empresa->foto ?>">
								<?php } else { ?>
									<div style="width: 200px; height: 150px;"> </div>
								<?php } ?>
								<br>
								<br>
							</td>
						</tr>
						<tr align="center">
							<td>
								<br>
								<input id="text" type="hidden" value="<?php echo $get_empleado->numero_empleado ?>">
								<div id="qrcode"></div>
								<br>
								<span style="font-size: 23px;">NO. DE EMPLEADO: <span class="numero_emp"></span> </span>
								<br>
							</td>
						</tr>

						<tr align="center">
							<td>
								<br>
								<span style="font-size: 20px;">Datos de emergencia:</span>
								<br>
								<span style="font-size: 18px;">Contacto: <?php echo $get_empleado->persona != '' ? $get_empleado->persona : '---';  ?> (<?php echo $get_empleado->parentesco != '' ? $get_empleado->parentesco : '---'; ?>) </span>
								<br>
								<span style="font-size: 18px;">Telefono: <?php echo $get_empleado->tel_emergencia != '' ? $get_empleado->tel_emergencia : '---';  ?> </span>
								<br>
								<span style="font-size: 18px;">Tipo de sangre: <?php echo $get_empleado->sangre != '' ? $get_empleado->sangre : '---';  ?> </span>
								<br>
								<br>
							</td>
						</tr>
					</tbody>
				</table>
			</td>

			<td width="45%" class="margen_todo ">
				<table width="100%">
					<tbody>
						<tr align="center">
							<td>
								<br>
								<?php
								$html = '';
								if ($get_empleado->foto) { ?>
									<img id="img_avatar" style="width: 300px; height: 300px; border-radius: 150px;" src="<?php echo base_url() ?>uploads/personal/<?php echo $get_empleado->foto ?>">
								<?php } else { ?>
									<img src="<?php echo base_url() ?>images/avatarh.PNG">
								<?php  } ?>
								<br>
								<br>
							</td>
						</tr>
						<tr align="center">
							<td>
								<div style="display:inline-block;text-align:center;margin-top:0.5cm;padding:0.1cm;width:9cm;border-bottom:5px solid <?php echo $get_empleado->color ?>;"></div>
								<table>
									<thead>
										<tr align="center">
											<td style="font-size: 38px;"><?php echo $get_empleado->nombre . ' ' . $get_empleado->appaterno . ' '	. $get_empleado->apmaterno ?></td>
										</tr>
										<tr align="center">
											<td style="font-size: 25px;"><?php echo $get_empleado->operativo != '' ? $get_empleado->operativo : '---'; ?> </td>
										</tr>
									</thead>
								</table>
								<div style="width: 9cm; border-bottom: 5px solid <?php echo $get_empleado->color ?>;"></div>
							</td>
						</tr>

						<tr align="center">
							<td>
								<br>
								<span style="font-size: 18px;">Empresa: <?php echo $get_empresa->cliente != '' ? $get_empresa->cliente : '---'; ?> </span>
								<br>
								<span style="font-size: 18px;">Ruta: <?php echo $get_empresa->ruta != '' ? $get_empresa->ruta : '---'; ?> </span>
								<br>
								<br>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
<?php /*
<table class="margen_todo">

ALTER TABLE `personal` ADD `foto` VARCHAR(255) NOT NULL AFTER `usuario`;
ALTER TABLE `personal` ADD `color` VARCHAR(20) NOT NULL AFTER `foto`;
	<tbody>
		<tr align="center">
			<td><img style="width: 400px" src="<?php echo base_url() ?>images/gc.png"></td>
		</tr>
		<tr style="font-size:10%;">
            <td style="border-bottom: 3px solid #026934;">
            </td>
        </tr>
		<tr align="center">
			<td>
			    <br> 
				    <img src="<?php echo base_url() ?>images/avatarh.PNG">
			    </td>
		</tr>
		<tr align="center">
			<td><h1><?php echo $get_empleado->nombre ?> </h1></td>
		</tr>
		<tr align="center">
			<td><h1><?php echo $get_empleado->puesto ?> </h1></td>
		</tr>
		<tr align="center">
			<td>
				<input id="text" type="hidden" value="<?php echo $get_empleado->personalId ?>">
				<div id="qrcode"></div>
				<br>
				<br>
			</td>
		</tr>
		<tr align="center">
			<td><h3>NO. DE EMPLEADO: <span class="numero_emp"></span> </h3></td>
		</tr>
	</tbody>
</table>
<br>
<table class="margen_todo">
	<tbody>
		<tr align="center">
			<td><img style="width: 400px" src="<?php echo base_url() ?>images/gc.png"></td>
		</tr>
		<tr style="font-size:10%;">
            <td style="border-bottom: 3px solid #026934;">
            </td>
        </tr>
		<tr align="center">
			<td>
				<br>
				<br>
				<br>
				<div style="display:inline-block;text-align:center;margin-top:0.5cm;padding:0.1cm;width:9cm;border-bottom:1px solid black;"></div>
				<h2>Firma del empleado</h2>
			</td>
		</tr>
		<tr align="center">
			<td>
				<br>
				<br>
				<br>
				<div style="display:inline-block;text-align:center;margin-top:0.5cm;padding:0.1cm;width:9cm;border-bottom:1px solid black;"></div>
				<h2>Firma del jefe inmediato</h2>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
			</td>
		</tr>
	</tbody>
</table>
*/
?>

<script src="<?php echo base_url(); ?>assets/node_modules/jquery/jquery-3.2.1.min.js"></script>
<script src="<?php echo base_url(); ?>public/core/js/qrcode.js"></script>
<script src="<?php echo base_url(); ?>public/js/qr.js"></script>

<script type="text/javascript">
	function pad(str, max) {
		str = str.toString();
		return str.length < max ? pad("0" + str, max) : str;
	}
</script>

<script type="text/javascript">
	$(document).ready(function() {
		var num_emp = $('#text').val();
		$('.numero_emp').html(pad(num_emp, 4));

		window.print();
		
	});
</script>