<input type="hidden" id="titulo1_aux" value="<?php echo $gafete->titulo1 ?>">
<input type="hidden" id="titulo2_aux" value="<?php echo $gafete->titulo2  ?>">
<style type="text/css">
    .krajee-default.file-preview-frame .kv-file-content {
        width: 213px;
        height: 80px !important;
    }
    .file-drop-zone {
        border: 1px dashed #aaa;
        min-height: 215px !important;
    }
</style>
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12"><br>
                <div class="card">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                       <h3>Gafete</h3> 
                                       <hr>
                                    </div>
                                    <div class="col-md-3">
                                        <h4>Logo de la pelicula</h4>
                                        <input id="titulo1" name="titulo1" type="file">
                                        <div><br>
                                            <button type="button" class="btn btn-info waves-effect btn_registro" onclick="guardar_titulo1()">Guardar</button>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <h4>Logo de la producción</h4>
                                        <input id="titulo2" name="titulo2" type="file">
                                        <div><br>
                                            <button type="button" class="btn btn-info waves-effect btn_registro" onclick="guardar_titulo2()">Guardar</button>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <h4>Información referente</h4>
                                            <textarea name="titulo3" id="titulo3" class="form-control" rows="6"><?php echo $gafete->titulo3  ?></textarea>
                                        </div>
                                        <div align="right">
                                            <button type="button" class="btn btn-info waves-effect btn_registro" onclick="guardar_titulo3()">Guardar</button>
                                        </div>
                                    </div>

                                </div>    
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>    