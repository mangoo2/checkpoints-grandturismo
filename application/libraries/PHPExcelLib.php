<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Incluir el archivo PHPExcel principal.
require_once(APPPATH . 'third_party/PHPExcel/PHPExcel.php');

class PHPExcelLib extends PHPExcel {
    public function __construct() {
        parent::__construct();  // Llama al constructor de la clase base
    }
}

