<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12"><br>
                <div class="card">
                    <div class="row">

                        <div class="col-lg-12">
                            <div class="card-body">

                                <div class="row my-4">
                                    <div class="col-md-12" align="left">

                                        <div class="form-group">
                                            <label>Ruta actual:</label>

                                                <select class="form-control" id="rutaId" onchange="selectRuta(0)">
                                                    <option disabled selected value="0">Selecciona una opción</option>
                                                    <?php foreach ($rutas as $r) {
                                                        echo '<option value="'.$r->id.'">'.$r->ruta.'</option>';
                                                    } ?>
                                                </select>
                                            
                                        </div>

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12" align="center">
                                        <h1>Escaneo QR</h1>
                                    </div>
                                </div>

                                <div class="row my-5">
                                    <div class="col-md-6"  align="center">
                                        <button type="button" class="btn btn-lg btn-info p-5 mt-4" onclick="escaneo(1)"> Realizar escaneo <br><i class="fas fa-caret-up"></i> Subida</button>
                                    </div>

                                    <div class="col-md-6"  align="center">
                                        <button type="button" class="btn btn-lg btn-danger p-5 mt-4" onclick="escaneo(2)"> Realizar escaneo <br><i class="fas fa-caret-down"></i> Bajada</button>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="confirmacion_ruta_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea cambiar de ruta?</h5>
            </div>
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="change_ruta()">Aceptar</button>
            </div>
        </div>   
    </div>
</div>



<div id="select_ruta_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Selecciona la ruta actual</h4>
                </div>
                <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
            </div>
            <div class="modal-body">
                <!--<h5>Selecciona la ruta actual:</h5>-->
                <div class="form-group">
                    <select class="form-control" id="rutaIdM" onchange="change_ruta(1)">
                        <option disabled selected value="0">Selecciona una opción</option>
                        <?php foreach ($rutas as $r) {
                            echo '<option value="'.$r->id.'">'.$r->ruta.'</option>';
                        } ?>
                    </select>                    
                </div>
            </div>
        </div>   
    </div>
</div>  