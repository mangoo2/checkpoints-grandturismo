<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Rutas extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->idpersonal = $this->session->userdata('idpersonal');
        $this->administrador = $this->session->userdata('administrador');
        $this->perfilid = $this->session->userdata('perfilid');
        date_default_timezone_set('America/Mexico_City');
        $this->fecha_hora_actual = date('Y-m-d G:i:s');
        $this->fechainicio = date('Y-m-d');
        $this->inicioactual = date('G:i:s');
        if ($this->session->userdata('logeado')) {
            $this->idpersonal = $this->session->userdata('idpersonal');
            $this->perfilid = $this->session->userdata('perfilid');
            $permiso = $this->Login_model->getviewpermiso($this->perfilid, 1); // perfil y id del submenu
            if ($permiso == 0) {
                redirect('Login');
            }
        } else {
            redirect('/Login');
        }
    }

    public function index()
    {
        $data['perfil'] = $this->perfilid;
        $data['clientes'] = $this->General_model->get_records_condition('estatus = 1', 'clientes');
        $data['unidades'] = $this->General_model->get_records_condition('activo = 1', 'unidades');
        $data['operadores'] = $this->General_model->get_records_condition('estatus = 1', 'operadores');

        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('rutas/index', $data);
        $this->load->view('templates/footer');
        $this->load->view('rutas/index_js');
    }

    public function registra_ruta()
    {
        $data = $this->input->post();
        //log_message('error', 'DATA: ' . json_encode($data));
        $ID = $data['rutaId'];
        unset($data['rutaId']);
        $reg_id = 0;

        if ($ID == 0) {
            $data['reg'] = $this->fecha_hora_actual;
            $reg_id = $this->General_model->add_record('rutas', $data);
        } else {
            $reg_id = $this->General_model->edit_record('id', $ID, $data, 'rutas');
            $reg_id = $ID;
        }
        echo $reg_id;
    }

    public function getlistado()
    {
        $params = $this->input->post();
        $getdata = $this->ModelCatalogos->get_rutas($params);
        $totaldata = $this->ModelCatalogos->total_rutas($params);

        $json_data = array(
            "draw"            => intval($params['draw']),
            "recordsTotal"    => intval($totaldata),
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           => $this->db->last_query()
        );
        echo json_encode($json_data);
    }

    public function delete_ruta()
    {
        $id = $this->input->post('id');
        $data = array('estatus' => 0);
        $this->General_model->edit_record('id', $id, $data, 'rutas');
    }

    function registros_delete()
    {
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);

        //log_message('error', 'DATA REGISTROS: ' . json_encode($DATA));

        for ($i = 0; $i < count($DATA); $i++) {
            $rutaId = $DATA[$i]->rutaId;
            $array = array('estatus' => 0);
            $this->General_model->edit_record('id', $rutaId, $array, 'rutas');
        }
    }

    public function delete_registros()
    {
        $array = array('estatus' => 0);
        $this->General_model->edit_record_all($array, 'rutas');
    }

    public function save_checks(){
        $id = $this->input->post('id');
        $lat = $this->input->post('lat');
        $long = $this->input->post('long');
        $fecha = $this->input->post('fecha');

        $cont_check=0; $cont_checkb=0; $cont_emp=0;
        $getc=$this->General_model->getChecksRuta($id,$fecha,1); //verifica si existen registros de subida de fecha indicada
        foreach ($getc as $c) {
            $cont_check++;
        }

        $getc=$this->General_model->getChecksRuta($id,$fecha,2); //verifica si existen registros de bajada de fecha indicada
        foreach ($getc as $c) {
            $cont_checkb++;
        }
        if($cont_check>0){
            $getr=$this->General_model->getRutaCliCheck($id,$fecha);
            foreach ($getr as $r) {
                $data['personalId'] = $r->personalId;
                $data['idcliente'] = $r->idcliente;
                $data['idruta'] = $id;
                $data['idpoints_check'] = 2;
                $data['idempleado'] = $this->idpersonal;
                $data['latitud'] = $lat;
                $data['longitud'] = $long;
                $data['reg'] = date("Y-m-d H:i:s", strtotime($fecha . $this->inicioactual));//agregar hora
                

                $cont_emp++;
                $this->General_model->add_record('check_points_detalles', $data);
            }
        }
        echo  json_encode(array("cont_emp"=>$cont_emp,"cont_check"=>$cont_check,"cont_checkb"=>$cont_checkb));
    }
}
