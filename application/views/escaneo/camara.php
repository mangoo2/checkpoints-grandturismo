<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>css/tracking.css?v=<?php echo date('YmdGis') ?>">
<style type="text/css">
    .qrsacaner {
        background: url(<?php echo base_url() ?>images/qr2.PNG);
        background-repeat: no-repeat;
        background-position: center;
    }
</style>
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row" id="outdiv">
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div align="center">
                        <br>
                        <h1>Escaneo QR</h1>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-body">
                                <div class="row" id="mainbody">
                                    <div class="col-md-12 col-sm-12 col-xs-12" style="display: none;">
                                        <button class="btn btn-primary" type="button" onclick="normal()">Normal</button>
                                        <button class="btn btn-primary" type="button" onclick="frontal()">Frontal</button>
                                    </div>
                                    <div class="col-md-12" style="display: none;">
                                        <img class="selector" id="qrimg" src="cam.png" onclick="setimg()" align="right" />
                                        <img class="selector" id="webcamimg" src="vid.png" onclick="setwebcam()" align="left" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 qrsacaner" onclick="load()">
                                    </div>
                                    <div class="col-md-12" style="font-size: 200%; text-align: center;" id="result">

                                    </div>

                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Codigo de empleado:</label>
                                                    <input type="text" id="barcode" class="form-control" style="border: 1px solid #b4b8bb;min-height: 50px;">
                                                    <input type="hidden" id="ponit" value="<?php echo $ponit ?>">
                                                </div>
                                            </div>

                                            <div class="col-md-4" hidden>
                                                <div class="form-group">
                                                    <label>Latitud:</label>
                                                    <input type="number" id="latitude" class="form-control" style="border: 1px solid #b4b8bb;min-height: 50px;">
                                                </div>
                                            </div>

                                            <div class="col-md-4" hidden>
                                                <div class="form-group">
                                                    <label>Longitud:</label>
                                                    <input type="number" id="longitude" class="form-control" style="border: 1px solid #b4b8bb;min-height: 50px;">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row mt-4">
                                            <div class="col-md-12 my-2">
                                                <div class="form-group" align="center">
                                                    <button type="button" class="btn waves-effect waves-light btn-rounded btn_sistema" onclick="validarcodigo()">Registrar</button>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group" align="center">
                                                    <button type="button" class="btn waves-effect waves-light btn-rounded btn-secondary" onclick="regresar_vista()"><i class="fas fa-arrow-left"></i> Regresar</button>
                                                    <button type="button" class="btn waves-effect waves-light btn-rounded btn-secondary" onclick="actulizar_vista(<?php echo $ponit ?>)"><i class="fas fa-redo"></i></button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="row">
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<canvas id="qr-canvas" width="800" height="600" style="display: none"></canvas>