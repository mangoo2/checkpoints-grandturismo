<style type="text/css">
    .kv-file-upload,
    .fileinput-remove-button {
        display: none;
    }
</style>

<input type="hidden" id="perfil" value="<?php echo $perfil ?>">
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12"><br>
                <div class="card">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <?php if ($perfil != 2) { ?>
                                            <button type="button" class="btn btn-outline-info mt-2" onclick="modal_empleado()"><i class="fas fa-user-plus"></i> Nuevo Empleado</button>
                                            <a type="button" title="Plantilla para carga de empleados" class="btn btn-outline-info mt-2" href="<?php echo base_url(); ?>Empleados/plantillaExcel"><i class="fas fa-file-excel"></i> Exportar Plantilla</a>
                                            <button type="button" title="Plantilla para carga de empleados" class="btn btn-outline-info mt-2" onclick="cargar_malla()"><i class="fa fa-upload"></i> Cargar Plantilla</button>
                                        <?php } ?>
                                        <!--
                                        <button type="button" class="btn btn-outline-info" disabled><i class="fas fa-paper-plane"></i> Enviar cita y formulario</button> 
                                        <button type="button" class="btn btn-outline-info" disabled><i class="far fa-heart"></i> Recomentar</button>
                                    -->
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group bootstrap-touchspin">
                                            <span class="input-group-text mt-2">Buscar Empleado &nbsp;&nbsp; <i class="fas fa-search"></i></span></span><input id="empleado_busqueda" type="text" class="form-control" placeholder="Escriba nombre del empleado" oninput="reload_registro()">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-2 mt-2">
                                        <div class="custom-control custom-checkbox mr-sm-2">
                                            <input type="checkbox" class="custom-control-input" id="check_todo" onclick="btn_check_todo()">
                                            <label class="custom-control-label" for="check_todo">Seleccionar todo</label>
                                        </div>
                                    </div>
                                    <div class="col-md-2 mt-2">
                                        <button type="button" class="btn waves-effect waves-light btn-sm btn-danger" onclick="btn_eliminar_registros()"><i class="fas fa-trash-alt"></i> Eliminar Selección</button>
                                    </div>
                                    <div class="col-md-2 mt-2">
                                        <button type="button" class="btn waves-effect waves-light btn-sm btn-info" onclick="btn_eliminar_registros_all()"><i class="fas fa-trash-alt"></i> Eliminar Todos</button>
                                    </div>

                                    <div class="col-md-3"></div>

                                    <div class="col-md-3">
                                        <?php if ($perfil == 1) { ?>
                                            <div class="form-group">
                                                <label>Seleccionar Cliente</label>
                                                <input type="hidden" id="idCli" value="<?php echo $cliente ?>">
                                                <select id="clientes" class="form-control" onchange="checkSelect()">
                                                    <option value="0" selected="">Todos</option>
                                                    <?php $result=$this->General_model->getselectwhere('clientes','estatus',1);
                                                    foreach ($result as $item){
                                                        echo '<option value="'.$item->clienteId.'">'.$item->nombre.' '.$item->appaterno.' '.$item->apmaterno.'</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        <?php } ?>
                                    </div>

                                    <div class="col-md-12">
                                        <table class="table" id="table_datos">
                                            <thead class="bg-blue">
                                                <tr>
                                                    <th></th>
                                                    <th>ID</th>
                                                    <th>Nombre del empleado</th>
                                                    <th>Operativa</th>
                                                    <th>Cliente</th>
                                                    <th></th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="registro_empleado" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header modal_sistema">
                <div class="col-md-1"></div>
                <div class="col-md-10" align="center">
                    <h4 class="modal-title">Registro de empleado</h4>
                </div>
                <div class="col-md-1">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" id="foto_validar" value="0">
                                <form class="form" method="post" role="form" id="form_empleado">
                                    <input type="hidden" name="personalId" id="personalId" value="0">

                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="foto_empl">
                                                <img id="img_avatar" style="width: 145px; height: 145px; border-radius: 70px;" src="<?php echo base_url(); ?>images/annon.png">
                                            </div>
                                            <span class="foto_avatar">
                                                <input type="file" id="foto_avatar" style="display: none" accept=".jpg, .jpeg, .png">
                                            </span>
                                            <label for="foto_avatar">
                                                <span class="btn waves-effect waves-light btn-secondary"><i class="fas fa-camera"></i> Cambiar foto</span>
                                            </label>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label><span style="color: red;">*</span> Nombre</label>
                                                        <input type="text" name="nombre" id="nombre" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Apellido Paterno</label>
                                                        <input type="text" name="appaterno" id="appaterno" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Apellido Materno</label>
                                                        <input type="text" name="apmaterno" id="apmaterno" class="form-control">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Operativa</label>
                                                        <input type="text" name="operativo" id="operativo" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <?php if ($perfil == 1) { ?>
                                                        <div class="form-group">
                                                            <label>Cliente</label>
                                                            <select class="form-control" name="cliente" id="cliente">
                                                                <option value="0">Ninguno</option>
                                                                <?php foreach ($clientes as $c) {
                                                                    echo '<option value="' . $c->clienteId . '">' . $c->nombre . ' ' . $c->appaterno . '</option>';
                                                                } ?>
                                                            </select>
                                                            <!--<input type="text" name="empresa" id="empresa" class="form-control">-->
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label class="form-label" for="especial">Empleado especial</label>
                                                        <span class="switch switch-icon">
                                                            <label>
                                                                <input type="checkbox" name="especial" id="especial">
                                                        <span></span>
                                                        </label>
                                                    </span>
                                                </div>
                                            </div>

                                            <hr>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h6>Datos de emergencia</h6>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Teléfono de emergencia</label>
                                                        <input type="text" name="tel_emergencia" id="tel_emergencia" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Persona (contacto)</label>
                                                        <input type="text" name="persona" id="persona" class="form-control">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Parentesco</label>
                                                        <input type="text" name="parentesco" id="parentesco" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Tipo de sangre</label>
                                                        <input type="text" name="sangre" id="sangre" class="form-control">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="custom-control custom-checkbox m-b-0">
                                                    <input type="checkbox" class="custom-control-input" name="check_baja" id="verificar_check" onclick="check_baja_btn()">
                                                    <span class="custom-control-label" style="color: red">Dar de baja al empleado</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="baja_texto" style="display: none">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Fecha de baja</label>
                                                    <input type="date" name="fechabaja" id="fechabaja" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label>Motivo</label>
                                                    <textarea name="motivo" id="motivo" class="form-control js-auto-size"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info waves-effect btn_registro" onclick="guarda_empleado()">Guardar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div id="elimina_empleado_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea borrar éste empleado? Esta operación no se puede deshacer.</h5>
            </div>
            <input type="hidden" id="id_empleado">
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="delete_empleado()">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div id="usuario_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Usuario</h4>
                    <input type="hidden" id="idEmp">
                    <input type="hidden" id="tipoEmp" value="1">
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="text_usuario"></div>
            </div>
            <input type="hidden" id="id_empleado">
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" id="btn_modal_usu" class="btn btn_sistema waves-effect btn_registro" onclick="guardar_usuario()">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div id="elimina_registro_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea borrar estos registros? Esta operación no se puede deshacer.</h5>
            </div>
            <input type="hidden" id="id_empleado">
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="delete_registros()">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div id="elimina_masivo_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea borrar todos los registros? Esta operación no se puede deshacer.</h5>
            </div>
            <input type="hidden" id="id_empleado">
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <?php if ($perfil == 3) { ?>
                    <button type="button" class="btn btn-danger waves-effect" onclick="delete_registros_all_particular()">Aceptar</button>
                <?php } else { ?>
                    <button type="button" class="btn btn-danger waves-effect" onclick="delete_registros_all()">Aceptar</button>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<div id="carga_malla_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Cargar malla de datos</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row carga_malla">
                    <div class="col-md-12">
                        <form id="form_carga_malla" method="POST" enctype="multipart/form-data">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Seleccione un cvs</label>
                                    <input type="file" name="inputFile" id="inputFile" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12 mt-4">
                                <div class="form-group">
                                    <button type="button" class="btn btn-outline-info" onclick="aceptar_malla()"><i class="fa fa-upload"></i> Cargar Malla</button>
                                    <!--<button name="cancelar" id="cancelar" class="btn bg-red" style="width:80px;">Cancelar</button>-->
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <!--<button type="button" class="btn btn-danger waves-effect" onclick="delete_registros_all()">Aceptar</button>-->
            </div>
        </div>
    </div>
</div>



<div class="iframe_datos" style="display: none;">
</div>