<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class ModelCatalogos extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }


    function get_empleados($params){
        $columns = array( 
            0=>'p.personalId',
            1=>'p.personalId',
            2=>'p.nombre',
            3=>'p.operativo',
            4=>'c.nombre as NomCliente',
            5=>'p.puesto',
            6=>'p.correo',
            7=>'p.celular',
            8=>'p.fechabaja',
            9=>'p.motivo',
            10=>'p.check_baja',
            11=>'p.usuario',
            12=>'p.foto',
            13=>'p.color',
            14=>'p.numero_empleado',
            15=>'p.appaterno',
            16=>'p.apmaterno',
            17=>'p.cliente',
            18=>'p.tel_emergencia',
            19=>'p.persona',
            20=>'p.parentesco',
            21=>'p.sangre',
            22=>'c.appaterno as AppCliente',
            23=>'c.apmaterno as ApmCliente',
            24=>'p.especial'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }

        $columns2 = array( 
            0=>'p.personalId',
            1=>'p.personalId',
            2=>'p.nombre',
            3=>'p.operativo',
            4=>'c.nombre',
            5=>'p.puesto',
            6=>'p.correo',
            7=>'p.celular',
            8=>'p.fechabaja',
            9=>'p.motivo',
            10=>'p.check_baja',
            11=>'p.usuario',
            12=>'p.foto',
            13=>'p.color',
            14=>'p.numero_empleado',
            15=>'p.appaterno',
            16=>'p.apmaterno',
            17=>'p.cliente',
            18=>'p.tel_emergencia',
            19=>'p.persona',
            20=>'p.parentesco',
            21=>'p.sangre',
            22=>'c.appaterno',
            23=>'c.apmaterno',
            24=>'p.especial'
        );

        $this->db->select($select);
        $this->db->from('personal as p');
        $this->db->join('clientes c', 'p.cliente = c.clienteId AND c.estatus = 1', 'left');

        if($params['particular'] != '0'){
            $this->db->where('p.cliente = '.$params['particular']);
        }

        if($params['selectCliente'] != '0'){
            $this->db->where('p.cliente = '.$params['selectCliente']);
        }

        if($params['empleado']!=''){
            $this->db->group_start();
            $this->db->like('p.nombre',$params['empleado']);
            $this->db->or_like('p.appaterno',$params['empleado']);
            $this->db->or_like('p.apmaterno',$params['empleado']);
            $this->db->or_like('p.operativo',$params['empleado']);
            $this->db->group_end();
        }

        $where = array('p.estatus'=>1, 'p.tipo_empleado'=>0);
        $this->db->where($where);

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_empleados($params){
        $columns = array( 
            0=>'p.personalId',
            1=>'p.personalId',
            2=>'p.nombre',
            3=>'p.operativo',
            4=>'c.nombre',
            5=>'p.puesto',
            6=>'p.correo',
            7=>'p.celular',
            8=>'p.fechabaja',
            9=>'p.motivo',
            10=>'p.check_baja',
            11=>'p.usuario',
            12=>'p.foto',
            13=>'p.color',
            14=>'p.numero_empleado',
            15=>'p.appaterno',
            16=>'p.apmaterno',
            17=>'p.cliente',
            18=>'p.tel_emergencia',
            19=>'p.persona',
            20=>'p.parentesco',
            21=>'p.sangre',
            22=>'c.appaterno',
            23=>'c.apmaterno',
            24=>'p.especial'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('personal as p');
        $this->db->join('clientes c', 'p.cliente = c.clienteId AND c.estatus = 1', 'left');
        

        if($params['particular'] != '0'){
            $this->db->where('p.cliente = '.$params['particular']);
        }

        if($params['selectCliente'] != '0'){
            $this->db->where('p.cliente = '.$params['selectCliente']);
        }

        if($params['empleado']!=''){
            $this->db->group_start();
            $this->db->like('p.nombre',$params['empleado']);
            $this->db->or_like('p.appaterno',$params['empleado']);
            $this->db->or_like('p.apmaterno',$params['empleado']);
            $this->db->or_like('p.operativo',$params['empleado']);
            $this->db->group_end();
        }

        $where = array('p.estatus'=>1, 'p.tipo_empleado'=>0);
        $this->db->where($where);

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    ///===============!!
    function get_reporte($params){
        $columns = array( 
            0=>'cpd.id',
            1=>'DATE_FORMAT(cpd.reg,  "%d / %m / %Y  %r" ) AS reg',
            2=>'CONCAT (p.nombre," ",p.appaterno," ",p.apmaterno )AS empleado',
            3=>'cp.nombre AS point',
            4=>'r.ruta',
            5=>'c.nombre AS cliente',
            6=>'cpd.longitud',
            7=>'cpd.latitud',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $columns2 = array( 
            0=>'cpd.id',
            1=>'cpd.reg',
            2=>'cpd.id',
            3=>'cpd.latitud',
            4=>'cpd.longitud',
            5=>'p.nombre',
            6=>'r.ruta',
            7=>'c.nombre',
            8=>'cp.nombre',
            9=>'p.appaterno',
            10=>'p.apmaterno',
        );
        $this->db->select($select);
        $this->db->from('check_points_detalles cpd');
        $this->db->join('personal p','p.personalId=cpd.personalId','left');
        $this->db->join('check_points cp','cp.id=cpd.idpoints_check','left');
        $this->db->join('rutas r','r.id = cpd.idruta','left');
        $this->db->join('clientes c','c.clienteId = cpd.idcliente','left');
        if($params['fecha_inicio']!='' && $params['fecha_fin']!=''){
            $this->db->where('cpd.reg BETWEEN "'.$params['fecha_inicio'].' 00:00:00" AND "'.$params['fecha_fin'].' 23:59:59"');
        }
        if($params['points_check']!=0){
            $this->db->where('cpd.idpoints_check='.$params['points_check']);   
        }

        if($params['clientes']!=0){
            $this->db->where('cpd.idcliente='.$params['clientes']);   
        }
        if($params['rutas']!=0){
            $this->db->where('cpd.idruta='.$params['rutas']);   
        }
        if($params['operadores']!=0){
            $this->db->where('r.choferId='.$params['operadores']);   
        }

        if($params['buscar']!=''){
            $this->db->like('p.nombre',$params['buscar']);
            $this->db->or_like('cp.nombre',$params['buscar']);
            if($params['fecha_inicio']!='' && $params['fecha_fin']!=''){
                $this->db->where('cpd.reg BETWEEN "'.$params['fecha_inicio'].' 00:00:00" AND "'.$params['fecha_fin'].' 23:59:59"');
            }
            $this->db->or_like('p.puesto',$params['buscar']);
            if($params['fecha_inicio']!='' && $params['fecha_fin']!=''){
                $this->db->where('cpd.reg BETWEEN "'.$params['fecha_inicio'].' 00:00:00" AND "'.$params['fecha_fin'].' 23:59:59"');
            }
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_reporte($params){
        $columns = array( 
            0=>'cpd.id',
            1=>'cpd.reg',
            2=>'cpd.id',
            3=>'cpd.latitud',
            4=>'cpd.longitud',
            5=>'p.nombre',
            6=>'r.ruta',
            7=>'c.nombre',
            8=>'cp.nombre',
            9=>'p.appaterno',
            10=>'p.apmaterno',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('check_points_detalles cpd');
        $this->db->join('personal p','p.personalId=cpd.personalId','left');
        $this->db->join('check_points cp','cp.id=cpd.idpoints_check','left');
        $this->db->join('rutas r','r.id = cpd.idruta','left');
        $this->db->join('clientes c','c.clienteId = cpd.idcliente','left');
        if($params['fecha_inicio']!='' && $params['fecha_fin']!=''){
            $this->db->where('cpd.reg BETWEEN "'.$params['fecha_inicio'].' 00:00:00" AND "'.$params['fecha_fin'].' 23:59:59"');
        }
        if($params['points_check']!=0){
            $this->db->where('cpd.idpoints_check='.$params['points_check']);   
        }

        if($params['clientes']!=0){
            $this->db->where('cpd.idcliente='.$params['clientes']);   
        }
        if($params['rutas']!=0){
            $this->db->where('cpd.idruta='.$params['rutas']);   
        }
        if($params['operadores']!=0){
            $this->db->where('r.choferId='.$params['operadores']);   
        }

        if($params['buscar']!=''){
            $this->db->like('p.nombre',$params['buscar']);
            $this->db->or_like('cp.nombre',$params['buscar']);
            if($params['fecha_inicio']!='' && $params['fecha_fin']!=''){
                $this->db->where('cpd.reg BETWEEN "'.$params['fecha_inicio'].' 00:00:00" AND "'.$params['fecha_fin'].' 23:59:59"');
            }
            $this->db->or_like('p.puesto',$params['buscar']);
            if($params['fecha_inicio']!='' && $params['fecha_fin']!=''){
                $this->db->where('cpd.reg BETWEEN "'.$params['fecha_inicio'].' 00:00:00" AND "'.$params['fecha_fin'].' 23:59:59"');
            }
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
    /////////////////////////////// Guanabana
    //=================================================
    function get_check_points($params){
        $columns = array( 
            0=>'id', 
            1=>'nombre', 
            2=>'codigo' 
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('check_points as ch');

        $where = array('ch.activo'=>1);
        $this->db->where($where);

        if($params['points']!=''){
            $this->db->group_start();
            $this->db->like('nombre',$params['points']); 
            $this->db->group_end();
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    
    public function total_check_points($params){
        $columns = array( 
            0=>'id', 
            1=>'nombre', 
            2=>'codigo'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('check_points as ch');

        $where = array('ch.activo'=>1);
        $this->db->where($where);

        if($params['points']!=''){
            $this->db->group_start();
            $this->db->like('nombre',$params['points']); 
            $this->db->group_end();
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
    //////////////////////// Guanabanas

    function get_unidades($params){
        $columns = array( 
            0=>'u.id',
            1=>'u.unidad',
            2=>'u.marca',
            3=>'u.modelo',
            4=>'u.placas',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('unidades u');
        //$this->db->join('clientes c', 'u.cliente = c.clienteId', 'left');

        $where = array('u.activo'=>1);
        $this->db->where($where);

        if($params['points']!=''){
            $this->db->group_start();
            $this->db->like('u.unidad',$params['points']);
            $this->db->or_like('u.ruta', $params['points']);
            $this->db->group_end();
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    
    public function total_unidades($params){
        $columns = array( 
            0=>'u.id',
            1=>'u.unidad',
            2=>'u.marca',
            3=>'u.modelo',
            4=>'u.placas',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('unidades as u');
        //$this->db->join('clientes c', 'u.cliente = c.clienteId', 'left');

        $where = array('u.activo'=>1);
        $this->db->where($where);

        if($params['points']!=''){
            $this->db->group_start();
            $this->db->like('u.unidad',$params['points']);
            $this->db->or_like('u.ruta', $params['points']);
            $this->db->group_end();
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
    //////////////////////// Guanabanas

    //=================================================
    function get_operadores($params){
        $columns = array(
            0=>'operadorId',
            1=>'nombre',
            2=>'telefono',
            3=>'fecha_ingreso',
            4=>'ap_paterno',
            5=>'ap_materno',
            6=>'direccion',
            7=>'vigencia_examen',
            8=>'vigencia_licencia',
            9=>'tipo_licencia',
            10=>'usuario',
            11=>'especial',
            12=>'tel_emergencia',
            13=>'persona',
            14=>'parentesco',
            15=>'sangre'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('operadores');
        $where = array('estatus'=>1);
        $this->db->where($where);
        if($params['points']!=''){
            $this->db->like('nombre',$params['points']);
            $this->db->or_like('ap_paterno',$params['points']);
            $this->db->or_like('ap_materno',$params['points']);
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }

    public function total_opeardores($params){
        $columns = array( 
            0=>'operadorId',
            1=>'nombre',
            2=>'telefono',
            3=>'fecha_ingreso',
            4=>'ap_paterno',
            5=>'ap_materno',
            6=>'direccion',
            7=>'vigencia_examen',
            8=>'vigencia_licencia',
            9=>'tipo_licencia',
            10=>'usuario',
            11=>'especial',
            12=>'tel_emergencia',
            13=>'persona',
            14=>'parentesco',
            15=>'sangre'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('operadores');
        $where = array('estatus'=>1);
        $this->db->where($where);
        if($params['points']!=''){
            $this->db->like('nombre',$params['points']);
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
    //////////////////////// Guanabanas
/*
    function get_empresas($params){
        $columns = array( 
            0=>'id',
            1=>'nombre',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('empresas');
        $where = array('activo'=>1);
        $this->db->where($where);
        if($params['points']!=''){
            $this->db->like('nombre',$params['points']);
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_empresas($params){
        $columns = array( 
            0=>'id',
            1=>'nombre',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('empresas');
        $where = array('activo'=>1);
        $this->db->where($where);
        if($params['points']!=''){
            $this->db->like('nombre',$params['points']);
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    */

    function get_clientes($params){
        $columns = array( 
            0=>'c.clienteId',
            1=>'c.clienteId',
            2=>'c.nombre',
            3=>'c.razon_social',
            4=>'c.telefono',
            5=>'c.correo',
            6=>'c.appaterno',
            7=>'c.apmaterno',
            8=>'c.rfc',
            9=>'c.direccion',
            10=>'c.usuario',
            11=>'c.foto',
            12=>'c.area',
            13=>'c.color'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('clientes as c');
        $this->db->where('c.estatus = 1');

        if($params['cliente']!=''){
            $this->db->group_start();
            $this->db->like('c.nombre',$params['cliente']);
            $this->db->or_like('c.appaterno',$params['cliente']);
            $this->db->or_like('c.apmaterno',$params['cliente']);
            $this->db->group_end();
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }


    public function total_clientes($params){
        $columns = array( 
            0=>'c.clienteId',
            1=>'c.clienteId',
            2=>'c.nombre',
            3=>'c.razon_social',
            4=>'c.telefono',
            5=>'c.correo',
            6=>'c.appaterno',
            7=>'c.apmaterno',
            8=>'c.rfc',
            9=>'c.direccion',
            10=>'c.usuario',
            11=>'c.foto',
            12=>'c.area',
            13=>'c.color'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('clientes as c');
        $this->db->where('c.estatus = 1');

        if($params['cliente']!=''){
            $this->db->group_start();
            $this->db->like('c.nombre',$params['cliente']);
            $this->db->or_like('c.appaterno',$params['cliente']);
            $this->db->or_like('c.apmaterno',$params['cliente']);
            $this->db->group_end();
        }
        
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }


    function get_rutas($params){
        $columns = array( 
            0=>'r.id',
            1=>'r.ruta',
            2=>'ch.nombre as nomChofer ',
            3=>'u.unidad',
            4=>'cl.nombre as nomCliente',
            5=>'r.choferId',
            6=>'r.unidadId',
            7=>'r.clienteId',
            8=>'ch.ap_paterno as appChofer',
            9=>'ch.ap_materno as apmChofer',
            10=>'cl.appaterno as appCliente',
            11=>'cl.apmaterno as apmCliente',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }

        $columns2 = array( 
            0=>'r.id',
            1=>'r.ruta',
            2=>'ch.nombre',
            3=>'u.unidad',
            4=>'cl.nombre',
            5=>'r.choferId',
            6=>'r.unidadId',
            7=>'r.clienteId',
            8=>'ch.ap_paterno',
            9=>'ch.ap_materno',
            10=>'cl.appaterno',
            11=>'cl.apmaterno',
        );
        
        $this->db->select($select);
        $this->db->from('rutas as r');
        $this->db->join('operadores ch', 'r.choferId = ch.operadorId AND ch.estatus = 1', 'left');
        $this->db->join('unidades u', 'r.unidadId = u.id AND u.activo = 1', 'left');
        $this->db->join('clientes cl', 'r.clienteId = cl.clienteId AND cl.estatus = 1', 'left');
        $this->db->where('r.estatus = 1');

        if($params['ruta']!=''){
            $this->db->group_start();
            $this->db->like('r.ruta',$params['ruta']);
            //$this->db->or_like('c.appaterno',$params['cliente']);
            //$this->db->or_like('c.apmaterno',$params['cliente']);
            $this->db->group_end();
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }


    public function total_rutas($params){
        $columns = array( 
            0=>'r.id',
            1=>'r.ruta',
            2=>'ch.nombre',
            3=>'u.unidad',
            4=>'cl.nombre',
            5=>'r.choferId',
            6=>'r.unidadId',
            7=>'r.clienteId',
            8=>'ch.ap_paterno',
            9=>'ch.ap_materno',
            10=>'cl.appaterno',
            11=>'cl.apmaterno',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('rutas as r');
        $this->db->join('operadores ch', 'r.choferId = ch.operadorId AND ch.estatus = 1', 'left');
        $this->db->join('unidades u', 'r.unidadId = u.id AND u.activo = 1', 'left');
        $this->db->join('clientes cl', 'r.clienteId = cl.clienteId AND cl.estatus = 1', 'left');
        $this->db->where('r.estatus = 1');

        if($params['ruta']!=''){
            $this->db->group_start();
            $this->db->like('r.ruta',$params['ruta']);
            //$this->db->or_like('c.appaterno',$params['cliente']);
            //$this->db->or_like('c.apmaterno',$params['cliente']);
            $this->db->group_end();
        }
        
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }


    function get_rutas_clientes($params){
        $columns = array( 
            0=>'r.ruta',
            1=>'u.unidad',
            2=>'o.nombre',
            3=>'r.id',
            4=>'o.ap_paterno',
            5=>'o.ap_materno',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('rutas as r');
        $this->db->join('operadores o', 'r.choferId = o.operadorId AND o.estatus = 1', 'left');
        $this->db->join('unidades u', 'r.unidadId = u.id AND u.activo = 1', 'left');
        $this->db->where('r.estatus = 1');

        if($params['particular'] != '0'){
            $this->db->where('r.clienteId = '.$params['particular']);
        }

        if($params['ruta']!=''){
            $this->db->group_start();
            $this->db->like('r.ruta',$params['ruta']);
            $this->db->group_end();
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }


    public function total_rutas_clientes($params){
        $columns = array( 
            0=>'r.id',
            1=>'r.ruta',
            2=>'u.unidad',
            3=>'o.nombre',
            4=>'o.ap_paterno',
            5=>'o.ap_materno',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('rutas as r');
        $this->db->join('operadores o', 'r.choferId = o.operadorId AND o.estatus = 1', 'left');
        $this->db->join('unidades u', 'r.unidadId = u.id AND u.activo = 1', 'left');
        $this->db->where('r.estatus = 1');

        if($params['particular'] != '0'){
            $this->db->where('r.clienteId = '.$params['particular']);
        }

        if($params['ruta']!=''){
            $this->db->group_start();
            $this->db->like('r.ruta',$params['ruta']);
            $this->db->group_end();
        }
        
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    public function get_poins_excel($f1,$f2,$p,$c,$r,$o,$b){
        $whare1='';
        $whare2='';
        $whare3='';
        $whare4='';
        $whare5='';
        $whare6='';
        
        if($f1!='' && $f2!=''){
            $whare1=' cpd.reg BETWEEN "'.$f1.' 00:00:00" AND "'.$f2.' 23:59:59" ';  
        }

        if($p!=0){
            $whare2=' AND cpd.idpoints_check='.$p;    
        }

        if($c!=0){
            $whare4=' AND cpd.idcliente='.$c;    
        }

        if($r!=0){
            $whare5=' AND cpd.idruta='.$r;    
        }

        if($o!=0){
            $whare6=' AND r.choferId='.$o;    
        }

        if($b!='null'){
            $whare3=' AND  p.nombre LIKE "%'.$b.'%"
                        OR  cp.nombre LIKE "%'.$b.'%"
                        OR  p.puesto LIKE "%'.$b.'%"';  
        }
        $sql='SELECT cpd.id, DATE_FORMAT(cpd.reg,  "%d / %m / %Y  %r" ) AS reg, p.nombre, cp.nombre as poins, r.ruta, c.nombre AS cliente
              FROM check_points_detalles AS cpd
              LEFT JOIN personal AS p ON p.personalId = cpd.personalId
              LEFT JOIN check_points AS cp ON cp.id = cpd.idpoints_check
              LEFT JOIN rutas AS r ON r.id = cpd.idruta
              LEFT JOIN clientes AS c ON c.clienteId = cpd.idcliente
              WHERE  '.$whare1.$whare2.$whare4.$whare5.$whare6.$whare3.' ORDER BY cpd.id DESC';
        $query = $this->db->query($sql);
        return $query->result();        
    }


    function get_RuCli($params){
        $columns = array(
            0=>'rc.id',
            1=>'rc.tipo',
            2=>'r.ruta',
            3=>'p.nombre',
            4=>'p.appaterno',
            5=>'p.apmaterno',
        );
        
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('rutas_clientes rc');
        $this->db->join('rutas r', 'rc.rutaId = r.id AND r.estatus = 1', 'left');
        $this->db->join('personal p', 'rc.empleadoId = p.personalId AND p.estatus = 1', 'left');
        $this->db->where('rc.rutaId = ' . $params['id']);
        $this->db->where('rc.estatus = 1');

        if($params['particular'] != '0'){
            $this->db->where('r.clienteId = '.$params['particular']);
        }

        if($params['rucli']!=''){
            $this->db->group_start();
            $this->db->like('p.nombre',$params['rucli']);
            $this->db->or_like('p.appaterno',$params['rucli']);
            $this->db->or_like('p.apmaterno',$params['rucli']);
            $this->db->group_end();
        }

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }


    public function total_RuCli($params){
        $columns = array( 
            0=>'rc.id',
            1=>'rc.tipo',
            2=>'r.ruta',
            3=>'p.nombre',
            4=>'p.appaterno',
            5=>'p.apmaterno',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('rutas_clientes rc');
        $this->db->join('rutas r', 'rc.rutaId = r.id AND r.estatus = 1', 'left');
        $this->db->join('personal p', 'rc.empleadoId = p.personalId AND p.estatus = 1', 'left');
        $this->db->where('rc.rutaId = ' . $params['id']);
        $this->db->where('rc.estatus = 1');

        if($params['particular'] != '0'){
            $this->db->where('r.clienteId = '.$params['particular']);
        }

        if($params['rucli']!=''){
            $this->db->group_start();
            $this->db->like('p.nombre',$params['rucli']);
            $this->db->or_like('p.appaterno',$params['rucli']);
            $this->db->or_like('p.apmaterno',$params['rucli']);
            $this->db->group_end();
        }
        
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

}