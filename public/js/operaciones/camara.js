var base_url = $('#base_url').val();
//var latitud, longitud;

$(document).ready(function ($) {
	$("#barcode").keypress(function (e) {
		if (e.which == 13) {
			validarcodigo();
		}
	});
});

var validar_codigo_clic = 0;

function validarcodigo() {
	if (navigator.geolocation) {
		var success = function (position) {
			$("#latitude").val(position.coords.latitude);
			$("#longitude").val(position.coords.longitude);

			setTimeout(function () {
				saveCheckPoint();
			}, 1000);
		}
		navigator.geolocation.getCurrentPosition(success, function (msg) {
			//console.error(msg);
			swal("Error", "Geolocalización no activa.", "error");
		});
	} else {
		swal("Error", "Geolocalización no activa.", "error");
	}
}

function saveCheckPoint() {
	var barcode = $("#barcode").val();
	var point = $('#ponit').val();
	var latitud = $("#latitude").val();
	var longitud = $("#longitude").val();
	var checkSB = 0;

	if (latitud == '' || longitud == '') {
		//return;
	}

	//$('.btn_sistema').prop('disabled', true);

	$.ajax({
		type: 'POST',
		url: base_url + 'Operacion/checkSubidasBajadas',
		data: {
			codigo: barcode,
		},
		async: false,
		statusCode: {
			404: function (data) {
				swal("Error", "No se encuentra el archivo.", "error");
			},
			500: function () {
				swal("Error", "500.", "error");
			}
		},
		success: function (data) {
			checkSB = data;
		},
		complete: function () {

			if (checkSB == 1 && point == 1) {
				console.log("ERROR!!!, Existe subida y quiero agregar subida.");
				swal("Error", "Subida registrada anteriormente.", "error");
				//$('.btn_sistema').prop('disabled', false);
				return;

			} else if (checkSB == 0 && point == 2) {
				console.log("ERROR!!!, NO existe subida y quiero agregar bajada.");
				swal("Error", "No existe subida registrada anteriormente.", "error");
				//$('.btn_sistema').prop('disabled', false);
				return;

			} else {
				console.log("Agrego porque no existe. 1");
			}

			setTimeout(function () {
				$('.btn_sistema').prop('disabled', false);
			}, 3000);

			$.ajax({
				type: 'POST',
				url: base_url + 'Operacion/validarcodigo',
				data: {
					codigo: barcode,
					point: point,
					clic: validar_codigo_clic,
					latitud: latitud,
					longitud: longitud
				},
				async: false,
				statusCode: {
					404: function (data) {
						swal("Error", "No se encuentra el archivo.", "error");
					},
					500: function () {
						swal("Error", "500.", "error");
					}
				},
				success: function (data) {
					//console.log("QR: " + data);
					var idcodigo = parseInt(data);
					validar_codigo_clic++;

					if (idcodigo == 0) {
						swal("Error", "El empleado no existe.", "error");
						validar_codigo_clic = 0;
						$("#barcode").val("");
					} else if (idcodigo == '-1') {
						swal("Error", "El empleado no tiene ruta asignada.", "error");
						validar_codigo_clic = 0;
						$("#barcode").val("");
					} else if (idcodigo == '-2') {
						swal("Error", "El empleado no tiene esta ruta asignada.", "error");
						validar_codigo_clic = 0;
						$("#barcode").val("");
					} else if (idcodigo == '-3') {
						swal("Error", "El operador no puede llevar empleado(s) de otra ruta", "error");
						validar_codigo_clic = 0;
						$("#barcode").val("");
					} else {
						window.location.href = base_url + 'Operacion/empleado/' + idcodigo + '/' + point;
					}

					//$("button").prop("disabled", false);
				}
			});
		}
	});

}

function actulizar_vista(id) {
	window.location.href = base_url + 'Operacion/camara/' + id;
}

function regresar_vista() {
	window.location.href = base_url + 'Operacion';
}
/*
function get_location() {
	if (navigator.geolocation) {
		var success = function (position) {
			latitud = position.coords.latitude;
			longitud = position.coords.longitude;
		}

		navigator.geolocation.getCurrentPosition(success, function (msg) {
			console.error(msg);
			console.error("TRUE");
			swal("Error", "Geolocalización no activa.", "error");
		});
	} else {
		console.error("Geolocalización no soportada");
		swal("Error", "Geolocalización no activa.", "error");
	}
}
*/
function get_latitud() {
	var lat = 0;
	if (navigator.geolocation) {
		var success = function (position) {
			lat = position.coords.latitude;
		}
		navigator.geolocation.getCurrentPosition(success, function (msg) {
			//console.log("lat: " + lat);
		});
	}
	//console.log("lat: " + lat);
	return lat;
}

function get_longitud() {
	var lon = 0;
	if (navigator.geolocation) {
		var success = function (position) {
			lon = position.coords.longitude;
		}
		navigator.geolocation.getCurrentPosition(success, function (msg) {
			//console.log("lon: " + lon);
		});
	}
	//console.log("lon: " + lon);
	return lon;
}