var base_url = $('#base_url').val();

$(document).ready(function($) {
	getRutaID();

	//reloadEscaneo();

});

function getRutaID(){
	$.ajax({
		type: 'POST',
		url: base_url + 'Operacion/get_ruta_session',
		data: {},
		async: false,
		statusCode: {
			404: function (data) {
				swal("Error", "No se encuentra el archivo.", "error");
			},
			500: function () {
				swal("Error", "500.", "error");
			}
		},
		success: function (data) {
			console.log("DATA: " + data);
			
			if(data > 0){
				$('#rutaId').val(data);
			}else{
				//$("#rutaId").val(data);
				$('#select_ruta_modal').modal();
			}
			
		}
	});
}

function escaneo(option = 0){
	if(option > 0){
		if(option == 1){
			window.location = base_url+'Operacion/camara/'+option;	
		}else if(option == 2){
			window.location = base_url+'Operacion/camara/'+option;
		}
	}else{
		var point=$('#points_check option:selected').val();
		window.location = base_url+'Operacion/camara/'+point;
	}
}

function escaneo_prinicipal(){
	window.location = base_url+'Operacion';
}

function escaneo_camara(point){
	window.location = base_url+'Operacion/camara/'+point;
}


function selectRuta(){
  $('#confirmacion_ruta_modal').modal();
}

function change_ruta(type = 0) {

	if(type == 1){
		$IDRuta = $("#rutaIdM").val();
	}else{
		$IDRuta = $("#rutaId").val();
	}
	

	$.ajax({
		type: 'POST',
		url: base_url + 'Operacion/set_ruta_session',
		data: {
			IDruta: $IDRuta
		},
		async: false,
		statusCode: {
			404: function (data) {
				swal("Error", "No se encuentra el archivo.", "error");
			},
			500: function () {
				swal("Error", "500.", "error");
			}
		},
		success: function (data) {
			console.log("DATA" + data);
			$('#confirmacion_ruta_modal').modal('hide');

			$.toast({
				heading: 'Éxito',
				text: 'La vista se recargara',
				position: 'top-right',
				loaderBg: '#ff6849',
				icon: 'success',
				hideAfter: 1500,
				stack: 6
			});
			$('#elimina_cliente_modal').modal('hide');
			setTimeout(function () {
				location.reload();
			}, 1000);

		}
	});

	//$('#confirmacion_ruta_modal').modal('hide');
}

function reloadEscaneo(){
	
	setTimeout(function () {
		escaneo_camara( $("#ponitR").val() );
	}, 3000);
	
}