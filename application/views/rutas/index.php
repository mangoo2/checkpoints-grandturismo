<input type="hidden" id="perfil" value="<?php echo $perfil ?>">
<div class="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12"><br>
                <div class="card">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6 mb-2">
                                        <?php if ($perfil == 1) { ?>
                                            <button type="button" class="btn btn-outline-info" onclick="modal_ruta()"><i class="fas fa-user-plus"></i> Nueva Ruta</button>
                                        <?php } ?>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group bootstrap-touchspin">
                                            <span class="input-group-text">Buscar Rutas &nbsp;&nbsp; <i class="fas fa-search"></i></span></span><input id="ruta_busqueda" type="text" class="form-control" placeholder="Escriba nombre de la ruta" oninput="reload_registro()">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-2 mt-2">
                                        <div class="custom-control custom-checkbox mr-sm-2 mb-3">
                                            <input type="checkbox" class="custom-control-input" id="check_todo" onclick="btn_check_todo()">
                                            <label class="custom-control-label" for="check_todo">Seleccionar todo</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 mt-2">
                                        <?php if ($perfil == 1) { ?>
                                            <button type="button" class="btn waves-effect waves-light btn-sm btn-danger" onclick="btn_eliminar_registros()"><i class="fas fa-trash-alt"></i> Eliminar Selección</button>
                                        <?php } ?>
                                    </div>
                                    <div class="col-lg-2 mt-2">
                                        <?php if ($perfil == 1) { ?>
                                            <button type="button" class="btn waves-effect waves-light btn-sm btn-info" onclick="btn_eliminar_registros_all()"><i class="fas fa-trash-alt"></i> Eliminar Todos</button>
                                        <?php } ?>
                                    </div>
                                    <div class="col-lg-12">
                                        <table class="table" id="table_datos">
                                            <thead class="bg-blue">
                                                <tr>
                                                    <th></th>
                                                    <th>Ruta</th>
                                                    <th>Chofer</th>
                                                    <th>Unidad</th>
                                                    <th>Cliente</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="registro_ruta" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header modal_sistema">
                <div class="col-lg-1"></div>
                <div class="col-lg-10" align="center">
                    <h4 class="modal-title">Registro de ruta</h4>
                </div>
                <div class="col-lg-1">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" id="foto_validar" value="0">
                                <form class="form" method="post" role="form" id="form_ruta">
                                    <input type="hidden" name="rutaId" id="rutaId" value="0">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label><span style="color: red;">*</span> Ruta</label>
                                                        <input type="text" name="ruta" id="ruta" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <?php if($perfil == 1){ ?>
                                                    <div class="form-group">
                                                        <label>Cliente</label>
                                                            <select class="form-control" name="clienteId" id="clienteId">
                                                            <option disabled selected value="0">Selecciona una opción</option>
                                                            <?php foreach ($clientes as $c) {
                                                            echo '<option value="'.$c->clienteId.'">'.$c->nombre.' '.$c->appaterno.'</option>';
                                                            } ?>
                                                        </select>
                                                        
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <?php if($perfil == 1){ ?>
                                                    <div class="form-group">
                                                        <label>Unidad</label>
                                                        <select class="form-control" name="unidadId" id="unidadId">
                                                            <option disabled selected value="0">Selecciona una opción</option>
                                                            <?php foreach ($unidades as $u) {
                                                                echo '<option value="'.$u->id.'">'.$u->unidad.'</option>';
                                                            } ?>
                                                        </select>
                                                        
                                                    </div>
                                                    <?php } ?>
                                                </div>



                                                <div class="col-md-6">
                                                    <?php if($perfil == 1){ ?>
                                                    <div class="form-group">
                                                        <label>Chofer</label>
                                                        <select class="form-control" name="choferId" id="choferId">
                                                            <option disabled selected value="0">Selecciona una opción</option>
                                                            <?php foreach ($operadores as $o) {
                                                                echo '<option value="'.$o->operadorId.'">'.$o->nombre.' '.$o->ap_paterno.' '.$o->ap_materno.'</option>';
                                                            } ?>
                                                        </select>
                                                        
                                                    </div>
                                                    <?php } ?>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info waves-effect btn_registro" onclick="guarda_ruta()">Guardar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div id="elimina_ruta_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea borrar esta ruta? Esta operación no se puede deshacer.</h5>
            </div>
            <input type="hidden" id="id_ruta">
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="delete_ruta()">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div id="elimina_registro_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea borrar estos registros? Esta operación no se puede deshacer.</h5>
            </div>
            <input type="hidden" id="id_ruta">
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="delete_registros()">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div id="elimina_masivo_modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea borrar todos los registros? Esta operación no se puede deshacer.</h5>
            </div>
            <input type="hidden" id="id_ruta">
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="delete_registros_all()">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="iframe_datos" style="display: none;">
</div>

<div id="modal_check" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Check masivo</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <input type="hidden" id="idruta">
                    <input type="hidden" id="lat">
                    <input type="hidden" id="long">
                    <div class="col-md-12">
                        <h4 class="modal-title" id="myLargeModalLabel">Registro para usuarios sin registro de tipo bajada</h4>
                    </div>
                    <div class="col-md-6">
                        <br>
                        <div class="form-group">
                            <label>Fecha de registro</label>
                            <input type="date" id="fecha_reg" class="form-control" value="<?php echo date("Y-m-d"); ?>" max="<?php echo date("Y-m-d"); ?>">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <br>
                        <span>Se realizará cambios a todos los usuarios que no hayan realizado registro</span>
                    </div>
                </div>
              
            </div>
            <input type="hidden" id="id_ruta">
            <div class="modal-footer btn_paciente_registro">
                <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-danger waves-effect" onclick="saveCheck()">Realizar</button>
            </div>
        </div>
    </div>
</div>