<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Empleados extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        $this->idpersonal=$this->session->userdata('idpersonal');
        $this->administrador=$this->session->userdata('administrador');
        $this->perfilid=$this->session->userdata('perfilid');
        date_default_timezone_set('America/Mexico_City');
        $this->fecha_hora_actual = date('Y-m-d G:i:s');
        $this->fechainicio = date('Y-m-d');
        $this->inicioactual = date('G:i:s');
        $this->load->library('PHPExcelLib');  // Carga la librería personalizada

        if ($this->session->userdata('logeado')){
            $this->idpersonal=$this->session->userdata('idpersonal');
            $this->perfilid=$this->session->userdata('perfilid');
            $this->cliente = $this->session->userdata('cliente');
            $permiso=$this->Login_model->getviewpermiso($this->perfilid,1);// perfil y id del submenu
            if ($permiso==0) {
                redirect('Login');
            }
        }else{
            redirect('/Login');
        }
    }
    public function index(){
        $data['perfil']=$this->perfilid;
        $data['cliente']=$this->cliente;
        $data['clientes'] = $this->General_model->get_records_condition('estatus = 1','clientes');

        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('empleados/empleados',$data);
        $this->load->view('templates/footer');
        $this->load->view('empleados/empleadosjs');
    }

    public function registra_empleado(){
        //log_message('error','$this->cliente: '.$this->cliente);
        $data=$this->input->post();
        $personalId=$data['personalId'];
        //$array = array('estatus'=>1,'tipo_empleado'=>0);
        $array = array('estatus'=>1);
        $result=$this->General_model->getselectwhere_orden_asc('personal',$array,'personalId');
        $cant=0;
        foreach ($result as $i){
            $cant=$i->numero_empleado;
        }
        $cant_aux=$cant+1;
        if(isset($data['check_baja'])){
            $data['check_baja']='on';
        }else{
            $data['check_baja']='';
        }
        
        if($personalId==0){
            //$data['cliente'] = 0;

            if($this->perfilid != 1){
                $data['cliente'] = $this->cliente;
            }

            $data['reg']=$this->fecha_hora_actual;
            $data['numero_empleado']=$cant_aux;
            $id=$this->General_model->add_record('personal',$data);
        }else{
            $id=$this->General_model->edit_record('personalId',$personalId,$data,'personal');
            $id=$personalId;
        }

        echo $id;
    }

    public function getlistado(){
        $params = $this->input->post();

        if($this->perfilid == 3){
            $params['particular'] = $this->cliente;
            //log_message('error','PARTICULAR');
            $params['selectCliente'] = '0';
        }
        //log_message('error','PARTICULAR: '.$params['particular']);
        //log_message('error','SELECTCliente: '.$params['selectCliente']);

        $getdata = $this->ModelCatalogos->get_empleados($params);
        $totaldata= $this->ModelCatalogos->total_empleados($params);
        
        //log_message('error','DATA: '.json_encode($getdata->result()));

        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        
        echo json_encode($json_data);
    }
    public function delete_empleado(){
        $id=$this->input->post('id');
        $data = array('estatus'=>0);
        $this->General_model->edit_record('personalId',$id,$data,'personal');
        $this->General_model->edit_record('personalId',$id,$data,'usuarios');
    }
    public function registro_usuario(){
        $id=$this->input->post('id');
        $UsuarioID=0;
        $perfilId=0;
        $Usuario='';
        $pass='';
        $result_usuario=$this->General_model->getselectwhere('usuarios','personalId',$id);
        foreach ($result_usuario as $item){
            $UsuarioID=$item->UsuarioID;
            $perfilId=$item->perfilId;
            $personalId=$item->personalId;
            $Usuario=$item->Usuario;
            $pass='xxxxxxxxxx.';
        }
        $html='<form class="form" method="post" role="form" id="form_usuario">
                    <input type="hidden" name="UsuarioID" id="UsuarioID" value="'.$UsuarioID.'"> 
                    <div class="txt_usu">
                      <div class="row">
                          <div class="col-md-12">
                              <div class="form-group">
                                  <label>Nombre de usuario <div style="color: red" class="txt_usuario"></div></label>
                                  <input type="text" name="usuario" id="usuario" autocomplete="nope" oninput="verificar_usuario('.$UsuarioID.')" value="'.$Usuario.'" class="form-control">
                              </div>
                          </div>
                      </div>
                    </div>  
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Contraseña</label>
                                <input type="password" name="contrasena" id="contrasena" autocomplete="new-password" value="'.$pass.'" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Verificar contraseña</label>
                                <input type="password" name="contrasena2" id="contrasena2" value="'.$pass.'" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Empleado</label>';
                                $result=$this->General_model->getselectwhere('personal','personalId',$id);
                                $html.='<input type="hidden" name="personalId" readonly  class="form-control" value="'.$id.'">'; 
                                foreach ($result as $item) {
                                $html.='<input type="text" readonly class="form-control" value="'.$item->nombre.'">'; 
                                } 
                                $html.='
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Perfil</label>';
                                $resultp=$this->General_model->getselectwhere('perfiles','estatus',1);
                                $html.='<select name="perfilId" id="perfilId" class="form-control">';
                                foreach ($resultp as $item) {
                                    if($item->perfilId==$perfilId){
                                        $html.='<option value="'.$item->perfilId.'" selected>'.$item->nombre.'</option>';
                                    }else{
                                        $html.='<option value="'.$item->perfilId.'">'.$item->nombre.'</option>';
                                    }  
                                }
                                $html.='</select>
                        </div>
                        </div>
                    </div>   
                </form>';
        echo $html;
    }
    public function add_usuarios(){
        $datos = $this->input->post();
        $pss_verifica = $datos['contrasena'];
        $pass = password_hash($datos['contrasena'], PASSWORD_BCRYPT);
        $datos['contrasena'] = $pass;
        if($pss_verifica == 'xxxxxxxxxx.'){
           unset($datos['contrasena']);
        }
        $id=$datos['UsuarioID'];
        $personalId=$datos['personalId'];
        unset($datos['UsuarioID']);
        unset($datos['contrasena2']);
        if ($id>0) {
            $where = array('UsuarioID'=>$id);
            $this->General_model->edit_recordw($where,$datos,'usuarios');
            $result=2;
        }else{
            $this->General_model->add_record('usuarios',$datos);
            $result=1;
            $this->General_model->edit_recordw(array('personalId'=>$personalId),array('usuario'=>1),'personal');
        }   
        echo $result;
    }

    public function gafete($id){
        $data['get_empleado'] = $this->General_model->get_record('personalId',$id,'personal');
        $data['get_empresa'] = $this->General_model->getData_rutaCliente($id);

        if($data['get_empresa'] == null){
            //log_message('error','IS NULL GET EMPRESA: '.$id);
            $json_string = '{"ruta":"","cliente":"","foto":"","color":"#59636c"}';
            $data['get_empresa'] = json_decode($json_string);
        }

        //log_message('error','Data Empleado: '. json_encode($data['get_empleado']));
        //log_message('error','Data empresa: '. json_encode($data['get_empresa']));

        //$data['gafete']=$this->General_model->get_record('id',1,'gafete');
        $this->load->view('templates/header',$data);
        $this->load->view('empleados/gafete',$data);
    }

    function validar(){
        $Usuario = $this->input->post('Usuario');
        $Id = $this->input->post('Id');
        //$result = $this->General_model->getselectwhere('usuarios','Usuario',$Usuario);
        $result = $this->General_model->getselectwhereall('usuarios',array('Usuario'=>$Usuario,'estatus'=>1));
        //log_message('error','RESULT: '.json_encode($result));
        $resultado = 0;

        foreach ($result as $row) {
            if($Id != $row->personalId){
                $resultado = 1;
            }
        }

        echo $resultado;
    }
    function cargafiles(){
        $id=$this->input->post('id');
        $folder="personal";
        $upload_folder ='uploads/'.$folder;
        $nombre_archivo = $_FILES['foto']['name'];
        $tipo_archivo = $_FILES['foto']['type'];
        $tamano_archivo = $_FILES['foto']['size'];
        $tmp_archivo = $_FILES['foto']['tmp_name'];
        //$archivador = $upload_folder . '/' . $nombre_archivo;
        $fecha=date('ymd-His');
        $newfile='doc_'.$fecha.$nombre_archivo;        
        $archivador = $upload_folder . '/'.$newfile;
        if (!move_uploaded_file($tmp_archivo, $archivador)) {
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{
          $array = array('foto'=>$newfile);
          $this->General_model->edit_record('personalId',$id,$array,'personal');
          $return = Array('ok'=>TRUE);
        }
        echo json_encode($return);
    }  

    function registros_delete(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $personalId = $DATA[$i]->personalId;    
            $array = array('estatus'=>0);      
            $this->General_model->edit_record('personalId',$personalId,$array,'personal');
            $this->General_model->edit_record('personalId',$personalId,$array,'usuarios');
        }
    }

    public function delete_registros_particular(){
        $array = array('estatus'=>0);  
        //$this->General_model->edit_record_all($array,'personal');
        $cliente_aux = $this->perfilid == 3 ? $this->cliente : 0;

        $this->General_model->edit_record('cliente',$cliente_aux,$array,'personal');
        $this->General_model->edit_record('clienteId',$cliente_aux,$array,'usuarios');

        $array2 = array('estatus'=>1,'tipo_empleado'=>1);      
        $this->General_model->edit_record('personalId',1,$array2,'personal');

        $array3 = array('estatus'=>1); 
        $this->General_model->edit_record('UsuarioID',1,$array3,'usuarios');
    }

    public function delete_registros(){
        $array = array('estatus'=>0);  
        $this->General_model->edit_record_all($array,'personal');
        $this->General_model->edit_record_all($array,'usuarios');

        $array2 = array('estatus'=>1,'tipo_empleado'=>1);
        $this->General_model->edit_record('personalId',1,$array2,'personal');

        $array3 = array('estatus'=>1); 
        $this->General_model->edit_record('UsuarioID',1,$array3,'usuarios');

        //$this->General_model->edit_record_all_protected($array,'usuarios','UsuarioID',1);
    }

    function plantillaExcel(){
        $data['esAdmin'] = 0;

        if($this->perfilid == 1){
            $data['esAdmin'] = 1;
        }

        $this->load->view('empleados/plantillaExcel',$data);
    }


    function guardar_malla_empleados()
    {
        $configUpload['upload_path'] = FCPATH . 'fileExcel/';
        $configUpload['allowed_types'] = 'xls|xlsx|csv';

        $this->load->library('upload', $configUpload);
        $this->upload->do_upload('inputFile');
        $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
        $file_name = $upload_data['file_name']; //uploded file name
        $extension = $upload_data['file_ext'];    // uploded file extension
        //===========================================
        $objReader = PHPExcel_IOFactory::createReader('CSV'); // For cvs delimitado por comas  
        $objReader->setReadDataOnly(true);
        //Load excel file
        $objPHPExcel = $objReader->load(FCPATH . 'fileExcel/' . $file_name);
        $totalrows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows avalable in excel         
        $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
        //loop from first data untill last data

        //$cont = 1;
        $band_carga = 0;
        $idEmpleadosDeCliente = array();
        $perteneceAlCliente = false;
        //log_message('error','Pertenece: '.$perteneceAlCliente);


        if($this->perfilid > 1)
        {
            $idEmpleadosDeCliente = $this->General_model->obtener_ids_por_tabla('personal', array('cliente'=>$this->cliente,'estatus'=>1), 'personalId');
            //log_message("error","Data DESDE MALLA: " . json_encode($idEmpleadosDeCliente));
        }


        for ($i = 2; $i <= $totalrows; $i++) {
            
            $personalId = empty( $objWorksheet->getCellByColumnAndRow(0, $i)->getValue() ) ? 0 : $objWorksheet->getCellByColumnAndRow(0, $i)->getValue();
            $nombre = empty( $objWorksheet->getCellByColumnAndRow(1, $i)->getValue() ) ? '' : $objWorksheet->getCellByColumnAndRow(1, $i)->getValue();
            $appaterno = empty( $objWorksheet->getCellByColumnAndRow(2, $i)->getValue() ) ? '' : $objWorksheet->getCellByColumnAndRow(2, $i)->getValue();
            $apmaterno = empty( $objWorksheet->getCellByColumnAndRow(3, $i)->getValue() ) ? '' : $objWorksheet->getCellByColumnAndRow(3, $i)->getValue();
            $operativo = empty( $objWorksheet->getCellByColumnAndRow(4, $i)->getValue() ) ? '' : $objWorksheet->getCellByColumnAndRow(4, $i)->getValue();
            $tel_emergencia = empty( $objWorksheet->getCellByColumnAndRow(5, $i)->getValue() ) ? '' : $objWorksheet->getCellByColumnAndRow(5, $i)->getValue();
            $persona = empty( $objWorksheet->getCellByColumnAndRow(6, $i)->getValue() ) ? '' : $objWorksheet->getCellByColumnAndRow(6, $i)->getValue();
            $parentesco = empty( $objWorksheet->getCellByColumnAndRow(7, $i)->getValue() ) ? '' : $objWorksheet->getCellByColumnAndRow(7, $i)->getValue();
            $sangre = empty( $objWorksheet->getCellByColumnAndRow(8, $i)->getValue() ) ? '' : $objWorksheet->getCellByColumnAndRow(8, $i)->getValue();
            
            $cliente = empty( $objWorksheet->getCellByColumnAndRow(9, $i)->getValue() ) ? 0 : $objWorksheet->getCellByColumnAndRow(9, $i)->getValue();
            $cliente =  $this->perfilid == 1 ? $cliente : $this->cliente;

            if ($nombre == '') {
                continue; // Salta esta iteración   
            }


            if($this->perfilid > 1)
            {
                $perteneceAlCliente = in_array($personalId, $idEmpleadosDeCliente);

                if($perteneceAlCliente == true){
                    //log_message('error', "El valor pertenece al cliente? SI ");
                }else{
                    if($personalId == 0){
                        //log_message('error', "El valor pertenece al cliente? NO, pero es personal nuevo.");
                    }else{
                        //log_message('error', "El valor pertenece al cliente? NO ");
                    }
                }

                if ($perteneceAlCliente == false && $personalId > 0) {
                    //log_message('error', "No se registra / edita el personal.");
                    continue; // Salta esta iteración
                }
            }
            

            //Numero de empleado INICIO --------------------->
            $dataP = $this->General_model->getselectwhere_orden_asc('personal',array('estatus'=>1),'personalId');
            $cant = 0;
            foreach ($dataP as $p){
                $cant = $p->numero_empleado;
            }
            $cant_aux = $cant+1;
            //Numero de empleado FINAL ---------------------->

            
            $data = array(
                'personalId' => $personalId,
                'nombre' => $nombre,
                'appaterno' => $appaterno,
                'apmaterno' => $apmaterno,
                'operativo' => $operativo,
                'tel_emergencia' => $tel_emergencia,
                'persona' => $persona,
                'parentesco' => $parentesco,
                'sangre' => $sangre,
                'cliente' => $cliente,
            );

            if (intval($personalId) == 0) {

                $data['reg'] = $this->fecha_hora_actual;
                $data['numero_empleado'] = $cant_aux;
                $id = $this->General_model->add_record('personal',$data);
            } else {
                $this->General_model->edit_record('personalId',$personalId,$data,'personal');
                $id = $personalId;
            }

            $band_carga++;
        
        }
        unlink('fileExcel/' . $file_name); //File Deleted After uploading in database . 

        //===========================================
        $output = [];
        /*if($band_carga>0)
          $output=array("estatus"=>"ok");
        else 
          $output=array("estatus"=>"no_ok");
        */
        echo json_encode($output);
    }

}