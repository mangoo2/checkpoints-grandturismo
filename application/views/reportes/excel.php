<?php
$fechainicio = $f1;
$fechafin = $f2;

header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=reporte_" . $fechainicio . "_al_" . $fechafin . ".xls");
?>

<table border="1">
    <thead>
        <tr>
        <tr>
            <th>ID</th>
            <th>Fecha y hora</th>
            <th>Empleado</th>
            <th>Ruta</th>
            <th>Cliente</th>
            <th>Check points</th>
        </tr>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($get_poins as $or) { ?>
            <tr>
                <td><?php echo $or->id ?></td>
                <td><?php echo $or->reg ?></td>
                <td><?php echo utf8_decode($or->nombre) ?></td>
                <td><?php echo utf8_decode($or->ruta) ?></td>
                <td><?php echo utf8_decode($or->cliente) ?></td>
                <td><?php echo utf8_decode($or->poins) ?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>