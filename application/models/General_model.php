<?php
class General_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get_table($table){
    	$sql = "SELECT * FROM $table";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_table_active($table){
    	$sql = "SELECT * FROM $table WHERE estatus=1";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_record($col,$id,$table){
    	$sql = "SELECT * FROM $table WHERE $col=$id";
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function get_records_condition($condition,$table){
    	$sql = "SELECT * FROM $table WHERE $condition";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function add_record($table,$data){
    	$this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function edit_record($cos,$id,$data,$table){
    	$this->db->set($data);
        $this->db->where($cos, $id);
        return $this->db->update($table);
    }
    public function edit_recordw($where,$data,$table){
        $this->db->set($data);
        $this->db->where($where);
        return $this->db->update($table);
    }

    public function edit_record_all_protected($data, $table, $column, $id){
        $this->db->where($column . '!=', $id);
        $this->db->set($data);
        return $this->db->update($table);
    }

    public function edit_record_all($data,$table){
        $this->db->set($data);
        return $this->db->update($table);
    }

    public function delete_records($condition,$table){
    	$sql = "DELETE FROM $table WHERE $condition";
        $query = $this->db->query($sql);
        return $query;
    }
    public function getselectwhere($tables,$cols,$values){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($cols,$values);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }
    public function getselectwhereall($tables,$values){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($values);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }
    public function getselectwhereall2($tables,$values){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($values);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        //$this->db->close();
        return $query;
    }
    public function getselectlike($tables,$values,$search){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($values);
        $this->db->or_like($search);
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }
    public function getselectwhere_orden_asc($tables,$values,$title){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($values);/// Se puede ocupar un array para n condiciones
        $this->db->order_by($title, 'ASC');
        $query=$this->db->get();
        return $query->result();
    }
    public function getselectwhere_orden_desc($tables,$values,$title){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($values);/// Se puede ocupar un array para n condiciones
        $this->db->order_by($title, 'DESC');
        $query=$this->db->get();
        return $query->result();
    } 
    public function getselectwhere_orden_desc_row($tables,$values,$title){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($values);/// Se puede ocupar un array para n condiciones
        $this->db->order_by($title, 'DESC');
        $query=$this->db->get();
        return $query->row();
    } 
    public function horacioslaborales($dia){
        switch ($dia) {
            case 1:
                $dial='l';
                break;
            case 2:
                $dial='m';
                break;
            case 3:
                $dial='mi';
                break;
            case 4:
                $dial='j';
                break;
            case 5:
                $dial='v';
                break;
            case 6:
                $dial='s';
                break;
            case 7:
                $dial='d';
                break;
        }
        $sql = "SELECT hl.horafin AS horas
                FROM config_horario_laboral AS hl
                WHERE hl.status =1 AND hl.id =$dia
                UNION
                SELECT hl.horainicio AS horas
                FROM config_horario_laboral AS hl
                WHERE hl.status =1 AND hl.id =$dia
                UNION
                SELECT h_inicio AS horas
                FROM config_horario_nodisponible AS hnl
                WHERE hnl.activo =1
                AND hnl.$dial =1
                UNION
                SELECT h_fin AS horas
                FROM config_horario_nodisponible AS hnl
                WHERE hnl.activo =1
                AND hnl.$dial =1
                ORDER BY `horas` ASC";
        $query = $this->db->query($sql);
        return $query;
    }
    public function get_info_random($table, $cantidad){
        $sql = "SELECT * FROM $table WHERE activo=1 ORDER BY RAND() LIMIT $cantidad";
        $query = $this->db->query($sql);
        return $query;
    }

    public function delete_detalle_perfil($id){
        $sql = "DELETE FROM perfiles_detalles WHERE Perfil_detalleId=".$id;
        $query = $this->db->query($sql);
    }
    /*
    public function get_records_menu($id){
        $sql = "SELECT m.Pagina FROM config_general AS c
                INNER JOIN menu_sub AS m ON m.MenusubId = c.MenusubId
                WHERE c.perfilId=$id";
        $query = $this->db->query($sql);
        return $query->result();
    }
    */

    public function get_data_condition($select, $condition, $table){
        $sql = "SELECT $select FROM $table WHERE $condition";
        $query = $this->db->query($sql);
        return $query->result()->$select;
    }

    public function obtener_ids_por_tabla($table, $condition, $id_column = 'id') {
        $this->db->select($id_column);
        $this->db->from($table);
        $this->db->where($condition); // Se puede ocupar un array para n condiciones
        $query = $this->db->get();

        $ids = array_column($query->result_array(), $id_column);

        return $ids; // Devuelve un array con los IDs
    }
    
    public function get_data_rutasClientes($id){
        $this->db->select('rc.id, rc.tipo, r.ruta, CONCAT(p.nombre," ",p.appaterno," ",p.apmaterno) as empleado');
        $this->db->from('rutas_clientes rc');
        $this->db->join('rutas r', 'rc.rutaId = r.id AND r.estatus = 1', 'left');
        $this->db->join('personal p', 'rc.empleadoId = p.personalId AND p.estatus = 1', 'left');
        $this->db->where('rc.rutaId = '.$id);
        $this->db->where('rc.estatus = 1');

        $query = $this->db->get();
        return $query->result();
    }



    public function get_doubles($idEmpleado,$idRuta,$idCliente){
        $this->db->select('id');
        $this->db->from('rutas_clientes');
        $this->db->where('empleadoId = '.$idEmpleado);
        /*$this->db->where('rutaId = '.$idRuta);*/
        $this->db->where('clienteId = '.$idCliente);
        $this->db->where('estatus = 1');

        $query = $this->db->get();
        return $query->row()->id;
    }


    public function getOperadorRuta($idC){
        $this->db->select('DISTINCT o.operadorId, CONCAT(o.nombre, " ",o.ap_paterno, " ",o.ap_materno) AS operador', false);
        $this->db->from('rutas AS r');
        $this->db->join('operadores AS o', 'r.choferId = o.operadorId AND o.estatus = 1');
        $this->db->where('r.clienteId = '.$idC);
        $query = $this->db->get();
        return $query->result();
    }


    public function getData_rutaCliente($id){
        $this->db->select('IFNULL(r.ruta, "") as ruta, IFNULL(CONCAT(c.nombre," ",c.appaterno," ",c.apmaterno), "") as cliente, IFNULL(c.foto,"") as foto, IF(c.color = "" OR c.color IS NULL, "#59636c", c.color) AS color ');
        $this->db->from('personal p');
        //$this->db->from('rutas_clientes rc');
        $this->db->join('rutas_clientes rc', 'p.personalId = rc.empleadoId AND rc.estatus = 1', 'left');
        $this->db->join('rutas r', 'rc.rutaId = r.id AND r.estatus = 1', 'left');
        $this->db->join('clientes c', 'p.cliente = c.clienteId AND c.estatus = 1', 'left');
        $this->db->where('p.personalId = '.$id);
        $this->db->where('p.estatus = 1');

        $query = $this->db->get();
        return $query->row();
    }

/*
    public function getOperadorRuta($idR){
        $this->db->select('o.operadorId, CONCAT(o.nombre, " ",o.ap_paterno, " ",o.ap_materno) AS operador');
        $this->db->from('rutas AS r');
        $this->db->join('operadores AS o', 'r.choferId = o.operadorId AND o.estatus = 1');
        $this->db->where('r.id = '.$idR);
        $query = $this->db->get();
        return $query->result();
    }
*/

    public function getChecksRuta($id,$fecha,$tipo){
        $this->db->select('c.*');
        $this->db->from('check_points_detalles c');
        $this->db->where('c.idruta',$id);
        $this->db->where("c.reg between '".$fecha." 00:00:00' AND '".$fecha." 23:59:59' ");
        $this->db->where("c.idpoints_check",$tipo);
        $query = $this->db->get();
        return $query->result();
    }

    /*public function getRutaCliCheck($id,$fecha){
        $this->db->select('rc.*');
        $this->db->from('rutas_clientes rc');
        $this->db->join('check_points_detalles cp','cp.idruta=rc.rutaId and cp.idpoints_check=1');
        $this->db->where('rc.rutaId',$id);
        $this->db->where("cp.reg between '".$fecha." 00:00:00' AND '".$fecha." 23:59:59' ");
        $this->db->where("rc.estatus","1");
        
        $query = $this->db->get();
        return $query->result();
    }*/

    public function getRutaCliCheck($id,$fecha){
        $sql = "SELECT c.*
        FROM check_points_detalles c
        WHERE c.idruta = ".$id."
        AND c.reg BETWEEN '".$fecha." 00:00:00' AND '".$fecha." 23:59:59'
        AND c.idpoints_check = 1
        AND c.personalId NOT IN (
            SELECT p.personalId
            FROM check_points_detalles p
            WHERE p.idruta = ".$id."
            AND p.reg BETWEEN '".$fecha." 00:00:00' AND '".$fecha." 23:59:59'
            AND p.idpoints_check = 2
        );";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function checkSubidaBajada($id){
        // Contar registros con idpoints_check = 1 dentro de los últimos dos días
        $this->db->select('COUNT(*) as count_1');
        $this->db->from('check_points_detalles');
        $this->db->where('personalId', $id);
        $this->db->where('idpoints_check', 1);
        $this->db->where('DATE(reg) >=', 'DATE_SUB(CURDATE(), INTERVAL 2 DAY)', false);
        $query1 = $this->db->get_compiled_select();

        // Contar registros con idpoints_check = 2 dentro de los últimos dos días
        $this->db->select('COUNT(*) as count_2');
        $this->db->from('check_points_detalles');
        $this->db->where('personalId', $id);
        $this->db->where('idpoints_check', 2);
        $this->db->where('DATE(reg) >=', 'DATE_SUB(CURDATE(), INTERVAL 2 DAY)', false);
        $query2 = $this->db->get_compiled_select();

        // Calcular la diferencia
        $this->db->select("GREATEST(($query1) - ($query2), 0) AS diferencia", false);
        $query = $this->db->get();
        $result = $query->row();

        // Mostrar el resultado
        return $result->diferencia;
    }
}